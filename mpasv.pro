TEMPLATE = subdirs

CONFIG += c++latest

SUBDIRS += \
	base \
	controller \
	gui

controller.depends = base
gui.depends = controller base

DEFINES_DEBUG += __DEBUG__

DEFINES_RELEASE += __RELEASE__

DEFINES += QT_DEPRECATED_WARNINGS
