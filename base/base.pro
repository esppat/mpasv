QT -= gui

TEMPLATE = lib
DEFINES += BASE_LIBRARY

SOURCES += \
	Base.cpp \
	tools/Demangler.cpp \
	tools/Lock.cpp \
	tools/Logger.cpp \
	tools/Nameable.cpp \
	tools/Task.cpp \
	tools/ThreadedTask.cpp \
	tools/Tools.cpp \
	tools/autotests/AutoTestManager.cpp \
	tools/autotests/Testable.cpp \
	tools/gps/GeoPos.cpp \
	tools/intercomm/IntercommBase.cpp \
	tools/intercomm/IntercommClient.cpp \
	tools/intercomm/IntercommContext.cpp \
	tools/intercomm/IntercommMessage.cpp \
	tools/intercomm/IntercommReceiver.cpp \
	tools/intercomm/IntercommSender.cpp \
	tools/intercomm/IntercommServer.cpp \
	tools/parameters/ParameterManager.cpp \
	tools/physics/SClock.cpp \
	tools/routing/Route.cpp \
	tools/routing/Waypoint.cpp \
	tools/test_coverage/TestCoverage.cpp

HEADERS += \
	base_global.h \
	Base.h \
	nlohmann/adl_serializer.hpp \
	nlohmann/detail/conversions/from_json.hpp \
	nlohmann/detail/conversions/to_chars.hpp \
	nlohmann/detail/conversions/to_json.hpp \
	nlohmann/detail/exceptions.hpp \
	nlohmann/detail/input/binary_reader.hpp \
	nlohmann/detail/input/input_adapters.hpp \
	nlohmann/detail/input/lexer.hpp \
	nlohmann/detail/input/parser.hpp \
	nlohmann/detail/iterators/internal_iterator.hpp \
	nlohmann/detail/iterators/iter_impl.hpp \
	nlohmann/detail/iterators/iteration_proxy.hpp \
	nlohmann/detail/iterators/json_reverse_iterator.hpp \
	nlohmann/detail/iterators/primitive_iterator.hpp \
	nlohmann/detail/json_pointer.hpp \
	nlohmann/detail/json_ref.hpp \
	nlohmann/detail/macro_scope.hpp \
	nlohmann/detail/macro_unscope.hpp \
	nlohmann/detail/meta.hpp \
	nlohmann/detail/output/binary_writer.hpp \
	nlohmann/detail/output/output_adapters.hpp \
	nlohmann/detail/output/serializer.hpp \
	nlohmann/detail/value_t.hpp \
	nlohmann/json.hpp \
	nlohmann/json_fwd.hpp \
	tools/Demangler.h \
	tools/Exception.h \
	tools/Lock.h \
	tools/Lockable.h \
	tools/Logger.h \
	tools/Nameable.h \
	tools/SetUnsetValue.h \
	tools/Task.h \
	tools/ThreadedTask.h \
	tools/Tools.h \
	tools/autotests/AutoTestManager.h \
	tools/autotests/Testable.h \
	tools/gps/GeoPos.h \
	tools/intercomm/IntercommBase.h \
	tools/intercomm/IntercommClient.h \
	tools/intercomm/IntercommContext.h \
	tools/intercomm/IntercommData.h \
	tools/intercomm/IntercommMessage.h \
	tools/intercomm/IntercommReceiver.h \
	tools/intercomm/IntercommSender.h \
	tools/intercomm/IntercommServer.h \
	tools/intercomm/zmq.hpp \
	tools/parameters/ParameterManager.h \
	tools/physics/AmortizedValue.h \
	tools/physics/Pulse.h \
	tools/physics/RegulatedValue.h \
	tools/physics/SClock.h \
	tools/physics/Units.h \
	tools/physics/ValueHistory.h \
	tools/physics/_units.h \
	tools/routing/Route.h \
	tools/routing/Waypoint.h \
	tools/test_coverage/TestCoverage.h

# Default rules for deployment.
unix {
	target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
