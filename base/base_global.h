#pragma once

#include <QtCore/qglobal.h>

#if defined(BASE_LIBRARY)
#  define BASE_EXPORT Q_DECL_EXPORT
#else
#  define BASE_EXPORT Q_DECL_IMPORT
#endif
