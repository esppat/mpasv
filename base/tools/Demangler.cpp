/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Demangler.cpp
 * Author: PEspie
 *
 * Created on February 19, 2019, 9:55 AM
 */

#include "tools/Demangler.h"
#include "tools/Logger.h"

#include "cxxabi.h"

namespace mpasv::tools
	{

		/*static*/
		std::map<const char *, std::string> Demangler::m_names;

		/*static*/
		void Demangler::demangle(const char * name, std::string & result)
		{
			int status;
			char * demangled = abi::__cxa_demangle(name, nullptr, nullptr, &status);
			result = demangled;
			free(demangled);
		}

	} // mpasv
