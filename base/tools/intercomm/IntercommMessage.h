/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommMessage.h
 * Author: esppat
 *
 * Created on January 7, 2020, 9:23 AM
 */

#pragma once

#include "tools/Exception.h"
#include "tools/Tools.h"
#include "zmq.hpp"

#include "IntercommData.h"


namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			class IntercommMessage
			: public zmq::message_t
			, public enable_shared_from_this<IntercommMessage>
			{
			public:

				IntercommMessage()
				{
				}

				virtual ~IntercommMessage()
				{
				}

				template <class DATA>
				const DATA & data() const
				{
					CHECK_TRUE(m_received);
					CHECK_EQUAL(zmq::message_t::size(), sizeof (DATA));
					return *static_cast<const DATA*> (zmq::message_t::data());
				}

				template <class DATA>
				void setData(const DATA & data)
				{
					zmq::message_t::rebuild(&data, sizeof (DATA));
				}

				bool send(zmq::socket_t * socket) const;
				bool receive(zmq::socket_t * socket);

				DataType getType() const
				{
					return *(DataType*) zmq::message_t::data();
				}

			protected:
			protected:
			private:
			private:

				bool m_received = false;

			};

		} // intercomm
	} // tools
} // mpasv
