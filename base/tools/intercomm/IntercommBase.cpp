/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommBase.cpp
 * Author: esppat
 *
 * Created on January 6, 2020, 10:35 PM
 */

#include "IntercommBase.h"
 #include <utility> 
#include "IntercommContext.h"

#include "tools/Tools.h"
#include "tools/Exception.h"


namespace mpasv::tools::intercomm
		{

			IntercommBase::IntercommBase(std::string  address, const unsigned socketType)
				: m_address(std::move(address))
				, m_socketType(socketType)
			{
			}

			IntercommBase::~IntercommBase()
			{
				if (m_socket != nullptr)
				{
					delete m_socket;
					m_socket = nullptr;
				}
			}

			void IntercommBase::setsockopt(int option_, const void *optval_, size_t optvallen_)
			{
				CHECK_NOT_NULLPTR(m_socket);
				m_socket->setsockopt(option_, optval_, optvallen_);
			}

			void IntercommBase::getsockopt(int option_, void *optval_, size_t *optvallen_) const
			{
				CHECK_NOT_NULLPTR(m_socket);
				return m_socket->getsockopt(option_, optval_, optvallen_);
			}

			void IntercommBase::connect()
			{
				LOCK(mutex());
				createSocket();
				m_socket->connect(m_address.c_str());
			}

			void IntercommBase::disconnect()
			{
				LOCK(mutex());
				destroySocket();
			}

			bool IntercommBase::isConnected() const
			{
				LOCK(mutex());
				return m_socket != nullptr;
			}

			void IntercommBase::bind()
			{
				LOCK(mutex());
				createSocket();
				m_socket->bind(m_address.c_str());
			}

			void IntercommBase::createSocket()
			{
				CHECK_NULLPTR(m_socket);
				m_socket = new zmq::socket_t(IntercommContext::getIntercommContext()->getContext(), m_socketType);
			}

			void IntercommBase::destroySocket()
			{
				CHECK_NOT_NULLPTR(m_socket);
				delete m_socket;
				m_socket = nullptr;
			}

		} // mpasv
