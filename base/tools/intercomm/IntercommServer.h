/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommServer.h
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#pragma once

#include "IntercommContext.h"
#include "IntercommBase.h"

#include "tools/ThreadedTask.h"
#include "tools/Logger.h"
#include "tools/Exception.h"
#include "tools/Tools.h"

#include <functional>
#include <string>

#include "tools/intercomm/zmq.hpp"


namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			template <class SERVER>
			class IntercommServer
			: public tools::enable_shared_from_this<IntercommServer<SERVER>>
			, public tools::ThreadedTask
			, public IntercommBase
			{
				public:

				IntercommServer(SERVER & server, const std::string address);
				IntercommServer(const IntercommServer & orig) = delete;
				~IntercommServer();

				protected:
				protected:
				private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

				private:

				SERVER & m_server;

			};

			template <class SERVER>
			IntercommServer<SERVER>::IntercommServer(SERVER & server, const std::string address)
				: tools::ThreadedTask("IntercommServer", true)
				, IntercommBase(address, ZMQ_REP)
				, m_server(server)
			{
				setDelayBetweenRun(0.0_s);
			}

			template <class SERVER>
			IntercommServer<SERVER>::~IntercommServer()
			{
			}

			template <class SERVER>
			bool IntercommServer<SERVER>::setup() noexcept
			{
				if (!ThreadedTask::setup())
					return false;

				bool result = false;

				try
				{
					bind();
					result = true;
				}
				catch (zmq::error_t & e)
				{
					LOG_WARN("Cannot bind server socket: error=", e.what());
				}

				if (!result && isConnected())
				{
					disconnect();
				}

				return result;
			}

			template <class SERVER>
			void IntercommServer<SERVER>::initialize() noexcept
			{
				tools::ThreadedTask::initialize();
			}

			template <class SERVER>
			void IntercommServer<SERVER>::run(const second_t /*tickDuration*/) noexcept
			{
				try
				{
					IntercommMessage request;
					if (request.receive(socket()))
					{
						IntercommMessage reply;
						m_server.manageRequest(request, reply);
						if (!reply.send(socket()))
						{
							LOG_ERR("Cannot send reply of type ", (uint8_t) reply.getType());
						}
					}
					else
					{
						LOG_ERR("Cannot receive request!");
					}
				}
				catch (zmq::error_t & e)
				{
					LOG_ERR("Error while receiving message: error=", e.what());
				}
			}

			template <class SERVER>
			void IntercommServer<SERVER>::finalize() noexcept
			{
				tools::ThreadedTask::finalize();
			}

		} // intercomm
	} // tools
} // mpasv
