/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommReceiver.h
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#pragma once

#include "IntercommBase.h"
#include "IntercommContext.h"

#include "tools/Logger.h"
#include "tools/physics/Units.h"
#include "tools/ThreadedTask.h"

#include <functional>
#include <string>

#include "tools/intercomm/zmq.hpp"


namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			template <class RECEIVER>
			class IntercommReceiver
			: public enable_shared_from_this<IntercommReceiver<RECEIVER>>
			, public tools::ThreadedTask
			, public IntercommBase
			{
				public:

				IntercommReceiver(RECEIVER & receiver,
								const std::string address,
								std::string receiveFilter = "");
				IntercommReceiver(const IntercommReceiver & orig) = delete;
				virtual ~IntercommReceiver();

				protected:
				protected:
				private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

				private:

				RECEIVER & m_receiver;

				std::string m_receiveFilter;

			};

			template <class RECEIVER>
			IntercommReceiver<RECEIVER>::IntercommReceiver(RECEIVER & receiver,
														const std::string address,
														std::string receiveFilter)
				: tools::ThreadedTask("IntercommReceiver", true)
				, IntercommBase(address, ZMQ_SUB)
				, m_receiver(receiver)
				, m_receiveFilter(receiveFilter)
			{
			}

			template <class RECEIVER>
			IntercommReceiver<RECEIVER>::~IntercommReceiver()
			{
			}

			template <class RECEIVER>
			bool IntercommReceiver<RECEIVER>::setup() noexcept
			{
				if (!ThreadedTask::setup())
					return false;

				bool result = false;

				try
				{
					connect();
					setsockopt(ZMQ_SUBSCRIBE, m_receiveFilter.c_str(), m_receiveFilter.length());
					setsockopt(ZMQ_RCVTIMEO, (int) 5000);

					IntercommMessage message;
					if (message.receive(socket()))
					{
						setsockopt(ZMQ_RCVTIMEO, (int) - 1);
						result = true;
					}
					else
					{
						LOG_WARN("Cannot connect: error=", zmq_strerror(zmq_errno()));
					}
				}
				catch (zmq::error_t & e)
				{
					LOG_WARN("Cannot connect: error=", e.what());
				}

				if (!result && isConnected())
				{
					disconnect();
				}

				return result;
			}

			template <class RECEIVER>
			void IntercommReceiver<RECEIVER>::initialize() noexcept
			{
				tools::ThreadedTask::initialize();
				setDelayBetweenRun(0.0_s);
			}

			template <class RECEIVER>
			void IntercommReceiver<RECEIVER>::run(const second_t /*tickDuration*/) noexcept
			{
				try
				{
					IntercommMessage message;

					if (message.receive(socket()))
					{
						m_receiver.messageReceived(message);
					}
				}
				catch (zmq::error_t & e)
				{
					LOG_ERR("Error while receiving message: error=", e.what());
				}
			}

			template <class RECEIVER>
			void IntercommReceiver<RECEIVER>::finalize() noexcept
			{
				if (isConnected())
				{
					disconnect();
				}

				tools::ThreadedTask::finalize();
			}

		} // intercomm
	} // tools
} // mpasv
