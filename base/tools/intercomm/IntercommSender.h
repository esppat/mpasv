/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommSender.h
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#pragma once

#include "IntercommBase.h"

#include "tools/physics/Units.h"

#include <stddef.h>


namespace zmq
{
	class socket_t;
} // zmq

namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			class IntercommSender
			: public enable_shared_from_this<IntercommSender>
			, public IntercommBase
			{
			public:

				IntercommSender(const std::string& address);
				IntercommSender(const IntercommSender& orig) = delete;
				virtual ~IntercommSender();

				void sendMessage(const IntercommMessage & message);

			protected:
			protected:
			private:
			private:
			};

		} // intercomm
	} // tools
} // mpasv
