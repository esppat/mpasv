/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommMessage.cpp
 * Author: esppat
 *
 * Created on January 7, 2020, 9:23 AM
 */

#include "IntercommMessage.h"

#include "tools/Exception.h"
#include "tools/Tools.h"
#include "tools/Logger.h"

#include "tools/physics/Units.h"


namespace mpasv::tools::intercomm
		{

			bool IntercommMessage::send(zmq::socket_t * socket) const
			{
				CHECK_NOT_NULLPTR(socket);
				bool result = true;

				try
				{
					while (socket->send(const_cast<IntercommMessage&> (*this), zmq::send_flags::dontwait).value() == 0)
					{
						if (zmq_errno() == EAGAIN)
						{
							tools::physics::sleepFor(1.0_ms);
						}
						else
						{
							result = false;
							break; // while
						}
					}
				}
				catch (const zmq::error_t &)
				{
					LOG_ERR("Cannot send message: error=\"", zmq_strerror(zmq_errno()));
				}

				return result;
			}

			bool IntercommMessage::receive(zmq::socket_t * socket)
			{
				CHECK_NOT_NULLPTR(socket);
				bool result = false;

				try
				{
					if (socket->recv(*this).has_value())
					{
						result = true;
					}
				}
				catch (const zmq::error_t &)
				{
					LOG_ERR("Cannot receive message: error=\"", zmq_strerror(zmq_errno()));
				}

				m_received = result;
				return result;
			}

		} // mpasv
