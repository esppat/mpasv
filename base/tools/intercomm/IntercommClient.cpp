/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommClient.cpp
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#include "IntercommClient.h"
#include "IntercommContext.h"

#include "tools/physics/Units.h"
#include "tools/Logger.h"
#include "tools/intercomm/zmq.hpp"


namespace mpasv::tools::intercomm
		{

			IntercommClient::IntercommClient(const std::string& address)
				: IntercommBase(address, ZMQ_REQ)
			{
				connect();
			}

			IntercommClient::~IntercommClient()
			= default;

			bool IntercommClient::sendRequest(const IntercommMessage & request, IntercommMessage & reply)
			{
				bool result = false;

				LOCK(mutex());
				if (request.send(socket()))
				{
					if (reply.receive(socket()))
					{
						result = true;
					}
					else
					{
						LOG_ERR("Cannot receive reply for request of type ", (uint8_t) request.getType());
					}
				}
				else
				{
					LOG_ERR("Cannot send request of type ", (uint8_t) request.getType());
				}

				return result;
			}

		} // mpasv
