/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommSender.cpp
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#include "IntercommSender.h"
#include "IntercommContext.h"

#include "tools/physics/Units.h"

#include <string>

#include "tools/intercomm/zmq.hpp"


namespace mpasv::tools::intercomm
		{

			IntercommSender::IntercommSender(const std::string& address)
				: IntercommBase(address, ZMQ_PUB)
			{
				bind();
			}

			IntercommSender::~IntercommSender()
			= default;

			void IntercommSender::sendMessage(const IntercommMessage & message)
			{
				message.send(socket());
			}

		} // mpasv
