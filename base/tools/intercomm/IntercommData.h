/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommData.h
 * Author: esppat
 *
 * Created on January 19, 2020, 10:06 PM
 */

#pragma once

#include "float.h"

#define INV_VAL (-9999.0)

enum class DataType : uint8_t
{
	dataBase = 0,
	dataDebug = 1,
	dataExtended = 2,
	dataIMU = 3,

	dataRequestWaypointDescription = 4,
	dataReplyWaypointDescription = 5,

};

typedef struct
{
	const DataType type = DataType::dataBase;

	float GPSSpeed_kts = INV_VAL;
	float GPSCourse_deg = INV_VAL;
	float lat_deg = FLT_MAX;
	float lon_deg = FLT_MAX;
} DataBase;

typedef struct
{
	const DataType type = DataType::dataDebug;

	float relativeWaterSpeed_kts = INV_VAL;
	float relativeWindSpeed_kts = INV_VAL;
	float relativeWinddegree_t_deg = INV_VAL;
	float realWindSpeedCalculated_kts = INV_VAL;
	float realWinddegree_tCalculated_deg = INV_VAL;
	float compassdegree_t_deg = INV_VAL;
	float targetCourse_deg = INV_VAL;
	float targetLat_deg = FLT_MAX;
	float targetLon_deg = FLT_MAX;
	float crossTrackError_m = INV_VAL;
} DataDebug;

typedef struct
{
	const DataType type = DataType::dataExtended;

	float helmPercent_pct = INV_VAL;
	float throttles_pct[2] = {INV_VAL, INV_VAL};
} DataExtended;

typedef struct
{
	const DataType type = DataType::dataIMU;

	float accel_mps_sq[3] = {INV_VAL, INV_VAL, INV_VAL};
	float gyro_dps[3] = {INV_VAL, INV_VAL, INV_VAL};
} DataIMU;

typedef struct
{
	const DataType type = DataType::dataRequestWaypointDescription;

	size_t waypointIndex;

} DataRequestWaypointDescription;

typedef struct
{
	const DataType type = DataType::dataReplyWaypointDescription;

	size_t waypointCount;
	size_t waypointIndex;
	float lat_deg = FLT_MAX;
	float lon_deg = FLT_MAX;
	float maxDistanceToPass_m = FLT_MAX;
	float durationToStay_s = FLT_MAX;
	float maxDistanceToStay_m = FLT_MAX;

} DataReplyWaypointDescription;
