/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommBase.h
 * Author: esppat
 *
 * Created on January 6, 2020, 10:35 PM
 */

#pragma once

#include "IntercommMessage.h"

#include "tools/physics/Units.h"
#include "tools/Lockable.h"
#include "tools/Lock.h"
#include "tools/Tools.h"

#include <map>
#include <string>

#include "tools/intercomm/zmq.hpp"


namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			class IntercommBase
			: public enable_shared_from_this<IntercommBase>
			, public virtual tools::Lockable
			{
			public:
			protected:

				IntercommBase(std::string  address, const unsigned socketType);
				IntercommBase(const IntercommBase& orig) = delete;
				virtual ~IntercommBase();

				template<typename T>
				void setsockopt(int option_, T const &optval)
				{
					LOCK(mutex());
					setsockopt(option_, &optval, sizeof (T));
				}

				template<typename T>
				T getsockopt(int option_) const
				{
					LOCK(mutex());
					T optval;
					size_t optlen = sizeof (T);
					getsockopt(option_, &optval, &optlen);
					return optval;
				}

				void setsockopt(int option_, const void * optval_, size_t optvallen_);
				void getsockopt(int option_, void *optval_, size_t *optvallen_) const;

				void connect();
				void disconnect();
				bool isConnected() const;

				void bind();

				zmq::socket_t * socket()
				{
					return m_socket;
				}

			protected:
			private:

				void createSocket();
				void destroySocket();

			private:

				zmq::socket_t * m_socket = nullptr;
				const std::string m_address;
				const unsigned m_socketType;

			};

		} // intercomm
	} // tools
} // mpasv
