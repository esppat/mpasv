/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommClient.h
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#pragma once

#include "IntercommBase.h"

#include <string>


namespace zmq
{
	class socket_t;
} // zmq

namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			class IntercommClient
			: public enable_shared_from_this<IntercommClient>
			, public IntercommBase
			{
			public:
				IntercommClient(const std::string& address);
				IntercommClient(const IntercommClient& orig) = delete;
				virtual ~IntercommClient();

				bool sendRequest(const IntercommMessage & request, IntercommMessage & reply);

			protected:
			protected:
			private:
			private:
			};

		} // intercomm
	} // tools
} // mpasv
