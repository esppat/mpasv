/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommContext.cpp
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#include "IntercommContext.h"

#include "tools/intercomm/zmq.hpp"


namespace mpasv::tools::intercomm
		{
			/*static*/ IntercommContext * IntercommContext::m_intercommContext = nullptr;
			/*static*/ std::mutex IntercommContext::m_intercommContextMutex;

			IntercommContext::IntercommContext()
				: m_context(1)
			{
			}

			IntercommContext::~IntercommContext()
			= default;

			/*static*/
			IntercommContext * IntercommContext::getIntercommContext()
			{
				if (m_intercommContext == nullptr)
				{
					m_intercommContextMutex.lock();
					if (m_intercommContext == nullptr)
					{
						m_intercommContext = new IntercommContext();
					}
					m_intercommContextMutex.unlock();
				}

				return m_intercommContext;
			}

			zmq::context_t & IntercommContext::getContext()
			{
				return m_context;
			}

		} // mpasv
