/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IntercommContext.h
 * Author: esppat
 *
 * Created on January 3, 2020, 4:33 PM
 */

#pragma once

#include <mutex>

#include "tools/intercomm/zmq.hpp"


namespace mpasv
{
	namespace tools
	{
		namespace intercomm
		{

			class IntercommContext
			{
			public:
				virtual ~IntercommContext();

				static IntercommContext * getIntercommContext();

				zmq::context_t & getContext();

			protected:
			protected:
			private:
				IntercommContext();
				IntercommContext(const IntercommContext& orig) = delete;

			private:
				static IntercommContext * m_intercommContext;
				static std::mutex m_intercommContextMutex;

				zmq::context_t m_context;

			};

		} // intercomm
	} // tools
} // mpasv
