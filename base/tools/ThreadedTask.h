/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ThreadedTask.h
 * Author: PEspie
 *
 * Created on November 20, 2018, 4:44 PM
 */

#pragma once

#include "tools/Task.h"

#include "tools/physics/Units.h"

#include <memory>
#include <mutex>
#include <thread>
#include <string>
#include <list>


namespace mpasv
{
	namespace tools
	{

		class ThreadedTask
				: public tools::Task
		{
		public:

			ThreadedTask(const std::string & taskName, bool canTakeMoreTimeThanAllowed = false);
			ThreadedTask(const ThreadedTask& orig) = delete;
			virtual ~ThreadedTask() = default;

			virtual bool started() const override;

			void setDelayBetweenRun(const second_t delayBetweenRuns);
			second_t delayBetweenRuns() const;

			void addPriorTask(const std::shared_ptr<ThreadedTask>& task);

		protected:

			virtual bool setup() noexcept override;
			virtual void initialize() noexcept override;
			virtual void run(const second_t tickDuration) noexcept = 0;
			virtual void finalize() noexcept override;

			bool stopRequested() const;

		private:

			ThreadedTask(const std::string & taskName, const second_t delayBetweenRuns, bool canTakeMoreTimeThanAllowed);

			static void s_run(ThreadedTask * threadedTask);

		private:

			bool m_threadStarted;
			bool m_stop_requested{};
			std::unique_ptr<std::thread> m_thread;

			second_t m_delayBetweenRuns;
			bool m_canTakeMoreTimeThanAllowed;

			static constexpr bool sm_doRealTime = true;

			std::list<std::shared_ptr<ThreadedTask>> m_priorTasks;

		};

	} // tools
} // mpasv
