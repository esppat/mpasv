/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Tools.h
 * Author: PEspie
 *
 * Created on November 20, 2018, 4:44 PM
 */

#include "tools/Tools.h"

#include <sstream>
#include <iostream>


namespace mpasv::tools
	{

		bool startsWith(const std::string & value, const std::string & starting)
		{
			if (starting.size() > value.size()) return false;
			bool result = std::equal(starting.begin(), starting.end(), value.begin());
			__TT();
			return result;
		}

		bool endsWith(const std::string & value, const std::string & ending)
		{
			if (ending.size() > value.size()) return false;
			bool result = std::equal(ending.rbegin(), ending.rend(), value.rbegin());
			__TT();
			return result;
		}

		void leftTrim(std::string &s)
		{
			s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch)
			{
				return !std::isspace(ch);
			}));
			__TT();
		}

		void rightTrim(std::string &s)
		{
			s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch)
			{
				return !std::isspace(ch);
			}).base(), s.end());
			__TT();
		}

		void trim(std::string &s)
		{
			leftTrim(s);
			rightTrim(s);
			__TT();
		}

		std::string leftTrimCopy(const std::string & s)
		{
			std::string scopy(s);
			leftTrim(scopy);
			__TT();
			return scopy;
		}

		std::string rightTrimCopy(const std::string & s)
		{
			std::string scopy(s);
			rightTrim(scopy);
			__TT();
			return scopy;
		}

		std::string trimCopy(const std::string & s)
		{
			std::string scopy(s);
			rightTrim(scopy);
			leftTrim(scopy);
			__TT();
			return scopy;
		}

		std::vector<std::string> split(const std::string & s, const char delimiter)
		{
			std::vector<std::string> result;
			std::istringstream f(s);
			char line[1024];
			while (f.getline(line, sizeof (line), delimiter))
			{
				result.emplace_back(line);
			}
			return result;
		}

		std::string getCurrentTime()
		{
			struct timeval tv{};
			time_t nowtime;
			struct tm *nowtm;
			char tmbuf[128]; char buf[256];

			gettimeofday(&tv, nullptr);
			nowtime = tv.tv_sec;
			nowtm = localtime(&nowtime);
			strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d-%H-%M-%S", nowtm);
			snprintf(buf, sizeof buf, "%s-%06ld", tmbuf, tv.tv_usec);

			return buf;
		}

	} // mpasv
