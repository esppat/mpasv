#pragma once

#include <mutex>

namespace mpasv
{
	namespace tools
	{

		class Lockable
		{
		public:

			Lockable()
			{
			}

			virtual ~Lockable()
			{
			}

			void lock() const
			{
				m_mutex.lock();
			}

			void unlock() const
			{
				m_mutex.unlock();
			}

			std::recursive_mutex & mutex() const
			{
				return m_mutex;
			}

		protected:
		protected:
		private:
		private:

			mutable std::recursive_mutex m_mutex;

		};

	} // tools
} // mpasv
