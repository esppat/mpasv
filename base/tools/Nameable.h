#pragma once

#include <string>


class Nameable
{
public:

	Nameable(const std::string & name);

	const std::string & name() const
	{
		return m_name;
	}

public:
protected:
protected:
private:
private:

	const std::string m_name;

};
