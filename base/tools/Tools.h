/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Tools.h
 * Author: PEspie
 *
 * Created on November 20, 2018, 4:44 PM
 */

#pragma once

#include "test_coverage/TestCoverage.h"

#include <string>
#include <algorithm>
#include <cctype>
#include <locale>
#include <memory>
#include <vector>
#include <sstream>
#include <iterator>

#include <sys/time.h>


namespace mpasv
{
	namespace tools
	{

		bool startsWith(const std::string & value, const std::string & starting);
		bool endsWith(const std::string & value, const std::string & ending);

		// trim from start (in place)
		void leftTrim(std::string &s);

		// trim from end (in place)
		void rightTrim(std::string &s);

		// trim from both ends (in place)
		void trim(std::string &s);

		// trim from start (copying)
		std::string leftTrimCopy(const std::string & s);

		// trim from end (copying)
		std::string rightTrimCopy(const std::string & s);

		// trim from both ends (copying)
		std::string trimCopy(const std::string & s);

		std::vector<std::string> split(const std::string & s, const char delimiter);

		// formatted now time
		std::string getCurrentTime();

		template <class Container>
		void split(const std::string & str, Container & cont, char delim)
		{
			std::stringstream ss(str);
			std::string token;
			while (std::getline(ss, token, delim))
			{
				cont.push_back(token);
			}
		}

		template <class T>
		void setInLimit(T & variable, const T minValue, const T maxValue)
		{
			if (variable < minValue)
			{
				variable = minValue;
			}
			else if (variable > maxValue)
			{
				variable = maxValue;
			}
		}

		class enable_shared_from_this_base : public std::enable_shared_from_this<enable_shared_from_this_base>
		{
		public:

			virtual ~enable_shared_from_this_base()
			{
			}
		};

		template<typename T>
		class enable_shared_from_this : public virtual enable_shared_from_this_base
		{
		public:

			std::shared_ptr<T> shared_from_this()
			{
				return std::dynamic_pointer_cast<T>(enable_shared_from_this_base::shared_from_this());
			}
		};

	} // tools
} // mpasv


#define DECL_NAMED_MEMBER_GET_SET(namespace, GetterName, UpperCaseClassName, LowerCaseClassName) \
	public: \
	std::shared_ptr<namespace::UpperCaseClassName> get##GetterName(); \
	std::shared_ptr<const namespace::UpperCaseClassName> get##GetterName() const; \
	void set##GetterName(std::shared_ptr<namespace::UpperCaseClassName> LowerCaseClassName); \
	private: \
	std::shared_ptr<namespace::UpperCaseClassName> m_##LowerCaseClassName


#define DEF_NAMED_MEMBER_GET_SET(thisClass, namespace, GetterName, UpperCaseClassName, LowerCaseClassName) \
	std::shared_ptr<namespace::UpperCaseClassName> thisClass::get##GetterName() \
{ \
	CHECK_NOT_NULLPTR(m_##LowerCaseClassName); \
	return m_##LowerCaseClassName; \
	} \
	std::shared_ptr<const namespace::UpperCaseClassName> thisClass::get##GetterName() const \
{ \
	CHECK_NOT_NULLPTR(m_##LowerCaseClassName); \
	return m_##LowerCaseClassName; \
	} \
	void thisClass::set##GetterName(std::shared_ptr<namespace::UpperCaseClassName> LowerCaseClassName) \
{ \
	CHECK_NOT_NULLPTR(LowerCaseClassName); \
	m_##LowerCaseClassName = LowerCaseClassName; \
	}

#define DECL_MEMBER_GET_SET(namespace, UpperCaseClassName, LowerCaseClassName) \
	DECL_NAMED_MEMBER_GET_SET(namespace, UpperCaseClassName, UpperCaseClassName, LowerCaseClassName)

#define DEF_MEMBER_GET_SET(thisClass, namespace, UpperCaseClassName, LowerCaseClassName) \
	DEF_NAMED_MEMBER_GET_SET(thisClass, namespace, UpperCaseClassName, UpperCaseClassName, LowerCaseClassName)

// -------------------------------------

#define _DEF_SET(name, upperName, type) \
	void set##upperName(const type & value) \
{ \
	m_##name = value; \
	}

#define _DEF_SET_WITH_LIMIT(name, upperName, type, low, hight) \
	void set##upperName(const type & value) \
{ \
	CHECK_GE(value, low); \
	CHECK_LE(value, hight); \
	m_##name = value; \
	}

#define _DEF_SET_ISVALID(name, upperName, type) \
	void set##upperName(const type & value) \
{ \
	CHECK_TRUE(value.isValid()); \
	m_##name = value; \
	}

#define _DEF_SET_WITH_HISTO(name, upperName, type) \
	void set##upperName(const type & value) \
{ \
	m_##name = value; \
	pushBack##upperName##History(value); \
	}

#define _DEF_SET_WITH_HISTO_WITH_LIMIT(name, upperName, type, low, hight) \
	void set##upperName(const type & value) \
{ \
	CHECK_GE(value, low); \
	CHECK_LE(value, hight); \
	m_##name = value; \
	pushBack##upperName##History(value); \
	}

#define _DEF_HAS_AND_GET(name, upperName, type) \
	bool has##upperName() const \
{ \
	return m_##name.isSet(); \
	} \
	type get##upperName() const \
{ \
	return m_##name; \
	} \
	void unset##upperName() \
{ \
	m_##name.unset(); \
	}

#define _DEF_HAS_AND_GET_WITH_HISTO(name, upperName, type) \
	_DEF_HAS_AND_GET(name, upperName, type) \
	void pushBack##upperName##History(const type value) \
{ \
	m_##name##History.push_back(value); \
	} \
	bool is##upperName##ContantFor(const second_t duration, const type absMaxVariation) const \
{ \
	return m_##name##History.isConstantFor(duration, absMaxVariation); \
	} \
	void reset##upperName##History() \
{ \
	m_##name##History.reset(); \
	}

#define _DEF_MEMBER(name, type) \
	tools::SetUnsetValue<type> m_##name;

#define _DEF_MEMBER_WITH_HISTO(name, type) \
	tools::SetUnsetValue<type> m_##name; \
	tools::physics::ValueHistory<type> m_##name##History;

// -------------------------------------

#define ENSURE_TRUE(s) do { if (!(s)) LOG_FATAL(#s, " should be true"); } while (false);

template <typename T>
T sqr(const T & t)
{
	return t * t;
}
