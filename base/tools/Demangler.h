/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Demangler.h
 * Author: PEspie
 *
 * Created on February 19, 2019, 9:55 AM
 */

#pragma once

#include <string>
#include <map>
#include <memory>


namespace mpasv
{
	namespace tools
	{

		class Demangler
		{
		public:

			template<typename TYPE>
			static const std::string & _typeName()
			{
				const char * typeIdName = typeid(TYPE).name();
				auto search = m_names.find(typeIdName);
				if (search != m_names.end())
				{
					return search->second;
				}
				else
				{
					std::string name;
					demangle(typeIdName, name);
					name = name.substr(name.find_last_of(':') + 1);
					m_names[typeIdName] = name;
					return m_names[typeIdName];
				}
			}

			template<typename TYPE>
			static const std::string & _typeName(const TYPE &)
			{
				return _typeName<TYPE>();
			}

		private:

			Demangler()
			{
			}

			Demangler(const Demangler& orig) = delete;

			virtual ~Demangler()
			{
			}

			static void demangle(const char * name, std::string & result);

		private:

			static std::map<const char *, std::string> m_names;

		};

	} // tools
} // mpasv

template <typename TYPE>
const std::string & typeName()
{
	return mpasv::tools::Demangler::_typeName<TYPE>();
}

template <typename TYPE>
const std::string & typeName(const TYPE &)
{
	return mpasv::tools::Demangler::_typeName<TYPE>();
}


template <typename TYPE>
const std::string & typeName(const TYPE *)
{
	return mpasv::tools::Demangler::_typeName<TYPE>();
}

template <typename TYPE>
const std::string & typeName(const std::shared_ptr<TYPE> &)
{
	return mpasv::tools::Demangler::_typeName<TYPE>();
}
