#include "tools/Logger.h"

#include "tools/Lock.h"

#include "tools/Exception.h"
#include "tools/gps/GeoPos.h"

#include "tools/routing/Route.h"

#include <iostream>
#include <sstream>
#include <iomanip>


namespace mpasv::routing
	{

		Route::Route()
		= default;

		Route::~Route()
		= default;

		void Route::createRouteWithOneWaypoint(const std::string & name, const tools::geo::GeoPos & geoPos)
		{
			CHECK_TRUE(!name.empty());
			CHECK_TRUE(geoPos.isValid());

			m_name = name;
			m_waypoints.resize(0);

			Waypoint waypoint(geoPos, 5_m, 0_m, std::string("WP_1"), -1_s);

			LOG_DEBUG("Way Point ", waypoint.getName(),
					", Latitude: ", std::setprecision(12), waypoint.pos().lat(),
					", Longitude: ", waypoint.pos().lon(),
					", distance from start: ", waypoint.getDistanceFromStart(),
					", Duration to stay: ", waypoint.getDurationToStay());
			m_waypoints.push_back(waypoint);
		}

		void Route::loadMainWaypointsFromFile(const std::string & file_name)
		{
			const std::string full_file_name = "data/routes/" + file_name + ".xml";
			std::ifstream file(full_file_name, std::ifstream::in);

			if (!file.is_open())
			{
				// TODO
				return;
			}
			std::stringstream strStream;
			strStream << file.rdbuf();
			std::string content = strStream.str();

			std::string open_tag = "<name>";
			std::string close_tag = "</name>";

			int pos1 = content.find(open_tag);
			int pos2 = content.find(close_tag);

			if ((pos1 != -1) && (pos2 != -1))
			{
				m_name = content.substr(pos1 + open_tag.length(), pos2 - (pos1 + open_tag.length()));
			}
			if (m_name.length() == 0)
			{
				m_name = file_name;
			}

			open_tag = "<coordinates>";
			close_tag = "</coordinates>";

			pos1 = content.find(open_tag);
			pos2 = content.find(close_tag);
			if ((pos1 != -1) && (pos2 != -1))
			{
				content = content.substr(pos1 + open_tag.length() + 1, pos2 - (pos1 + open_tag.length()));

				meter_t distance_from_start = 0.0_m;

				std::istringstream lines(content);
				std::string line;
				unsigned waypointCount = 0;

				LOCK(tools::logger.mutex());

				while (getline(lines, line, '\n'))
				{
					tools::leftTrim(line);
					if (line[0] == '<')
						break;

					std::stringstream ss(line);
					double dbl;

					ss >> dbl;
					degree_t longitude(dbl);

					char c;
					ss >> c; // skip coma

					ss >> dbl;
					degree_t latitude(dbl);

					ss >> c; // skip coma

					ss >> dbl;
					second_t durationToStay(dbl);

					tools::geo::GeoPos pos(latitude, longitude);
					if (!m_waypoints.empty())
					{
						Waypoint prev_wp = m_waypoints[m_waypoints.size() - 1];
						distance_from_start += prev_wp.pos().distanceTo(pos);
					}

					Waypoint waypoint(pos, 5_m, distance_from_start, std::string("WP") + std::to_string(waypointCount), durationToStay);

					LOG_DEBUG("Way Point ", waypoint.getName(),
							", Latitude: ", std::setprecision(12), waypoint.pos().lat(),
							", Longitude: ", waypoint.pos().lon(),
							", distance from start: ", waypoint.getDistanceFromStart(),
							", Duration to stay: ", waypoint.getDurationToStay());
					m_waypoints.push_back(waypoint);

					++waypointCount;
				}

				m_waypoints[waypointCount - 1].setDurationToStay(-1_s);
				m_waypoints[waypointCount - 1].setMaxDistanceToStay(10_m);
			}
		}

	} // mpasv
