#pragma once

#include "tools/gps/GeoPos.h"

#include <string>

namespace mpasv
{
	namespace routing
	{

		class Waypoint
		{
		public:
			Waypoint();
			Waypoint(const tools::geo::GeoPos & pos,
					const meter_t max_distance_to_pass_m,
					const meter_t distance_from_start_m,
					const std::string & name,
					const second_t durationToStay);
			~Waypoint();

			tools::geo::GeoPos pos() const
			{
				return m_pos;
			}

			meter_t getMaxDistanceToPass() const;

			meter_t getDistanceFromStart() const;

			std::string getName() const;

			bool isValid() const;

			second_t getDurationToStay() const;
			void setDurationToStay(const second_t durationToStay);

			meter_t getMaxDistanceToStay() const;
			void setMaxDistanceToStay(const meter_t maxDistanceToStay);

		private:
			tools::geo::GeoPos m_pos;
			meter_t m_max_distance_to_pass{};
			std::string m_name;
			meter_t m_distance_from_start{};
			bool m_valid;
			second_t m_durationToStay{};
			meter_t m_maxDistanceToStay{};
		};

	} //routing
} // mpasv
