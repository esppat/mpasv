#include "tools/routing/Waypoint.h"

#include "tools/Exception.h"

namespace mpasv::routing
	{

		Waypoint::Waypoint()
		{
			m_valid = false;
		}

		Waypoint::Waypoint(const tools::geo::GeoPos & pos,
						const meter_t max_distance_to_pass,
						const meter_t distance_from_start,
						const std::string & name,
						const second_t durationToStay)
		{
			m_pos = pos;

			CHECK_TRUE(max_distance_to_pass > 1.0_m);
			m_max_distance_to_pass = max_distance_to_pass;
			m_distance_from_start = distance_from_start;
			m_name = name;
			m_valid = true;
			m_durationToStay = durationToStay;
			m_maxDistanceToStay = 1.0_m; // TODO
		}

		Waypoint::~Waypoint()
		= default;

		meter_t Waypoint::getMaxDistanceToPass() const
		{
			return m_max_distance_to_pass;
		}

		meter_t Waypoint::getDistanceFromStart() const
		{
			return m_distance_from_start;
		}

		std::string Waypoint::getName() const
		{
			return m_name;
		}

		bool Waypoint::isValid() const
		{
			return m_valid;
		}

		second_t Waypoint::getDurationToStay() const
		{
			return m_durationToStay;
		}

		void Waypoint::setDurationToStay(const second_t durationToStay)
		{
			m_durationToStay = durationToStay;
		}

		meter_t Waypoint::getMaxDistanceToStay() const
		{
			return m_maxDistanceToStay;
		}

		void Waypoint::setMaxDistanceToStay(const meter_t maxDistanceToStay)
		{
			m_maxDistanceToStay = maxDistanceToStay;
		}

	} // mpasv
