#pragma once

#include "tools/Tools.h"

#include "tools/gps/GeoPos.h"

#include "tools/routing/Waypoint.h"

#include <string>
#include <vector>


namespace mpasv
{
	namespace routing
	{

		class Route : public tools::enable_shared_from_this<Route>
		{
		public:

			Route();
			~Route();

			void createRouteWithOneWaypoint(const std::string & name, const tools::geo::GeoPos & geoPos);

			void loadMainWaypointsFromFile(const std::string & file_name);

			size_t getWaypointCount() const
			{
				return m_waypoints.size();
			}

			const Waypoint & getWaypoint(const size_t index) const
			{
				return m_waypoints[index];
			}

			const std::vector<Waypoint> & getWaypoints() const
			{
				return m_waypoints;
			}

			meter_t getLength() const
			{
				return getWaypoint(getWaypointCount() - 1).getDistanceFromStart();
			}

			std::string getName() const
			{
				return m_name;
			}

		private:

			std::string m_name;
			std::vector<Waypoint> m_waypoints;
		};

	} //routing
} // mpasv
