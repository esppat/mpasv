/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ThreadedTask.cpp
 * Author: PEspie
 *
 * Created on November 20, 2018, 4:44 PM
 */

#include "tools/ThreadedTask.h"

#include "tools/Tools.h"
#include "tools/Lock.h"
#include "tools/Logger.h"
#include "tools/Exception.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/Demangler.h"
#include "tools/physics/SClock.h"

#include <memory>
#include "tools/test_coverage/TestCoverage.h"


namespace mpasv::tools
{
	ThreadedTask::ThreadedTask(const std::string & taskName, bool canTakeMoreTimeThanAllowed)
		: tools::ThreadedTask(taskName, param<millisecond_t, double>("ThreadedTask.Delay between runs (ms)"), canTakeMoreTimeThanAllowed)
	{
	}

	/* private */
	ThreadedTask::ThreadedTask(const std::string & taskName, const second_t delayBetweenRuns, bool canTakeMoreTimeThanAllowed)
		: Task(taskName)
		, m_threadStarted(false)
		, m_delayBetweenRuns(delayBetweenRuns)
		, m_canTakeMoreTimeThanAllowed(canTakeMoreTimeThanAllowed)
	{
	}

	bool ThreadedTask::started() const
	{
		return m_threadStarted && Task::started();
	}

	void ThreadedTask::setDelayBetweenRun(const second_t delayBetweenRuns)
	{
		LOCK(mutex());

		m_delayBetweenRuns = delayBetweenRuns;
	}

	second_t ThreadedTask::delayBetweenRuns() const
	{
		return m_delayBetweenRuns;
	}

	void ThreadedTask::addPriorTask(const std::shared_ptr<ThreadedTask>& task)
	{
		LOCK(mutex());

		CHECK_FALSE(started());
		CHECK_EQUAL(std::find(m_priorTasks.begin(), m_priorTasks.end(), task), m_priorTasks.end());
		m_priorTasks.push_back(task);
	}

	bool ThreadedTask::setup() noexcept
	{
		if (!Task::setup())
		{
			return false;
		}

		bool result = false;

		try
		{
			if (!m_priorTasks.empty())
			{
				for (const auto& task : m_priorTasks)
				{
					task->start();
				}
			}

			result = true;
		}
		catch (std::exception & ex)
		{
			LOG_ERR("Caught exception: ", ex.what(), " in ", typeName(this));
		}

		return result;
	}

	void ThreadedTask::initialize() noexcept
	{
		{
			LOCK(mutex());
			ENSURE_TRUE(m_thread == nullptr);
			m_stop_requested = false;

			std::string paramName = name() + ".Delay between runs (ms)";
			if (hasParam(paramName))
			{
				setDelayBetweenRun(param<millisecond_t, double>(paramName));
			}
			else
			{
				setDelayBetweenRun(param<millisecond_t, double>("ThreadedTask.Delay between runs (ms)"));
			}

			m_thread = std::make_unique<std::thread>(ThreadedTask::s_run, this);
		}

		// TODO add timeout
		while (!m_threadStarted)
		{
			sleepFor(1_ms);
		}

		__TT();
	}

	void ThreadedTask::finalize() noexcept
	{
		LOCK(mutex());
		ENSURE_TRUE(started());
		ENSURE_TRUE(m_thread != nullptr);

		m_stop_requested = true;

		mutex().unlock();
		m_thread->join();
		mutex().lock();

		ENSURE_TRUE(!started());
		m_thread.reset(nullptr);

		if (!m_priorTasks.empty())
		{
			for (auto task = m_priorTasks.rbegin(); task != m_priorTasks.rend(); task++)
			{
				(
				*task)->stop();
			}
		}
	}

	bool ThreadedTask::stopRequested() const
	{
		return m_stop_requested;
	}

	void ThreadedTask::s_run(ThreadedTask * threadedTask)
	{
		CHECK_NOT_NULLPTR(threadedTask);
		CHECK_FALSE(threadedTask->started());

		try
		{
			threadedTask->m_threadStarted = true;

			try
			{
				do
				{
					auto cycleBegin = theClock.now();

					try
					{
						LOCK(threadedTask->mutex());
						threadedTask->run(threadedTask->m_delayBetweenRuns);
					}
					catch (std::exception & ex)
					{
						LOG_ERR("Caught exception: ", ex.what(), " in ", typeName(threadedTask));
					}

					auto cycleEnd = theClock.now();
					second_t cycleDuration = cycleEnd - cycleBegin;

					if (cycleDuration / 1.1 > threadedTask->m_delayBetweenRuns && !threadedTask->m_canTakeMoreTimeThanAllowed)
					{
						LOG_WARN("Cycle duration ",
								 typeName(threadedTask),
								 " longer than delay between runs: ", cycleDuration,
								 " instead of ", threadedTask->m_delayBetweenRuns);
					}
					else
					{
						if (cycleDuration < threadedTask->m_delayBetweenRuns && sm_doRealTime)
						{
							do
							{
								tools::physics::sleepFor(1_ns);

								cycleEnd = theClock.now();
								cycleDuration = cycleEnd - cycleBegin;
							}
							while (cycleDuration < threadedTask->m_delayBetweenRuns);
						}
					}
				}
				while (!threadedTask->stopRequested());
			}
			catch (std::exception & ex)
			{
				LOG_ERR("Caught exception: ", ex.what(), " in ", typeName(threadedTask));
			}

			threadedTask->m_threadStarted = false;
		}
		catch (std::exception & ex)
		{
			LOG_ERR("Caught exception: ", ex.what(), " in ", typeName(threadedTask));
		}

		__TT();
	}

} // mpasv
