/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ConfigurationException.h
 * Author: esppat
 *
 * Created on 5 décembre 2018, 21:48
 */

#pragma once

#include <string>

namespace mpasv
{
	namespace tools
	{

		class Exception : public std::exception
		{
		public:

			Exception(const std::string msg)
			{
				m_msg = msg;
			}

			virtual const char* what() const noexcept override
			{
				return m_msg.c_str();
			}

		private:

			std::string m_msg;
		};

	} // tools
} // mpasv

#define CHECK_EQUAL(b,c) do { if ((b) != (c)) throw mpasv::tools::Exception(std::string(#b) + " should be equal to " + #c); } while (false);
#define CHECK_EQUAL_MSG(b,c,m) do { if ((b) != (c)) throw mpasv::tools::Exception(m); } while (false);

#define CHECK_NOT_EQUAL(b,c) do { if ((b) == (c)) throw mpasv::tools::Exception(#b + std::string(" should not be equal to ") + #c); } while (false);
#define CHECK_NOT_EQUAL_MSG(b,c,m) do { if ((b) == (c)) throw mpasv::tools::Exception((m)); } while (false);

#define CHECK_GT(b,c) do { if ((b) <= (c)) throw mpasv::tools::Exception(#b + std::string(" should be greater than ") + #c); } while (false);
#define CHECK_GT_MSG(b,c,m) do { if ((b) <= (c)) throw mpasv::tools::Exception((m)); } while (false);

#define CHECK_GE(b,c) do { if ((b) < (c)) throw mpasv::tools::Exception(#b + std::string(" should be greater than or equal to ") + #c); } while (false);
#define CHECK_GE_MSG(b,c,m) do { if ((b) < (c)) throw mpasv::tools::Exception((m)); } while (false);

#define CHECK_LT(b,c) do { if ((b) >= (c)) throw mpasv::tools::Exception(#b + std::string(" should be lower than ") + #c); } while (false);
#define CHECK_LT_MSG(b,c,m) do { if ((b) >= (c)) throw mpasv::tools::Exception((m)); } while (false);

#define CHECK_LE(b,c) do { if ((b) > (c)) throw mpasv::tools::Exception(#b + std::string(" should be lower than or equal to ") + #c); } while (false);
#define CHECK_LE_MSG(b,c,m) do { if ((b) > (c)) throw mpasv::tools::Exception((m)); } while (false);

#define CHECK_EMPTY(e) do { if (!(e).isEmpty()) throw mpasv::tools::Exception(#e + std::string(" should be empty")); } while (false);
#define CHECK_NOT_EMPTY(e) do { if ((e).isEmpty()) throw mpasv::tools::Exception(#e + std::string(" should not be empty")); } while (false);

#define CHECK_VALID_BEARING(b) do { if ((b) < 0.0_deg || (b) >= 360.0_deg) throw mpasv::tools::Exception(#b + std::string(" is not a valid bearing ") + std::to_string((double)b)); } while (false);

#define CHECK_CONTAINS(c, e) do { if ((c.find(e) == c.end())) throw mpasv::tools::Exception(#c + std::string(" should contain ") + #e + "=" + std::string(e)); } while (false);
#define CHECK_NOT_CONTAINS(c, e) do { if ((c.find(e) != c.end())) throw mpasv::tools::Exception(#c + std::string(" should not contain ") + #e + "=" + std::string(e)); } while (false);

#define CHECK_TRUE(b) CHECK_EQUAL(b, true)
#define CHECK_FALSE(b) CHECK_EQUAL(b, false)
#define CHECK_NULLPTR(p) CHECK_EQUAL(p, nullptr)
#define CHECK_NULLPTR_MSG(p,m) CHECK_EQUAL_MSG(p, nullptr, m)
#define CHECK_NOT_NULLPTR(p) CHECK_NOT_EQUAL(p, nullptr)
#define CHECK_NOT_NULLPTR_MSG(p,m) CHECK_NOT_EQUAL_MSG(p, nullptr, m)

#define THROW_ALWAYS(message) throw mpasv::tools::Exception(message);
