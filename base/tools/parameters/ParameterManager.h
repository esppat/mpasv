#pragma once

#include "tools/Lock.h"
#include "tools/Tools.h"
#include "tools/ThreadedTask.h"

#include <vector>
#include <mutex>
#include <iostream>

#include <time.h>

#include <nlohmann/json.hpp>


namespace mpasv
{
	namespace tools
	{
		namespace parameters
		{

			class ParameterManager
			{
			public:

				static void setParameterFileName(const std::string & fileName);

				template <typename T, typename U = T>
				T getParameter(const std::string & path)
				{
					LOCK(sm_parameterManagerMutex);
					return T(getParameterValue(path).get<U>());
				}

				template <typename T, typename U = T>
				void setParameter(const std::string & path, const T & valueToSet, bool mustSave = false)
				{
					LOCK(sm_parameterManagerMutex);

					std::vector<std::string> keys;
					split(path, keys, '.');

					nlohmann::json * value = &m_state;

					for (std::string & key : keys)
					{
						if (value->count(key) == 0)
						{
							(*value)[key] = {};
						}

						value = &((*value)[key]);
					}

					*value = (U) valueToSet;

					if (mustSave)
						save();
				}

				bool hasParameter(const std::string & path) const;

				static tools::parameters::ParameterManager * get();

				void load();
				void save();

			private:

				ParameterManager();
				virtual ~ParameterManager() = default;

				nlohmann::json & getParameterValue(const std::string & path);
				nlohmann::json & getOrCreateParameterValue(const std::string & path);

			private:

				static std::string sm_fileName;
				static std::recursive_mutex sm_parameterManagerMutex;
				static tools::parameters::ParameterManager * sm_parameterManager;

				std::string m_fileName;
				nlohmann::json m_state;

			};

		} // parameters
	} // tools
} // mpasv

template<class Type, class UnderlyingType = Type>
Type param(const std::string & path)
{
	return mpasv::tools::parameters::ParameterManager::get()->getParameter<Type, UnderlyingType>(path);
}

template<class Type, class UnderlyingType = Type>
Type param(const char * path)
{
	return param<Type, UnderlyingType>(std::string(path));
}

template<class Type, class UnderlyingType = Type>
void setParam(const std::string & path, const Type & value, bool mustSave = false)
{
	mpasv::tools::parameters::ParameterManager::get()->setParameter<Type, UnderlyingType>(path, value, mustSave);
}

template<class Type, class UnderlyingType = Type>
void setParam(const char * path, const Type & value, bool mustSave = false)
{
	setParam<Type, UnderlyingType>(std::string(path), value, mustSave);
}

inline bool hasParam(const std::string & path)
{
	return mpasv::tools::parameters::ParameterManager::get()->hasParameter(path);
}

inline bool hasParam(const char * path)
{
	return hasParam(std::string(path));
}
