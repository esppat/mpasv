#include "tools/parameters/ParameterManager.h"
#include "tools/Logger.h"
#include "tools/Lock.h"
#include "tools/Tools.h"

#include <fstream>

#include "sys/stat.h"
#include <ctime>

#include "tools/Exception.h"

namespace mpasv::tools::parameters
{
	/*static*/
	std::string ParameterManager::sm_fileName = "data/parameters/parameters.json";

	/*static*/
	std::recursive_mutex ParameterManager::sm_parameterManagerMutex;

	/*static*/
	tools::parameters::ParameterManager * ParameterManager::sm_parameterManager = nullptr;

	void ParameterManager::setParameterFileName(const std::string & fileName)
	{
		LOCK(sm_parameterManagerMutex);
		sm_fileName = fileName;
		if (sm_parameterManager != nullptr)
		{
			delete sm_parameterManager;
			sm_parameterManager = nullptr;
		}
	}

	bool ParameterManager::hasParameter(const std::string & path) const
	{
		LOCK(sm_parameterManagerMutex);

		std::vector<std::string> keys;
		split(path, keys, '.');

		const nlohmann::json * value = &m_state;

		for (const std::string & key : keys)
		{
			if (value->count(key) == 0)
			{
				return false;
			}

			value = &((*value)[key]);
		}

		return true;
	}

	/*static*/
	tools::parameters::ParameterManager * ParameterManager::get()
	{
		// no double check to avoid race condition with setParameterFileName

		LOCK(sm_parameterManagerMutex);
		if (sm_parameterManager == nullptr)
		{
			sm_parameterManager = new tools::parameters::ParameterManager();
		}

		return sm_parameterManager;
	}

	void ParameterManager::load()
	{
		LOCK(sm_parameterManagerMutex);
		std::ifstream file(m_fileName, std::ifstream::in);
		if (file.is_open())
		{
			file >> m_state;
			LOG_DEBUG("Parameters loaded");
		}
		else
		{
			LOG_ERR("Cannot open parameters file ", m_fileName, " in read mode");
		}
	}

	void ParameterManager::save()
	{
		LOCK(sm_parameterManagerMutex);
		std::ofstream file(m_fileName, std::ofstream::out);
		if (file.is_open())
		{
			file << std::setw(4) << m_state;
			LOG_DEBUG("Parameters saved");
		}
		else
		{
			LOG_ERR("Cannot open parameters file ", m_fileName, " in write mode");
		}
	}

	ParameterManager::ParameterManager()
		: m_fileName(sm_fileName)
	{
		try
		{
			load();
		}
		catch (std::exception & ex)
		{
			LOG_ERR("Exception caught while parsing file ", m_fileName, ": ", ex.what());
			throw ex;
		}
	}

	nlohmann::json & ParameterManager::getParameterValue(const std::string & path)
	{
		std::vector<std::string> keys;
		split(path, keys, '.');

		nlohmann::json * value = &m_state;

		for (std::string & key : keys)
		{
			try
			{
				value = &((*value)[key]);
			}
			catch (nlohmann::detail::type_error & ex)
			{
				THROW_ALWAYS("Error while getting parameter " + key + " in " + path + ": " + ex.what());
			}
		}

		return *value;
	}

	nlohmann::json & ParameterManager::getOrCreateParameterValue(const std::string & path)
	{
		std::vector<std::string> keys;
		split(path, keys, '.');

		nlohmann::json * value = &m_state;

		for (std::string & key : keys)
		{
			try
			{
				if (value->count(key) == 0)
				{
					(*value)[key] = {};
				}

				value = &((*value)[key]);
			}
			catch (nlohmann::detail::type_error & ex)
			{
				THROW_ALWAYS("Error while getting or creating parameter " + key + " in " + path + ": " + ex.what());
			}
		}

		return *value;
	}

} // mpasv
