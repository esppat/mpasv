#include "Lock.h"

#ifndef __DEBUG__
#else

#include <cassert>

unsigned Locker::m_nblock = 0;
std::recursive_mutex Locker::m_mutex;

Locker::Locker(const char * file, const unsigned line, const char * func, std::recursive_mutex & mutex)
{
	m_file = file;
	m_line = line;
	m_func = func;

	std::cout << file << "(" << line << ") " << func << ": Locker() " << m_nblock << "\r\n";
	std::cout.flush();
	m_nblock++;

	std::lock(m_mutex, mutex);
	m_lock1 = new std::lock_guard<std::recursive_mutex>(mutex, std::adopt_lock);
	m_mutex.unlock();
}

Locker::Locker(const char * file, const unsigned line, const char * func, std::recursive_mutex & mutex1, std::recursive_mutex & mutex2)
{
	m_file = file;
	m_line = line;
	m_func = func;

	std::cout << file << "(" << line << ") " << func << ": Locker() " << m_nblock << "\r\n";
	std::cout.flush();
	m_nblock++;

	std::lock(m_mutex, mutex1, mutex2);
	m_lock1 = new std::lock_guard<std::recursive_mutex>(mutex1, std::adopt_lock);
	m_lock2 = new std::lock_guard<std::recursive_mutex>(mutex2, std::adopt_lock);
	m_mutex.unlock();
}

Locker::~Locker()
{
	m_mutex.lock();
	assert(m_nblock > 0);
	m_nblock--;
	std::cout << m_file << "(" << m_line << ") " << m_func << ": ~Locker() " << m_nblock << "\r\n";
	std::cout.flush();

	if (m_lock1 != nullptr)
		delete m_lock1;

	if (m_lock2 != nullptr)
		delete m_lock2;
	m_mutex.unlock();
}

#endif // __DEBUG__
