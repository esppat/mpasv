/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Task.cpp
 * Author: esppat
 *
 * Created on 21 décembre 2018, 15:37
 */

#include "tools/Task.h"
#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/Demangler.h"

#include <utility>


namespace mpasv::tools
	{

		Task::Task(std::string  taskName)
			: Nameable(taskName)
			, m_initialized(false)
			, m_standby(false)
		{
		}

		Task::~Task()
		{
			if (started())
			{
				stop();
			}
		}

		bool Task::start(const bool standBy)
		{
			bool result = false;

			LOCK(mutex());
			CHECK_FALSE(started());

			if (standBy)
			{
				standby();
			}

			LOG_DEBUG(name(), ": starting");

			if (setup())
			{
				initialize();
				m_initialized = true;
				LOG_DEBUG(name(), ": started");
				result = true;
			}
			else
			{
				LOG_DEBUG(name(), ": start cancelled");
			}

			return result;
		}

		void Task::stop()
		{
			LOCK(mutex());

			CHECK_TRUE(started());

			LOG_DEBUG(name(), ": stopping");

			finalize();
			m_initialized = false;

			LOG_DEBUG(name(), ": stopped");
		}

		bool Task::started() const
		{
			return m_initialized;
		}

		void Task::standby()
		{
			LOCK(mutex());

			CHECK_FALSE(isOnStandby());
			m_standby = true;
		}

		void Task::endOfStandby()
		{
			LOCK(mutex());

			CHECK_TRUE(isOnStandby());
			m_standby = false;
		}

		bool Task::isOnStandby() const
		{
			LOCK(mutex());

			return m_standby;
		}

		bool Task::setup() noexcept
		{
			return true;
		}

	} // mpasv
