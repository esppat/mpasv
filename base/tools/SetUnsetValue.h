/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SetUnsetValue.h
 * Author: esppat
 *
 * Created on 6 décembre 2018, 00:48
 */

#pragma once

#include "tools/Exception.h"


namespace mpasv
{
	namespace tools
	{

		template<typename T>
		class SetUnsetValue
		{
		public:

			SetUnsetValue()
				: m_set(false)
			{
			}

			SetUnsetValue(const T & value)
				: m_set(true)
				, m_value(value)
			{
			}

			SetUnsetValue<T> & operator=(const T & value)
			{
				m_set = true;
				m_value = value;

				return *this;
			}

			T & operator()()
			{
				return get();
			}

			T operator()() const
			{
				return get();
			}

			operator T &()
			{
				return get();
			}

			operator const T &() const
			{
				return get();
			}

			T & get()
			{
				CHECK_TRUE(isSet());
				return m_value;
			}

			const T & get() const
			{
				CHECK_TRUE(isSet());
				return m_value;
			}

			T * operator->()
			{
				return &get();
			}

			const T * operator->() const
			{
				return &get();
			}

			T operator++(int) // postfix
			{
				return m_value++;
			}

			T operator++() // prefix
			{
				return ++m_value;
			}

			T operator--(int) // postfix
			{
				return m_value--;
			}

			T operator--() // prefix
			{
				return --m_value;
			}

			void unset()
			{
				m_set = false;
			}

			bool isSet() const
			{
				return m_set;
			}

			T operator+(const T & t)
			{
				return get() + t.get();
			}

			T operator-(const T & t)
			{
				return get() - t.get();
			}

		private:

			bool m_set;
			T m_value;
		};

		// simple operation

		template<class T, class U>
		bool operator+(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() + b;
		}

		template<class T, class U>
		bool operator-(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() - b;
		}

		template<class T, class U>
		bool operator*(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() * b;
		}

		template<class T, class U>
		bool operator/(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() / b;
		}

		template<class T, class U>
		bool operator+(const U & b, const SetUnsetValue<T> & a)
		{
			return b + a.get();
		}

		template<class T, class U>
		bool operator-(const U & b, const SetUnsetValue<T> & a)
		{
			return b - a.get();
		}

		template<class T, class U>
		bool operator*(const U & b, const SetUnsetValue<T> & a)
		{
			return b * a.get();
		}

		template<class T, class U>
		bool operator/(const U & b, const SetUnsetValue<T> & a)
		{
			return b / a.get();
		}

		// comparison

		template<class T, class U>
		bool operator>(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() > b;
		}

		template<class T, class U>
		bool operator>=(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() >= b;
		}

		template<class T, class U>
		bool operator<(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() < b;
		}

		template<class T, class U>
		bool operator<=(const SetUnsetValue<T> & a, const U & b)
		{
			return a.get() <= b;
		}

		template<class T, class U>
		bool operator>(const U & b, const SetUnsetValue<T> & a)
		{
			return b > a.get();
		}

		template<class T, class U>
		bool operator>=(const U & b, const SetUnsetValue<T> & a)
		{
			return b >= a.get();
		}

		template<class T, class U>
		bool operator<(const U & b, const SetUnsetValue<T> & a)
		{
			return b < a.get();
		}

		template<class T, class U>
		bool operator<=(const U & b, const SetUnsetValue<T> & a)
		{
			return b <= a.get();
		}

	} // tools
} // mpasv
