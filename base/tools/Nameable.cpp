#include "Nameable.h"

#include "tools/Exception.h"


Nameable::Nameable(const std::string & name)
	: m_name(name)
{
	CHECK_GT(m_name.length(), 0);
}
