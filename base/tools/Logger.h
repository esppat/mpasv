#pragma once

#include <fstream>
#include <iostream>
#include <queue>
#include <thread>
#include <sstream>
#include <mutex>

#define ignore_result(x) do { if (x) {} } while(false)

#include "tools/physics/SClock.h"


namespace mpasv
{
	namespace tools
	{

		enum class LoggerLevel
		{
			Debug,
			Info,
			Warn,
			Err,
			Fatal
		};

#define LOG_DEBUG(...) mpasv::tools::logger.log(mpasv::tools::LoggerLevel::Debug, "DEBUG: ", mpasv::tools::logger.removePathFromPathAndName(__FILE__), "(", __LINE__, ") ", __func__, ": ", __VA_ARGS__)
#define LOG_INFO(...) mpasv::tools::logger.log(mpasv::tools::LoggerLevel::Info, "INFO:  ", mpasv::tools::logger.removePathFromPathAndName(__FILE__), "(", __LINE__, ") ", __func__, ": ", __VA_ARGS__)
#define LOG_WARN(...) mpasv::tools::logger.log(mpasv::tools::LoggerLevel::Warn, "WARN:  ", mpasv::tools::logger.removePathFromPathAndName(__FILE__), "(", __LINE__, ") ", __func__, ": ", __VA_ARGS__)
#define LOG_ERR(...) mpasv::tools::logger.log(mpasv::tools::LoggerLevel::Err, "ERR:   ", mpasv::tools::logger.removePathFromPathAndName(__FILE__), "(", __LINE__, ") ", __func__, ": ", __VA_ARGS__)
#define LOG_FATAL(...) mpasv::tools::logger.log(mpasv::tools::LoggerLevel::Fatal, "FATAL: ", mpasv::tools::logger.removePathFromPathAndName(__FILE__), "(", __LINE__, ") ", __func__, ": ", __VA_ARGS__)

#define LOG_THAT(what) LOG_DEBUG(#what " = ", what)

		class Logger
		{
		public:

			Logger();
			virtual ~Logger();

			bool isOpen() const;

			void open(const std::string& file_name);
			bool tryOpen(const std::string& file_name);

			void close();

			void set_cout_level(const LoggerLevel level);

			void set_minimal_mode();
			void hideHeader();

			void disableFileWrite();

			template <typename First, typename... Rest>
			void log(const LoggerLevel level, const std::string string_level, const First& first, const Rest&... rest)
			{
				std::ostringstream stream;

				startPrint(stream, level, string_level);
				print(stream, level, first, rest...); // recursive call using pack expansion syntax
				endPrint(stream, level);

				std::string message = stream.str();

				mutex().lock();

				m_messages.push(message);

				std::cout << message;
				std::cout.flush();

				mutex().unlock();

				if (level == tools::LoggerLevel::Fatal)
				{
					exit(-1);
				}
			}

			std::string removePathFromPathAndName(const char * filePathAndName) const;

			std::recursive_mutex & mutex() const;

		private:

			void print(std::ostringstream & /*stream*/, const LoggerLevel /*level*/)
			{
			}

			template <typename T> void print(std::ostringstream & stream, const LoggerLevel level, const T& t)
			{
				if (!m_minimal_mode && m_coutLevel <= level)
				{
					stream << t;
				}
			}

			template <typename First, typename... Rest>
			void print(std::ostringstream & stream, const LoggerLevel level, const First& first, const Rest&... rest)
			{
				if (!m_minimal_mode && m_coutLevel <= level)
				{
					stream << first;
				}

				print(stream, level, rest...); // recursive call using pack expansion syntax
			}

			void checkRotation();
			void doRotation();

			void startPrint(std::ostringstream & stream, const LoggerLevel level, const std::string& string_level);
			void endPrint(std::ostringstream & stream, const LoggerLevel level);

			static void s_consumeMessages(Logger * logger);
			void consumeMessages();

			static void s_compressFile(const std::string& timed_file_name);

			bool openFile();

		private:

			mutable std::recursive_mutex m_mutex;

			std::ofstream m_out;
			bool m_isOpen;
			std::string m_baseFileName;
			std::string m_currentFileName;
			LoggerLevel m_coutLevel = LoggerLevel::Debug;

			bool m_minimal_mode;
			bool m_hideHeader;

			second_t m_lastRotationTimePoint;
			second_t m_rotationDuration;
			long m_rotationLineCount;
			const long m_rotationLineCountMax = 100000;

			std::queue<std::string> m_messages;

			bool m_fileWriteDisabled;

			bool m_writeToFileThread_stopRequested{};
			std::thread * m_writeToFileThread;
		};

		extern Logger logger;
		extern Logger dataLogger;
	} // tools
} // mpasv
