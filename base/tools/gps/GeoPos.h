#pragma once

#include "tools/physics/Units.h"
#include <cfloat>


namespace mpasv::tools::geo
{

	typedef degree_t latitude_t;
	typedef degree_t longitude_t;

	class GeoPos
	{
	public:

		GeoPos() = default;
		GeoPos(const latitude_t lat, const longitude_t lon);

		virtual ~GeoPos() = default;

		bool isValid() const;

		meter_t distanceTo(const GeoPos & pos) const;
		GeoPos forwardTo(const degree_t azymuth, const meter_t distance) const;
		meter_t crossTrackError(const GeoPos & pos1, const GeoPos & pos2) const;
		degree_t bearingTo(const GeoPos &pos) const;

		latitude_t lat() const;
		longitude_t lon() const;

		void set(const GeoPos & pos);
		void set(const latitude_t lat, const longitude_t lon);
		void setLat(const latitude_t lat);
		void setLon(const longitude_t lon);
		void unSet();

		static bool isValid(const latitude_t lat, const longitude_t lon);


	private:

		latitude_t m_lat {FLT_MAX};
		longitude_t m_lon {FLT_MAX};

	};

}

using namespace mpasv::tools::geo;

degree_t normalize360(const degree_t degree);
degree_t normalize180(const degree_t degree);
