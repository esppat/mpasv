#include "tools/Exception.h"

#include "tools/gps/GeoPos.h"

#include <GeographicLib/Geodesic.hpp>

#include <cmath>
#include <cmath>


namespace mpasv::tools::geo
{

	const GeographicLib::Geodesic & geod = GeographicLib::Geodesic::WGS84();

	GeoPos::GeoPos(const latitude_t lat, const longitude_t lon)
	{
		set(lat, lon);
	}

	bool GeoPos::isValid() const
	{
		return isValid(m_lat, m_lon);
	}

	meter_t GeoPos::distanceTo(const GeoPos & pos) const
	{
		CHECK_TRUE(isValid());
		CHECK_TRUE(pos.isValid());

		double s12;
		geod.Inverse((double) m_lat, (double) m_lon, (double) pos.lat(), (double) pos.lon(), s12);
		return meter_t(s12);
	}

	GeoPos GeoPos::forwardTo(const degree_t azymuth, const meter_t distance) const
	{
		CHECK_TRUE(isValid());

		double lat2; double lon2;
		geod.Direct((double) m_lat, (double) m_lon, (double) azymuth, (double) distance, lat2, lon2);
		return GeoPos(degree_t(lat2), degree_t(lon2));
	}

	meter_t GeoPos::crossTrackError(const GeoPos & pos1, const GeoPos & pos2) const
	{
		CHECK_TRUE(isValid());
		CHECK_TRUE(pos1.isValid());
		CHECK_TRUE(pos2.isValid());

		meter_t result; // Cross Track Error

		meter_t delta13 = pos1.distanceTo(*this);

		if (delta13 == 0.0_m)
		{
			result = delta13;
		}
		else
		{
			delta13 /= GeographicLib::Constants::WGS84_a();
			degree_t teta13 = pos1.bearingTo(*this);
			degree_t teta12 = pos1.bearingTo(pos2);

			auto sin1 = sin(teta13 - teta12);
			auto sin2 = sin(degree_t((double) delta13)); // yes, change meter_t to degree_t ... do not know why, but this formula works!
			auto mul = sin2 * sin1;

			degree_t deltaX = asin(mul);

			// yes, change back degree_t to meter_t
			result = meter_t((double) deltaX * GeographicLib::Constants::WGS84_a());
		}

		return result;
	}

	degree_t GeoPos::bearingTo(const GeoPos &pos) const
	{
		CHECK_TRUE(isValid());
		CHECK_TRUE(pos.isValid());

		double azi1; double azi2;
		geod.Inverse((double) m_lat, (double) m_lon, (double) pos.lat(), (double) pos.lon(), azi1, azi2);
		return normalize360(degree_t(azi1));
	}

	latitude_t GeoPos::lat() const
	{
		return m_lat;
	}

	longitude_t GeoPos::lon() const
	{
		return m_lon;
	}

	void GeoPos::set(const GeoPos & pos)
	{
		setLat(pos.lat());
		setLon(pos.lon());
	}

	void GeoPos::set(const latitude_t lat, const longitude_t lon)
	{
		setLat(lat);
		setLon(lon);
	}

	void GeoPos::setLat(const latitude_t lat)
	{
		CHECK_TRUE((double) lat != FLT_MAX);
		CHECK_TRUE((double) lat >= -90.0);
		CHECK_TRUE((double) lat <= +90.0);
		m_lat = lat;
	}

	void GeoPos::setLon(const longitude_t lon)
	{
		CHECK_TRUE((double) lon != FLT_MAX);
		CHECK_TRUE((double) lon >= -180.0);
		CHECK_TRUE((double) lon < +180.0);
		m_lon = lon;
	}

	void GeoPos::unSet()
	{
		m_lat = latitude_t(FLT_MAX);
		m_lon = longitude_t(FLT_MAX);
	}

	/*static*/
	bool GeoPos::isValid(const latitude_t lat, const longitude_t lon)
	{
		return double(lat) != FLT_MAX
							  && std::isfinite(double(lat))
							  && lat >= -90.0_deg
							  && lat <= +90.0_deg
							  && double(lon) != FLT_MAX
												&& std::isfinite(double(lon))
												&& lon >= -180.0_deg
												&& lon < +180.0_deg
												;
	}

} // mpasv

degree_t normalize360(const degree_t degree)
{
	CHECK_TRUE(std::isfinite((double)degree));

	degree_t result = degree;

	while (result < 0.0_deg)
		result += 360.0_deg;
	while (result >= 360.0_deg)
		result -= 360.0_deg;

	return result;
}

degree_t normalize180(const degree_t degree)
{
	CHECK_TRUE(std::isfinite((double)degree));

	degree_t result = degree;

	while (result < -180.0_deg)
		result += 360.0_deg;
	while (result >= 180.0_deg)
		result -= 360.0_deg;

	return result;
}
