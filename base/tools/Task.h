/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Task.h
 * Author: esppat
 *
 * Created on 21 décembre 2018, 15:37
 */

#pragma once

#include "tools/Lock.h"
#include "tools/Lockable.h"
#include "tools/Nameable.h"

#include <mutex>
#include <string>


namespace mpasv
{
	namespace tools
	{

		class Task
			: public virtual Lockable
			, public Nameable
		{
		public:

			Task(std::string  taskName);
			Task(const Task& orig) = delete;
			virtual ~Task();

			bool start(const bool standBy = false);
			void stop();

			virtual bool started() const;

			void standby();
			void endOfStandby();
			bool isOnStandby() const;

		protected:

			virtual bool setup() noexcept;
			virtual void initialize() noexcept = 0;
			virtual void finalize() noexcept = 0;

		private:

			bool m_initialized;
			bool m_standby;

		};

	} // tools
} // mpasv
