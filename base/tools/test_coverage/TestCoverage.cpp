/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#ifdef TEST_COVERAGE

#include "test_coverage/test_coverage.h"

#include "tools/Logger.h"

namespace mpasv
{
	namespace tools
	{
		namespace test_coverage
		{

			void testTarget(const char * file, const unsigned long line)
			{
				LOG_INFO(file, "(", line, ")");
			}
		} // test_coverage
	} // tools
} // mpasv

#endif // TEST_COVERAGE
