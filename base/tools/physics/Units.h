/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   units.h
 * Author: esppat
 *
 * Created on 10 janvier 2019, 10:03
 */

#pragma once

#define DISABLE_PREDEFINED_UNITS

#define ENABLE_PREDEFINED_ACCELERATION_UNITS
#define ENABLE_PREDEFINED_ANGLE_UNITS
#define ENABLE_PREDEFINED_ANGULAR_MOMENTUM_UNITS
#define ENABLE_PREDEFINED_ANGULAR_ACCELERATION_UNITS
#define ENABLE_PREDEFINED_ANGULAR_VELOCITY_UNITS
#define ENABLE_PREDEFINED_CONCENTRATION_UNITS
#define ENABLE_PREDEFINED_CONCENTRATION_VELOCITY_UNITS
#define ENABLE_PREDEFINED_FREQUENCY_UNITS
#define ENABLE_PREDEFINED_LENGTH_UNITS
#define ENABLE_PREDEFINED_TIME_UNITS
#define ENABLE_PREDEFINED_VELOCITY_UNITS
#define ENABLE_PREDEFINED_CURRENT_UNITS
#define ENABLE_PREDEFINED_VOLTAGE_UNITS
#define ENABLE_PREDEFINED_POWER_UNITS
#define ENABLE_PREDEFINED_ENERGY_UNITS
#define ENABLE_PREDEFINED_FORCE_UNITS
#define ENABLE_PREDEFINED_MASS_UNITS
#define ENABLE_PREDEFINED_TORQUE_UNITS

#include "tools/physics/_units.h"

using namespace units;
using namespace units::acceleration;
using namespace units::angle;
using namespace units::angular_acceleration;
using namespace units::angular_momentum;
using namespace units::angular_velocity;
using namespace units::concentration; // for percent
using namespace units::concentration_velocity; // for percent velocity
using namespace units::current;
using namespace units::detail;
using namespace units::dimensionless;
using namespace units::energy;
using namespace units::frequency;
using namespace units::force;
using namespace units::length;
using namespace units::literals;
using namespace units::mass;
using namespace units::math;
using namespace units::power;
using namespace units::time;
using namespace units::torque;
using namespace units::velocity;
using namespace units::voltage;

using namespace constants::detail;

#include <thread>
#include <functional>
#include <chrono>

namespace mpasv::tools::physics
{

//	inline second_t durationSince(const std::chrono::high_resolution_clock::time_point start)
//	{
//		std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
//		return nanosecond_t(std::chrono::duration_cast<std::chrono::nanoseconds>(now - start).count());
//	}

//	inline second_t durationBetween(const std::chrono::high_resolution_clock::time_point start, const std::chrono::high_resolution_clock::time_point end)
//	{
//		return nanosecond_t(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());
//	}

	inline void sleepFor(const nanosecond_t duration)
	{
		std::this_thread::sleep_for(std::chrono::nanoseconds(duration));
	}

	inline void sleepWhile(std::function<bool () > func, millisecond_t sleepDuration = 1_ms)
	{
		while (func())
			sleepFor(sleepDuration);
	}

	template <typename T, typename U>
	double doubleFrom(const U & u)
	{
		return double(T(u));
	}

	template <typename T, typename U>
	float floatFrom(const U & u)
	{
		return float(T(u));
	}

}

using namespace mpasv::tools::physics;
