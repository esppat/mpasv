/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmortizedValue.h
 * Author: esppat
 *
 * Created on 12 janvier 2019, 15:29
 */

#pragma once

#include <deque>

namespace mpasv
{
	namespace tools
	{
		namespace physics
		{

			template<class T>
			class AmortizedValue
			{
			public:

				AmortizedValue(const uint depth)
					: m_depth(depth)
					, m_sum(0.0)
				{
				}

				virtual ~AmortizedValue()
				{
				}

				T getAmortized() const
				{
					return m_amortized;
				}

				void push(const T & t)
				{
					m_tab.push_front(t);
					m_sum += t;

					if (m_tab.size() > m_depth)
					{
						m_sum -= m_tab.back();
						m_tab.pop_back();
					}

					m_amortized = T(0.0);
					uint multiplier = 0;
					uint divisor = 0;
					for (const T & element : m_tab)
					{
						++multiplier;
						divisor += multiplier;
						m_amortized += element * multiplier;
					}

					if (multiplier > 0)
					{
						m_amortized /= divisor;
					}
				}

			protected:

			private:

				std::deque<T> m_tab;
				uint m_depth;
				T m_sum;
				T m_amortized;

			};

		} // physics
	} // tools
} // mpasv
