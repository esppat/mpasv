/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ValueHistory.h
 * Author: PEspie
 *
 * Created on February 13, 2019, 9:55 AM
 */

#pragma once

#include "tools/Lock.h"
#include "tools/Logger.h"
#include "tools/physics/Units.h"
#include "tools/Exception.h"
#include "tools/physics/SClock.h"

#include <deque>
#include <mutex>


namespace mpasv::tools::physics
{

	template<typename ValueType>
	class ValueHistory
	{
	public:

		ValueHistory()
			: ValueHistory(2_s)
		{
		}

		ValueHistory(const second_t duration)
			: m_duration(duration)
		{
		}

		ValueHistory(const ValueHistory<ValueType> & other)
		{
			*this = other;
		}

		virtual ~ValueHistory()
		{
		}

		ValueHistory & operator=(const ValueHistory & other)
		{
			LOCK2(m_mutex, other.m_mutex);

			m_duration = other.m_duration;
			m_values = other.m_values;

			return *this;
		}

		void push_back(const ValueType & value)
		{
			auto now = theClock.now();

			LOCK(m_mutex);

			cleanup(now);

			m_values.push_back({now, value});
		}

		void reset()
		{
			LOCK(m_mutex);

			m_values.resize(0);
		}

		bool getVariation(const second_t duration, ValueType & variation) const
		{
			CHECK_GT(duration, 0.0_s);
			CHECK_LE(duration, m_duration);

			second_t now = theClock.now();
			unsigned count = 0;
			second_t measuredDuration = 0_s;

			LOCK(m_mutex);

			if (!m_values.empty())
			{
				ValueType recentValue = m_values.back().m_value;

				for (auto it = m_values.rbegin(); it != m_values.rend(); it++)
				{
					measuredDuration = now - it->m_time;
					if (measuredDuration > duration)
						break;

					variation = recentValue - it->m_value;

					++count;
				}
			}

			return count >= 2 && measuredDuration >= duration;
		}

		bool isConstantFor(const second_t duration, const ValueType absMaxVariation) const
		{
			CHECK_GT(duration, 0.0_s);
			CHECK_LE(duration, m_duration);
			CHECK_GT(absMaxVariation, ValueType(0.0));

			bool result = true;
			second_t now = theClock.now();
			unsigned count = 0;
			second_t measuredDuration = 0_s;

			LOCK(m_mutex);

			if (!m_values.empty())
			{
				ValueType maxValue = m_values.back().m_value;
				ValueType minValue = maxValue;

				for (auto it = m_values.rbegin(); it != m_values.rend(); it++)
				{
					measuredDuration = now - it->m_time;
					if (measuredDuration > duration)
						break;

					const ValueType value = it->m_value;

					if (value < minValue)
						minValue = value;
					else if (value > maxValue)
						maxValue = value;

					if (fabs(maxValue - minValue) > absMaxVariation)
					{
						result = false;
						break;
					}

					++count;
				}
			}

			return result && count >= 2 && measuredDuration >= duration;
		}

		void dump() const
		{
			auto now = theClock.now();

			LOCK(m_mutex);

			LOG_DEBUG(">>>>>");
			LOG_THAT(m_duration);
			for (auto t = m_values.rbegin(); t != m_values.rend(); t++)
			{
				second_t age = now - t->m_time;
				ValueType value = t->m_value;

				LOG_DEBUG("Age=", age, ", value=", value);
			}
			LOG_DEBUG("<<<<<");
		}

	private:

		void cleanup(const second_t now = theClock.now())
		{
			LOCK(m_mutex);

			while (!m_values.empty() && (now - m_values.front().m_time) > m_duration)
			{
				m_values.pop_front();
			}
		}

	private:

		typedef struct
		{
			second_t m_time;
			ValueType m_value;
		} Couple;

		mutable std::recursive_mutex m_mutex;

		second_t m_duration;
		std::deque<Couple> m_values;

	};

}
