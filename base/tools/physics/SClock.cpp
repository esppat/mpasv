#include "SClock.h"

#include "tools/Exception.h"
#include <chrono>

static auto epoch = std::chrono::high_resolution_clock::now();
static auto pauseTime = epoch;
static bool paused = false;


namespace mpasv::tools::physics
{

	SClock::SClock()
	{

	}

	void SClock::pause()
	{
		CHECK_FALSE(paused);
		paused = true;
		pauseTime = std::chrono::high_resolution_clock::now();
	}

	bool SClock::isPaused() const
	{
		return paused;
	}

	void SClock::resume()
	{
		CHECK_TRUE(paused);
		epoch += std::chrono::high_resolution_clock::now() - pauseTime;
		paused = false;
	}

	nanosecond_t SClock::now() const
	{
		if (paused)
		{
			return nanosecond_t(pauseTime - epoch);
		}
		else
		{
			return nanosecond_t(std::chrono::high_resolution_clock::now() - epoch);
		}
	}

}

SClock theClock;
