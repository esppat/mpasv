#pragma once

#include "tools/physics/Units.h"


namespace mpasv::tools::physics
{

	class SClock
	{
	public:

		SClock();

		void pause();
		bool isPaused() const;

		void resume();

		nanosecond_t now() const;

	protected:
	protected:
	private:
	private:

	};

}

using namespace mpasv::tools::physics;

extern SClock theClock;
