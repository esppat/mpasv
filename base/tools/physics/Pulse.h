/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Pulse.h
 * Author: esppat
 *
 * Created on January 19, 2020, 10:47 PM
 */

#pragma once

#include "tools/physics/Units.h"
#include "tools/physics/SClock.h"


namespace mpasv
{
	namespace tools
	{
		namespace physics
		{

			class Pulse
			{
			public:

				Pulse()
				{
					init(0_Hz, false);
				}

				Pulse(const hertz_t freq, const bool activate = true)
				{
					init(freq, activate);
				}

				virtual ~Pulse()
				{
				}

				void init(const hertz_t freq, const bool activate = true)
				{
					setFreq(freq);
					setActivate(activate);
				}

				void setFreq(const hertz_t freq)
				{
					m_freq = freq;
				}

				void setActivate(const bool activate)
				{
					m_activate = activate;
					if (m_activate)
					{
						m_start = theClock.now();
					}
				}

				bool shouldPulse() const
				{
					bool result = false;

					if (m_activate)
					{
						auto now = theClock.now();
						if (now - m_start >= 1.0 / m_freq)
						{
							m_start = now;
							result = true;
						}
					}

					return result;
				}

			public:
			protected:
			protected:
			private:
			private:

				hertz_t m_freq;
				bool m_activate;
				mutable second_t m_start;

			};

		} // physics
	} // tools
} // mpasv
