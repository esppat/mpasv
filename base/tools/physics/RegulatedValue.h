/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RegulatedValue.h
 * Author: PEspie
 *
 * Created on February 7, 2019, 5:12 PM
 */

#pragma once

#include "tools/physics/Units.h"
#include "tools/Lock.h"
#include "tools/Lockable.h"
#include "tools/Logger.h"
#include "tools/Exception.h"
#include "tools/physics/SClock.h"

#include <mutex>
#include <thread>


namespace mpasv
{
	namespace tools
	{
		namespace physics
		{

			template<typename ValueType, typename ValueChangeType>
			class RegulatedValue
			: public tools::Lockable
			{
			public:

				RegulatedValue()
					: m_traceOn(false)
					, m_initialized(false)
				{
				}

				RegulatedValue(const ValueType value, const ValueChangeType valueChange, const ValueType minValue, const ValueType maxValue)
					: RegulatedValue()
				{
					initialize(value, valueChange, minValue, maxValue);
				}

				RegulatedValue(const RegulatedValue & other)
				{
					operator=(other);
				}

				RegulatedValue & operator=(const RegulatedValue & other)
				{
					LOCK2(mutex(), other.mutex());

					m_startValue = other.m_startValue;
					m_calculatedValue = other.m_calculatedValue;
					m_lastChangeTime = other.m_lastChangeTime;

					m_valueChange = other.m_valueChange;
					m_minValue = other.m_minValue;
					m_maxValue = other.m_maxValue;
					m_targetValue = other.m_targetValue;

					m_traceOn = other.m_traceOn;
					m_initialized = other.m_initialized;

					return *this;
				}

				virtual ~RegulatedValue()
				{
					LOCK(mutex());

					m_initialized = false;
				}

				void setTrace(const bool traceOn)
				{
					m_traceOn = traceOn;
				}

				void set(const ValueType targetValue)
				{
					LOCK(mutex());
					CHECK_TRUE(m_initialized);

					if (m_targetValue != targetValue)
					{
						update();
						m_startValue = m_calculatedValue;

						CHECK_GE(targetValue, m_minValue);
						CHECK_LE(targetValue, m_maxValue);
						m_targetValue = targetValue;
						m_lastChangeTime = theClock.now();
					}
				}

				void set(const ValueType value, const ValueChangeType valueChange, const ValueType minValue, const ValueType maxValue)
				{
					LOCK(mutex());

					initialize(value, valueChange, minValue, maxValue);
				}

				ValueType get() const
				{
					LOCK(mutex());
					CHECK_TRUE(m_initialized);

					update();
					return m_calculatedValue;
				}

				ValueType getTargetValue() const
				{
					LOCK(mutex());
					CHECK_TRUE(m_initialized);

					return m_targetValue;
				}

				ValueType getMinValue() const
				{
					LOCK(mutex());
					CHECK_TRUE(m_initialized);

					return m_minValue;
				}

				ValueType getMaxValue() const
				{
					LOCK(mutex());
					CHECK_TRUE(m_initialized);

					return m_maxValue;
				}

				ValueType getIntegrationDuring(const second_t duration) const
				{
					LOCK(mutex());
					CHECK_TRUE(m_initialized);

					ValueType diff = m_targetValue - m_startValue;
					ValueType maxDiff = m_valueChange * duration;

					if (diff < -maxDiff)
						diff = -maxDiff;
					else if (diff > maxDiff)
						diff = maxDiff;

					return diff;
				}

			private:

				void initialize(const ValueType value, const ValueChangeType valueChange, const ValueType minValue, const ValueType maxValue)
				{
					CHECK_TRUE(valueChange > ValueChangeType(0.0));
					CHECK_TRUE(maxValue - minValue > ValueType(0.0));
					CHECK_GE(value, minValue);
					CHECK_LE(value, maxValue);

					m_startValue = value;
					m_calculatedValue = value;
					m_targetValue = value;
					m_valueChange = valueChange;
					m_minValue = minValue;
					m_maxValue = maxValue;

					m_lastChangeTime = theClock.now();

					m_initialized = true;
				}

				void update() const
				{
					auto now = theClock.now();
					second_t durationSinceLastChange = now - m_lastChangeTime;

					m_calculatedValue = m_startValue + getIntegrationDuring(durationSinceLastChange);

					if (m_calculatedValue < m_minValue)
						m_calculatedValue = m_minValue;
					else if (m_calculatedValue > m_maxValue)
						m_calculatedValue = m_maxValue;

					if (m_traceOn)
						dump();
				}

				void dump() const
				{
					LOG_DEBUG("m_startValue=", m_startValue,
							", m_calculatedValue=", m_calculatedValue,
							", m_valueChange=", m_valueChange,
							", m_minValue=", m_minValue,
							", m_maxValue=", m_maxValue,
							", m_targetValue=", m_targetValue);
				}

			private:

				mutable ValueType m_startValue;
				mutable ValueType m_calculatedValue;
				mutable second_t m_lastChangeTime;

				ValueChangeType m_valueChange;
				ValueType m_minValue;
				ValueType m_maxValue;
				ValueType m_targetValue;

				bool m_traceOn;
				bool m_initialized;
			};

		} // physics
	} // tools
} // mpasv
