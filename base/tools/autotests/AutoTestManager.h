/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AutoTestManager.h
 * Author: PEspie
 *
 * Created on February 19, 2019, 9:24 AM
 */

#pragma once

#include "tools/Tools.h"

#include <vector>


namespace mpasv::tools::autotests
{
	class Testable;

	class AutoTestManager
			: public tools::enable_shared_from_this<AutoTestManager>
	{
	public:

		AutoTestManager();
		AutoTestManager(const AutoTestManager& orig) = delete;
		virtual ~AutoTestManager();

		void add(Testable * testable);

		void startTests();

	private:

		std::vector<Testable*> m_testables;

	};

}
