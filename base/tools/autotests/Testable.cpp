/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Testable.cpp
 * Author: PEspie
 *
 * Created on February 19, 2019, 9:28 AM
 */

#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/Demangler.h"

#include "tools/autotests/Testable.h"


namespace mpasv::tools::autotests
		{

			Testable::Testable()
			{
				m_inTest = false;
			}

			Testable::~Testable()
			= default;

			void Testable::startTest()
			{
				CHECK_FALSE(isInTest());
				m_inTest = true;

				try
				{
					testImpl();
				}
				catch (...)
				{
					LOG_ERR("Exception caught during test in ", typeName(this));
				}

				m_inTest = false;
			}

			bool Testable::isInTest() const
			{
				return m_inTest;
			}

		} // mpasv
