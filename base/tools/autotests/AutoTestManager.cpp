/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AutoTestManager.cpp
 * Author: PEspie
 *
 * Created on February 19, 2019, 9:24 AM
 */

#include "tools/autotests/AutoTestManager.h"

#include "tools/autotests/Testable.h"

namespace mpasv::tools::autotests
		{

			AutoTestManager::AutoTestManager()
			= default;

			AutoTestManager::~AutoTestManager()
			= default;

			void AutoTestManager::add(Testable * testable)
			{
				m_testables.push_back(testable);
			}

			void AutoTestManager::startTests()
			{
				for (auto testable : m_testables)
				{
					testable->startTest();
				}
			}

		} // mpasv
