/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Testable.h
 * Author: PEspie
 *
 * Created on February 19, 2019, 9:28 AM
 */

#pragma once

#include <string>


namespace mpasv::tools::autotests
{

	class Testable
	{
	public:

		Testable();
		Testable(const Testable& orig) = delete;
		virtual ~Testable();

		void startTest();

		bool isInTest() const;

	private:

		virtual void testImpl() = 0;

	private:

		bool m_inTest;

	};

}
