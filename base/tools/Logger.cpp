#include "tools/Logger.h"
#include "tools/Exception.h"
#include "tools/Tools.h"

#include <iomanip>
#include <ctime>
#include <future>

namespace mpasv::tools
	{

		Logger logger;
		Logger dataLogger;

		Logger::Logger()
		{
			m_minimal_mode = false;
			m_hideHeader = false;

			m_rotationDuration = 1_hr;

			m_rotationLineCount = 0;
			m_lastRotationTimePoint = theClock.now();

			m_isOpen = false;

			m_fileWriteDisabled = false;
			m_writeToFileThread = nullptr;
		}

		Logger::~Logger()
		{
			mutex().lock();
			std::lock_guard<std::recursive_mutex> lock(mutex(), std::adopt_lock);

			if (isOpen())
			{
				close();
			}
		}

		bool Logger::isOpen() const
		{
			return m_isOpen;
		}

		void Logger::open(const std::string& file_name)
		{
			CHECK_TRUE(tryOpen(file_name));
		}

		bool Logger::tryOpen(const std::string& file_name)
		{
			bool result = false;

			mutex().lock();
			std::lock_guard<std::recursive_mutex> lock(mutex(), std::adopt_lock);

			CHECK_TRUE(!file_name.empty());
			m_baseFileName = file_name;

			CHECK_TRUE(!isOpen());

			if (openFile())
			{
				m_writeToFileThread_stopRequested = false;
				m_writeToFileThread = new std::thread(Logger::s_consumeMessages, this);

				m_isOpen = true;
				result = true;
			}
			else
			{
				std::cerr << "Cannot open log: " << m_currentFileName << std::endl;
			}

			return result;
		}

		void Logger::close()
		{
			mutex().lock();
			std::lock_guard<std::recursive_mutex> lock(mutex(), std::adopt_lock);

			CHECK_TRUE(isOpen());

			m_writeToFileThread_stopRequested = true;
			m_writeToFileThread->join();
			delete m_writeToFileThread;
			m_writeToFileThread = nullptr;

			m_out.close();
			m_isOpen = false;
		}

		void Logger::set_cout_level(const LoggerLevel level)
		{
			m_coutLevel = level;
		}

		void Logger::set_minimal_mode()
		{
			m_minimal_mode = true;
			m_out << std::setprecision(6);
		}

		void Logger::hideHeader()
		{
			m_hideHeader = true;
		}

		void Logger::disableFileWrite()
		{
			m_fileWriteDisabled = true;
		}

		std::string Logger::removePathFromPathAndName(const char * filePathAndName) const
		{
			const char * p = filePathAndName;
			while (*p)
				p++;
			while (p != filePathAndName)
			{
				if (*p == '/')
				{
					return ++p;
				}
				--p;
			}

			return filePathAndName;
		}

		std::recursive_mutex & Logger::mutex() const
		{
			return m_mutex;
		}

		void Logger::checkRotation()
		{
			// check log age
			auto now = theClock.now();

			bool must_do_rotation = ((now - m_lastRotationTimePoint) > m_rotationDuration);
			if (!must_do_rotation)
			{
				if (m_rotationLineCount > m_rotationLineCountMax)
				{
					must_do_rotation = true;
				}
			}

			if (must_do_rotation)
			{
				doRotation();
				m_lastRotationTimePoint = now;
				m_rotationLineCount = 0;
			}
		}

		void Logger::doRotation()
		{
			std::string fileName = m_currentFileName;

			// close file
			m_out.close();

			// compress file asynchronously
			std::async(std::launch::async, Logger::s_compressFile, fileName);

			// open logger again
			openFile();
		}

		void Logger::startPrint(std::ostringstream & stream, const LoggerLevel level, const std::string& string_level)
		{
			if (!m_minimal_mode && m_coutLevel <= level && !m_hideHeader)
			{
				stream << tools::getCurrentTime() << " " << string_level;
			}
		}

		void Logger::endPrint(std::ostringstream & stream, const LoggerLevel level)
		{
			if (!m_minimal_mode && m_coutLevel <= level)
			{
				stream << "\n";
			}
		}

		/*static*/
		void Logger::s_consumeMessages(Logger * logger)
		{
			logger->consumeMessages();
		}

		void Logger::consumeMessages()
		{
			auto start = theClock.now();

			while (!m_writeToFileThread_stopRequested)
			{
				m_mutex.lock();
				if (!m_messages.empty())
				{
					m_rotationLineCount++;

					std::string message = m_messages.front();
					m_messages.pop();

					m_mutex.unlock();

					m_out << message;
				}
				else
				{
					checkRotation();

					m_mutex.unlock();

					auto now = theClock.now();
					if (start - now >= 1_s)
					{
						m_out.flush();
						start = now;
					}

					sleepFor(0.5_s);
				}
			}
		}

		/*static*/
		void Logger::s_compressFile(const std::string& timed_file_name)
		{
			std::string command;

			// do compression
			command = "zip " + timed_file_name + ".zip " + timed_file_name;
			ignore_result(std::system(command.c_str()));

			// delete original file
			std::remove(timed_file_name.c_str());
		}

		bool Logger::openFile()
		{
			m_currentFileName = m_baseFileName + "_" + tools::getCurrentTime();
			m_out.open(m_currentFileName, std::ios::out | std::ios::app);
			return m_out.is_open();
		}

	} // mpasv
