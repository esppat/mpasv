#include "tools/Logger.h"
#include "tools/Tools.h"
#include "tools/Exception.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/physics/Units.h"
#include "tools/physics/SClock.h"

#include "virtual_captain/VirtualCaptain.h"

#include <thread>

#include <csignal>

static bool sm_SIGINT_received = false;

void receive_SIGINT(int /*param*/)
{
	sm_SIGINT_received = true;
	signal(SIGINT, SIG_DFL);
}

int main()
{
	// install SIGINT handler
	signal(SIGINT, receive_SIGINT);

	// install logger
	//tools::logger.open("/media/USBKEY/mp-asv");
	mpasv::tools::logger.open("dyndata/logs/mp-asv");
	mpasv::tools::logger.set_cout_level(mpasv::tools::LoggerLevel::Debug);
	mpasv::tools::logger.hideHeader();

	// install data logger
	//tools::data_logger.open("/media/USBKEY/mp-asv-data");
	//mpasv::tools::dataLogger.open("dyndata/logs/mp-asv-data");
	//mpasv::tools::dataLogger.set_minimal_mode();

	mpasv::tools::parameters::ParameterManager::get();
	mpasv::tools::parameters::ParameterManager::get()->save(); // for auto-formatting

	// start Virtual Captain
	std::shared_ptr<mpasv::virtual_captain::VirtualCaptain> virtualCaptain = std::make_shared<mpasv::virtual_captain::VirtualCaptain>();
	if (virtualCaptain->start())
	{
		second_t delay = 1_hr;
		auto start = theClock.now();
		LOG_INFO("Working for ", delay);
		while (theClock.now() - start < delay && !sm_SIGINT_received)
		{
			mpasv::tools::physics::sleepFor(1_s);
		}

		LOG_INFO("Working done: stopping ...");

		virtualCaptain->stop();
	}
	return 0;
}
