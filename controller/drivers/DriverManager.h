/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DriverManager.h
 * Author: esppat
 *
 * Created on December 18, 2019, 12:14 AM
 */

#pragma once

#include "tools/Demangler.h"
#include "tools/Exception.h"
#include "tools/Tools.h"

#include "drivers/AbstractDriver.h"
#include "drivers/AbstractSensor.h"
#include "drivers/AbstractActuator.h"

#include <map>


namespace mpasv
{
	namespace drivers
	{

		class DriverManager
		: public tools::enable_shared_from_this<DriverManager>
		{
		public:

			DriverManager();
			DriverManager(const DriverManager& orig) = delete;
			virtual ~DriverManager();

			template <class DRIVER>
			void addDriver(std::shared_ptr<DRIVER> driver)
			{
				CHECK_NOT_NULLPTR(driver);
				std::string driverType = typeName(driver);

				if (m_driverByTypeThenName.find(driverType) != m_driverByTypeThenName.end())
				{
					std::map<std::string, std::shared_ptr<AbstractDriver>> & drivers = getDriversByType(driverType);
					CHECK_NOT_CONTAINS(drivers, driver->name());
				}

				m_driverByTypeThenName[driverType][driver->name()] = driver;
			}

			template <class SENSOR>
			std::shared_ptr<SENSOR> getSensor(const std::string & sensorName)
			{
				CHECK_FALSE(sensorName.empty());

				std::shared_ptr<AbstractDriver> driver = getDriver<SENSOR>(sensorName);

				std::shared_ptr<AbstractSensor> abstractSensor = std::dynamic_pointer_cast<AbstractSensor>(driver);
				CHECK_NOT_NULLPTR_MSG(abstractSensor, std::string("Driver named '") + sensorName + "' (of type '" + typeName(driver) + "') is not a sensor");

				std::shared_ptr<SENSOR> sensor = std::dynamic_pointer_cast<SENSOR>(abstractSensor);
				CHECK_NOT_NULLPTR_MSG(sensor, std::string("Sensor named '") + sensorName + "' (of type '" + typeName(abstractSensor) + "') should be of type '" + typeName<SENSOR>() + "'");

				return sensor;
			}

			template <class ACTUATOR>
			std::shared_ptr<ACTUATOR> getActuator(const std::string & actuatorName)
			{
				CHECK_FALSE(actuatorName.empty());

				std::shared_ptr<AbstractDriver> driver = getDriver<ACTUATOR>(actuatorName);

				std::shared_ptr<AbstractActuator> abstractActuator = std::dynamic_pointer_cast<AbstractActuator>(driver);
				CHECK_NOT_NULLPTR_MSG(abstractActuator, std::string("Driver named '") + actuatorName + "' (of type '" + typeName(driver) + "') is not an actuator");

				std::shared_ptr<ACTUATOR> actuator = std::dynamic_pointer_cast<ACTUATOR>(abstractActuator);
				CHECK_NOT_NULLPTR_MSG(actuator, std::string("Sensor named '") + actuatorName + "' (of type '" + typeName(abstractActuator) + "') should be of type '" + typeName<ACTUATOR>() + "'");

				return actuator;
			}

			template <class DRIVER>
			std::map<std::string, std::shared_ptr<AbstractDriver>> & getDriversByType()
			{
				const std::string & name = typeName<DRIVER>();
				return getDriversByType(name);
			}

			std::map<std::string, std::shared_ptr<AbstractDriver>> & getDriversByType(const std::string & typeName)
			{
				CHECK_CONTAINS(m_driverByTypeThenName, typeName);
				return m_driverByTypeThenName.at(typeName);
			}

		private:

			template <class DRIVER>
			std::shared_ptr<AbstractDriver> getDriver(const std::string & driverName)
			{
				std::map<std::string, std::shared_ptr<AbstractDriver>> & drivers = getDriversByType<DRIVER>();

				CHECK_CONTAINS(drivers, driverName);
				std::shared_ptr<AbstractDriver> driver = drivers[driverName];

				CHECK_NOT_NULLPTR(driver);
				return driver;
			}

		private:

			std::map<std::string, std::map<std::string, std::shared_ptr<AbstractDriver>>> m_driverByTypeThenName;

		};

	} // drivers
} // mpasv
