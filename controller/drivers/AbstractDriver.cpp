/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractDriver.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#include "drivers/AbstractDriver.h"
#include <utility>


namespace mpasv::drivers
	{

		AbstractDriver::AbstractDriver(std::string  name)
			: m_name(std::move(name))
		{
			CHECK_FALSE(m_name.empty());
		}

		AbstractDriver::~AbstractDriver()
		= default;

		void AbstractDriver::push(const std::shared_ptr<const AbstractDriverData>& data)
		{
			CHECK_TRUE(canPush());
			pushImpl(data);
		}

	} // mpasv
