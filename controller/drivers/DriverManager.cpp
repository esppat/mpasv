/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DriverManager.cpp
 * Author: esppat
 *
 * Created on December 18, 2019, 12:14 AM
 */

#include "drivers/DriverManager.h"

#include "tools/Exception.h"

namespace mpasv::drivers
	{

		DriverManager::DriverManager()
		= default;

		DriverManager::~DriverManager()
		= default;

	} // mpasv
