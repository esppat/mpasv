/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractActuator.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#include "drivers/AbstractActuator.h"

namespace mpasv::drivers
	{

		AbstractActuator::AbstractActuator(const std::string & name)
			: AbstractDriver(name)
		{
		}

		AbstractActuator::~AbstractActuator()
		= default;

	} // mpasv
