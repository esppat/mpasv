/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#include "drivers/AbstractSensor.h"

namespace mpasv::drivers
	{

		AbstractSensor::AbstractSensor(const std::string & name)
			: AbstractDriver(name)
		{
		}

		AbstractSensor::~AbstractSensor()
		= default;

	} // mpasv
