/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractActuatorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:18 PM
 */

#pragma once

#include "drivers/AbstractDriverData.h"

#include "tools/Tools.h"


namespace mpasv
{
	namespace drivers
	{

		class AbstractActuatorData
		: public AbstractDriverData
		, public tools::enable_shared_from_this<AbstractActuatorData>
		{
		public:
			AbstractActuatorData() = default;
			AbstractActuatorData(const AbstractActuatorData& orig) = default;
			virtual ~AbstractActuatorData() = default;

		protected:
		protected:
		private:
		private:
		};

	} // drivers
} // mpasv
