/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractDriver.h
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#pragma once

#include "drivers/AbstractDriverData.h"

#include "tools/ThreadedTask.h"
#include "tools/Logger.h"
#include "tools/Tools.h"
#include "tools/Exception.h"

#include <string>


namespace mpasv
{
	namespace drivers
	{
		class AbstractDriverData;

		class AbstractDriver
		: public tools::enable_shared_from_this<AbstractDriver>
		{
		public:
			AbstractDriver(std::string  name);
			AbstractDriver(const AbstractDriver& orig) = delete;
			virtual ~AbstractDriver();

			std::string name() const
			{
				return m_name;
			}

			template <class SENSOR_DATA>
			void pull(const std::shared_ptr<SENSOR_DATA> data)
			{
				CHECK_TRUE(canPull());
				pullImpl(std::dynamic_pointer_cast<SENSOR_DATA, AbstractDriverData>(data));
			}

			void push(const std::shared_ptr<const AbstractDriverData>& data);

		protected:

			virtual void pullImpl(const std::shared_ptr<AbstractDriverData> /*data*/)
			{
				THROW_ALWAYS("Function should be redefined");
			}

			virtual void pushImpl(const std::shared_ptr<const AbstractDriverData> /*data*/)
			{
				THROW_ALWAYS("Function should be redefined");
			}

			virtual bool canPull() const
			{
				return false;
			}

			virtual bool canPush() const
			{
				return false;
			}

		protected:
		private:
		private:

			const std::string m_name;
		};

	} // drivers
} // mpasv
