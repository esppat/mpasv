/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv
{
	namespace drivers
	{
		namespace voltage
		{

			class VoltageSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<VoltageSensorData>
			{
			public:
				VoltageSensorData();
				VoltageSensorData(const VoltageSensorData& orig) = delete;
				virtual ~VoltageSensorData();

			public:
				volt_t m_voltage = 0.0_V;

			protected:
			protected:
			private:
			private:
			};

		} // voltage
	} // drivers
} // mpasv
