/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensorSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/sensors/voltage/VoltageSensor.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace voltage
		{

			class VoltageSensorSim
			: public VoltageSensor
			, public tools::enable_shared_from_this<VoltageSensorSim>
			{
			public:

				VoltageSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				VoltageSensorSim(const VoltageSensorSim& orig) = delete;
				virtual ~VoltageSensorSim();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // voltage
	} // drivers
} // mpasv
