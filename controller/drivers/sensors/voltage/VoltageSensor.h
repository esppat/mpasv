/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace voltage
		{
			class VoltageSensorSim;

			class VoltageSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<VoltageSensor>
			{
			public:

				typedef VoltageSensorSim SimulatedType;

				VoltageSensor(const std::string & name);
				VoltageSensor(const VoltageSensor& orig) = delete;
				virtual ~VoltageSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // voltage
	} // drivers
} // mpasv
