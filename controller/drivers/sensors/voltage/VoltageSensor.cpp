/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/voltage/VoltageSensor.h"
#include "drivers/sensors/voltage/VoltageSensorData.h"


namespace mpasv::drivers::voltage
		{

			VoltageSensor::VoltageSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			VoltageSensor::~VoltageSensor()
			= default;

			void VoltageSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				auto voltData = std::dynamic_pointer_cast<VoltageSensorData>(data);

				// TODO
				voltData->m_voltage = -9999.0_V;
			}

		} // mpasv
