/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/voltage/VoltageSensorSim.h"
#include "drivers/sensors/voltage/VoltageSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::voltage
{

	VoltageSensorSim::VoltageSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: VoltageSensor(name)
		, m_simulationController(std::move(simulationController))
	{
	}

	VoltageSensorSim::~VoltageSensorSim()
	= default;

	void VoltageSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
	{
		auto waterData = std::dynamic_pointer_cast<VoltageSensorData>(data);
		m_simulationController->update(waterData, name());
	}

} // mpasv
