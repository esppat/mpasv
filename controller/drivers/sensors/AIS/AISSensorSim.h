/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AISSensorSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/sensors/AIS/AISSensor.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace AIS
		{

			class AISSensorSim
			: public drivers::AIS::AISSensor
			, public tools::enable_shared_from_this<AISSensorSim>
			{
			public:

				AISSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				AISSensorSim(const AISSensorSim& orig) = delete;
				virtual ~AISSensorSim();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // AIS
	} // drivers
} // mpasv
