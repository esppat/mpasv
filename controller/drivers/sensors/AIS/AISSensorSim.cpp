/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AISSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/AIS/AISSensorSim.h"
#include "drivers/sensors/AIS/AISSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::AIS
		{

			AISSensorSim::AISSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
				: AISSensor(name)
				, m_simulationController(std::move(simulationController))
			{
			}

			AISSensorSim::~AISSensorSim()
			= default;

			void AISSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				const std::shared_ptr<AISSensorData> aisData = std::dynamic_pointer_cast<AISSensorData>(data);
				m_simulationController->update(aisData, name());
			}

		} // mpasv
