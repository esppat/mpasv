/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AISSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv::drivers::AIS
{

	class AISSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<AISSensorData>
	{
	public:

		AISSensorData() = default;
		AISSensorData(const AISSensorData& orig) = default;
		virtual ~AISSensorData() = default;

	public:

	protected:
	protected:
	private:
	private:
	};

}
