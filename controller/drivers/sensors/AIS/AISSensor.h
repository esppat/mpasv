/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AISSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace AIS
		{
			class AISSensorSim;

			class AISSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<AISSensor>
			{
			public:

				typedef AISSensorSim SimulatedType;

				AISSensor(const std::string & name);
				AISSensor(const AISSensor& orig) = delete;
				virtual ~AISSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // AIS
	} // drivers
} // mpasv
