/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AISSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/AIS/AISSensor.h"
#include "drivers/sensors/AIS/AISSensorData.h"


namespace mpasv::drivers::AIS
		{

			AISSensor::AISSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			AISSensor::~AISSensor()
			= default;

			void AISSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				const std::shared_ptr<AISSensorData> gpsData = std::dynamic_pointer_cast<AISSensorData>(data);

				// TODO
			}

		} // mpasv
