/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IMUSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"


namespace mpasv::drivers::IMU
{

	class IMUSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<IMUSensorData>
	{
	public:

		IMUSensorData() = default;
		IMUSensorData(const IMUSensorData& orig) = default;
		virtual ~IMUSensorData() = default;

	public:

		degree_t m_hullHeading = 0.0_deg;

		meters_per_second_squared_t m_accelX = 0.0_mps_sq;
		meters_per_second_squared_t m_accelY = 0.0_mps_sq;
		meters_per_second_squared_t m_accelZ = 0.0_mps_sq;

		degrees_per_second_t m_gyroX = 0.0_dps;
		degrees_per_second_t m_gyroY = 0.0_dps;
		degrees_per_second_t m_gyroZ = 0.0_dps;

	protected:
	protected:
	private:
	private:
	};

}
