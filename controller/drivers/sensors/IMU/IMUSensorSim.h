/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IMUSensorSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/sensors/IMU/IMUSensor.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace IMU
		{

			class IMUSensorSim
			: public drivers::IMU::IMUSensor
			, public tools::enable_shared_from_this<IMUSensorSim>
			{
			public:

				IMUSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				IMUSensorSim(const IMUSensorSim& orig) = delete;
				virtual ~IMUSensorSim();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // IMU
	} // drivers
} // mpasv
