/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IMUSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/IMU/IMUSensor.h"
#include "drivers/sensors/IMU/IMUSensorData.h"


namespace mpasv::drivers::IMU
		{

			IMUSensor::IMUSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			IMUSensor::~IMUSensor()
			= default;

			void IMUSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				const std::shared_ptr<IMUSensorData> imuData = std::dynamic_pointer_cast<IMUSensorData>(data);

				// TODO
				imuData->m_accelX = -9999.0_mps_sq;
				imuData->m_accelY = -9999.0_mps_sq;
				imuData->m_accelZ = -9999.0_mps_sq;
				imuData->m_gyroX = -9999.0_dps;
				imuData->m_gyroY = -9999.0_dps;
				imuData->m_gyroZ = -9999.0_dps;
			}

		} // mpasv
