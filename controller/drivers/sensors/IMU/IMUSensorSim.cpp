/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IMUSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/IMU/IMUSensorSim.h"
#include "drivers/sensors/IMU/IMUSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::IMU
		{

			IMUSensorSim::IMUSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
				: IMUSensor(name)
				, m_simulationController(std::move(simulationController))
			{
			}

			IMUSensorSim::~IMUSensorSim()
			= default;

			void IMUSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				const std::shared_ptr<IMUSensorData> imuData = std::dynamic_pointer_cast<IMUSensorData>(data);
				m_simulationController->update(imuData, name());
			}

		} // mpasv
