/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/ampere/AmperageSensor.h"
#include "drivers/sensors/ampere/AmperageSensorData.h"


namespace mpasv::drivers::ampere
		{

			AmperageSensor::AmperageSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			AmperageSensor::~AmperageSensor()
			= default;

			void AmperageSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				auto ampereData = std::dynamic_pointer_cast<AmperageSensorData>(data);

				// TODO
				ampereData->m_amperage = -9999.0_A;
			}

		} // mpasv
