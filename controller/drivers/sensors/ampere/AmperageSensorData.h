/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv
{
	namespace drivers
	{
		namespace ampere
		{

			class AmperageSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<AmperageSensorData>
			{
			public:
				AmperageSensorData();
				AmperageSensorData(const AmperageSensorData& orig) = delete;
				virtual ~AmperageSensorData();

			public:
				ampere_t m_amperage = 0.0_A;

			protected:
			protected:
			private:
			private:
			};

		} // ampere
	} // drivers
} // mpasv
