/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensorData.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#include "drivers/sensors/ampere/AmperageSensorData.h"

namespace mpasv::drivers::ampere
		{

			AmperageSensorData::AmperageSensorData()
			= default;

			AmperageSensorData::~AmperageSensorData()
			= default;

		} // mpasv
