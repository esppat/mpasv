/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensorSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/sensors/ampere/AmperageSensor.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace ampere
		{

			class AmperageSensorSim
			: public AmperageSensor
			, public tools::enable_shared_from_this<AmperageSensorSim>
			{
			public:

				AmperageSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				AmperageSensorSim(const AmperageSensorSim& orig) = delete;
				virtual ~AmperageSensorSim();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // ampere
	} // drivers
} // mpasv
