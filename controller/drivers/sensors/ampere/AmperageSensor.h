/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace ampere
		{
			class AmperageSensorSim;

			class AmperageSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<AmperageSensor>
			{
			public:

				typedef AmperageSensorSim SimulatedType;

				AmperageSensor(const std::string & name);
				AmperageSensor(const AmperageSensor& orig) = delete;
				virtual ~AmperageSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // ampere
	} // drivers
} // mpasv
