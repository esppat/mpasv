/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/ampere/AmperageSensorSim.h"
#include "drivers/sensors/ampere/AmperageSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::ampere
		{

			AmperageSensorSim::AmperageSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
				: AmperageSensor(name)
				, m_simulationController(std::move(simulationController))
			{
			}

			AmperageSensorSim::~AmperageSensorSim()
			= default;

			void AmperageSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				auto waterData = std::dynamic_pointer_cast<AmperageSensorData>(data);
				m_simulationController->update(waterData, name());
			}

		} // mpasv
