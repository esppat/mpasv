/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindSpeedSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/wind/WindSpeedSensor.h"
#include "drivers/sensors/wind/WindSpeedSensorData.h"


namespace mpasv::drivers::wind
{

	WindSpeedSensor::WindSpeedSensor(const std::string & name)
		: AbstractSensor(name)
	{
	}

	WindSpeedSensor::~WindSpeedSensor()
	= default;

	void WindSpeedSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
	{
		auto windData = std::dynamic_pointer_cast<WindSpeedSensorData>(data);

		// TODO
		windData->m_windSpeed = -9999.0_kts;
	}

} // mpasv
