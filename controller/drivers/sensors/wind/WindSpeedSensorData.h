/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindSpeedSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv
{
	namespace drivers
	{
		namespace wind
		{

			class WindSpeedSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<WindSpeedSensorData>
			{
			public:
				WindSpeedSensorData();
				WindSpeedSensorData(const WindSpeedSensorData& orig) = delete;
				virtual ~WindSpeedSensorData();

			public:
				knot_t m_windSpeed = 0.0_kts;

			protected:
			protected:
			private:
			private:
			};

		} // GPS
	} // drivers
} // mpasv
