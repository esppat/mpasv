/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindDirectionSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv::drivers::wind
{

	class WindDirectionSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<WindDirectionSensorData>
	{
	public:
		WindDirectionSensorData() = default;
		WindDirectionSensorData(const WindDirectionSensorData& orig) = default;
		virtual ~WindDirectionSensorData() = default;

	public:
		degree_t m_windDirection = 0.0_deg;

	protected:
	protected:
	private:
	private:
	};

}
