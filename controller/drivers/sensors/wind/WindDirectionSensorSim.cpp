/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindDirectionSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/wind/WindDirectionSensorSim.h"
#include "drivers/sensors/wind/WindDirectionSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::wind
{

	WindDirectionSensorSim::WindDirectionSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: WindDirectionSensor(name)
		, m_simulationController(std::move(simulationController))
	{
	}

	WindDirectionSensorSim::~WindDirectionSensorSim()
	= default;

	void WindDirectionSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
	{
		auto windData = std::dynamic_pointer_cast<WindDirectionSensorData>(data);
		m_simulationController->update(windData, name());
	}

} // mpasv
