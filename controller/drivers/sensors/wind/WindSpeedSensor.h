/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindSpeedSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace wind
		{
			class WindSpeedSensorSim;

			class WindSpeedSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<WindSpeedSensor>
			{
			public:

				typedef WindSpeedSensorSim SimulatedType;

				WindSpeedSensor(const std::string & name);
				WindSpeedSensor(const WindSpeedSensor& orig) = delete;
				virtual ~WindSpeedSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // GPS
	} // drivers
} // mpasv
