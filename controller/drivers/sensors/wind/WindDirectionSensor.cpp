/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindDirectionSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/wind/WindDirectionSensor.h"
#include "drivers/sensors/wind/WindDirectionSensorData.h"


namespace mpasv::drivers::wind
		{

			WindDirectionSensor::WindDirectionSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			WindDirectionSensor::~WindDirectionSensor()
			= default;

			void WindDirectionSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				auto windData = std::dynamic_pointer_cast<WindDirectionSensorData>(data);

				// TODO
				windData->m_windDirection = -9999.0_deg;
			}

		} // mpasv
