/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindDirectionSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace wind
		{
			class WindDirectionSensorSim;

			class WindDirectionSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<WindDirectionSensor>
			{
			public:

				typedef WindDirectionSensorSim SimulatedType;

				WindDirectionSensor(const std::string & name);
				WindDirectionSensor(const WindDirectionSensor& orig) = delete;
				virtual ~WindDirectionSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // GPS
	} // drivers
} // mpasv
