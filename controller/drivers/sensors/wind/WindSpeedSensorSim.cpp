/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindSpeedSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/wind/WindSpeedSensorSim.h"
#include "drivers/sensors/wind/WindSpeedSensorData.h"

#include "controller/simulation/SimulationController.h"

#include <utility>


namespace mpasv::drivers::wind
{

	WindSpeedSensorSim::WindSpeedSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: WindSpeedSensor(name)
		, m_simulationController(std::move(simulationController))
	{
	}

	WindSpeedSensorSim::~WindSpeedSensorSim()
	= default;

	void WindSpeedSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
	{
		auto windData = std::dynamic_pointer_cast<WindSpeedSensorData>(data);
		m_simulationController->update(windData, name());
	}

} // mpasv
