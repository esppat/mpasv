/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PropulsionWaterSpeedSensorSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/sensors/water/PropulsionWaterSpeedSensor.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace water
		{

			class PropulsionWaterSpeedSensorSim
			: public drivers::water::PropulsionWaterSpeedSensor
			, public tools::enable_shared_from_this<PropulsionWaterSpeedSensorSim>
			{
			public:

				PropulsionWaterSpeedSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				PropulsionWaterSpeedSensorSim(const PropulsionWaterSpeedSensorSim& orig) = delete;
				virtual ~PropulsionWaterSpeedSensorSim();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // GPS
	} // drivers
} // mpasv
