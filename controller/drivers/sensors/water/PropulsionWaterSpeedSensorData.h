/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PropulsionWaterSpeedSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv::drivers::water
{

	class PropulsionWaterSpeedSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<PropulsionWaterSpeedSensorData>
	{
	public:
		PropulsionWaterSpeedSensorData() = default;
		PropulsionWaterSpeedSensorData(const PropulsionWaterSpeedSensorData& orig) = default;
		virtual ~PropulsionWaterSpeedSensorData() = default;

	public:
		knot_t m_propulsionWaterSpeed = 0.0_kts;

	protected:
	protected:
	private:
	private:
	};

}
