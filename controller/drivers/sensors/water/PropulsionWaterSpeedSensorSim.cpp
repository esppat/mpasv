/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PropulsionWaterSpeedSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/water/PropulsionWaterSpeedSensorSim.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::water
		{

			PropulsionWaterSpeedSensorSim::PropulsionWaterSpeedSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
				: PropulsionWaterSpeedSensor(name)
				, m_simulationController(std::move(simulationController))
			{
			}

			PropulsionWaterSpeedSensorSim::~PropulsionWaterSpeedSensorSim()
			= default;

			void PropulsionWaterSpeedSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				auto waterData = std::dynamic_pointer_cast<PropulsionWaterSpeedSensorData>(data);
				m_simulationController->update(waterData, name());
			}

		} // mpasv
