/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PropulsionWaterSpeedSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/water/PropulsionWaterSpeedSensor.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensorData.h"


namespace mpasv::drivers::water
		{

			PropulsionWaterSpeedSensor::PropulsionWaterSpeedSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			PropulsionWaterSpeedSensor::~PropulsionWaterSpeedSensor()
			= default;

			void PropulsionWaterSpeedSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				auto waterData = std::dynamic_pointer_cast<PropulsionWaterSpeedSensorData>(data);

				// TODO
				waterData->m_propulsionWaterSpeed = -9999.0_kts;
			}

		} // mpasv
