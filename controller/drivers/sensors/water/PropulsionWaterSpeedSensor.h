/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PropulsionWaterSpeedSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace water
		{
			class PropulsionWaterSpeedSensorSim;

			class PropulsionWaterSpeedSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<PropulsionWaterSpeedSensor>
			{
			public:

				typedef PropulsionWaterSpeedSensorSim SimulatedType;

				PropulsionWaterSpeedSensor(const std::string & name);
				PropulsionWaterSpeedSensor(const PropulsionWaterSpeedSensor& orig) = delete;
				virtual ~PropulsionWaterSpeedSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // GPS
	} // drivers
} // mpasv
