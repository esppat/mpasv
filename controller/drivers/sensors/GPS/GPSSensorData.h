/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractSensorData.h"

#include "tools/physics/Units.h"

#include "tools/gps/GeoPos.h"


namespace mpasv::drivers::GPS
{

	class GPSSensorData
			: public drivers::AbstractSensorData
			, public tools::enable_shared_from_this<GPSSensorData>
	{
	public:

		GPSSensorData() = default;
		GPSSensorData(const GPSSensorData& orig) = default;
		virtual ~GPSSensorData() = default;

	public:

		bool m_hasFix = false;
		tools::geo::GeoPos m_geoPos;
		degree_t m_gpsCourse = 0.0_deg;
		knot_t m_gpsSpeed = 0.0_kts;

	protected:
	protected:
	private:
	private:
	};

}
