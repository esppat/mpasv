/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/GPS/GPSSensor.h"
#include "drivers/sensors/GPS/GPSSensorData.h"


namespace mpasv::drivers::GPS
		{

			GPSSensor::GPSSensor(const std::string & name)
				: AbstractSensor(name)
			{
			}

			GPSSensor::~GPSSensor()
			= default;

			void GPSSensor::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				const std::shared_ptr<GPSSensorData> gpsData = std::dynamic_pointer_cast<GPSSensorData>(data);

				// TODO
				gpsData->m_hasFix = false;
				gpsData->m_geoPos.unSet();
				gpsData->m_gpsCourse = -9999.0_deg;
				gpsData->m_gpsSpeed = -9999.0_kts;
			}

		} // mpasv
