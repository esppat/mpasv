/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensorSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "drivers/sensors/GPS/GPSSensorSim.h"
#include "drivers/sensors/GPS/GPSSensorData.h"

#include "controller/simulation/SimulationController.h"

#include "virtual_captain/VirtualCaptain.h"

#include <utility>


namespace mpasv::drivers::GPS
		{

			GPSSensorSim::GPSSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
				: GPSSensor(name)
				, m_simulationController(std::move(simulationController))
			{
			}

			GPSSensorSim::~GPSSensorSim()
			= default;

			void GPSSensorSim::pullImpl(const std::shared_ptr<AbstractDriverData> data)
			{
				const std::shared_ptr<GPSSensorData> gpsData = std::dynamic_pointer_cast<GPSSensorData>(data);
				m_simulationController->update(gpsData, name());
			}

		} // mpasv
