/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"


namespace mpasv
{
	namespace drivers
	{
		namespace GPS
		{
			class GPSSensorSim;

			class GPSSensor
			: public drivers::AbstractSensor
			, public tools::enable_shared_from_this<GPSSensor>
			{
			public:

				typedef GPSSensorSim SimulatedType;

				GPSSensor(const std::string & name);
				GPSSensor(const GPSSensor& orig) = delete;
				virtual ~GPSSensor();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // GPS
	} // drivers
} // mpasv
