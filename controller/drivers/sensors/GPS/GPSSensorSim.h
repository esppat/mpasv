/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensorSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/sensors/GPS/GPSSensor.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace GPS
		{

			class GPSSensorSim
			: public drivers::GPS::GPSSensor
			, public tools::enable_shared_from_this<GPSSensorSim>
			{
			public:

				GPSSensorSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				GPSSensorSim(const GPSSensorSim& orig) = delete;
				virtual ~GPSSensorSim();

			protected:

				virtual void pullImpl(const std::shared_ptr<AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // GPS
	} // drivers
} // mpasv
