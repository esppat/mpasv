/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DriverFactory.h
 * Author: esppat
 *
 * Created on December 18, 2019, 12:14 AM
 */

#pragma once

#include "tools/Demangler.h"
#include "tools/Exception.h"
#include "tools/Tools.h"
#include "tools/parameters/ParameterManager.h"

#include "drivers/AbstractDriver.h"
#include "drivers/AbstractSensor.h"
#include "drivers/AbstractActuator.h"

#include <map>


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{

		class DriverFactory
		: public tools::enable_shared_from_this<DriverFactory>
		{
		public:

			DriverFactory(std::shared_ptr<controller::simulation::SimulationController> simulationController);
			DriverFactory(const DriverFactory& orig) = delete;
			virtual ~DriverFactory();

			void setSimulated()
			{
				m_simulated = true;
			}

			template <class DRIVER>
			std::shared_ptr<DRIVER> spawnDriver(const std::string & name)
			{
				if (param<bool>("VirtualCaptain.Simulation.Do simulation (bool)"))
				{
					CHECK_NOT_NULLPTR(m_simulationController);
					LOG_DEBUG("Spawning driver \"", typeName<typename DRIVER::SimulatedType>(), "\" named \"", name, "\"");
					return std::make_shared<typename DRIVER::SimulatedType > (name, m_simulationController);
				}
				else
				{
					LOG_DEBUG("Spawning driver \"", typeName<DRIVER>(), "\" named \"", name, "\"");
					return std::make_shared<DRIVER>(name);
				}
			}

		protected:
		protected:
		private:

			bool simulated() const
			{
				return m_simulated;
			}

		private:

			bool m_simulated = false;

			std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

		};

	} // drivers
} // mpasv
