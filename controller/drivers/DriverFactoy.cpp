/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DriverFactory.cpp
 * Author: esppat
 *
 * Created on December 18, 2019, 12:14 AM
 */

#include <utility>
#include "drivers/DriverFactory.h"

#include "tools/Exception.h"


namespace mpasv::drivers
	{

		DriverFactory::DriverFactory(std::shared_ptr<controller::simulation::SimulationController> simulationController)
			: m_simulationController(std::move(simulationController))
		{
		}

		DriverFactory::~DriverFactory()
		= default;

	} // mpasv
