/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#pragma once

#include "drivers/AbstractDriver.h"

#include <memory>


namespace mpasv
{
	namespace drivers
	{

		class AbstractSensorData;

		class AbstractSensor
		: public drivers::AbstractDriver
		, public tools::enable_shared_from_this<AbstractSensor>
		{
		public:
			AbstractSensor(const std::string & name);
			AbstractSensor(const AbstractSensor& orig) = delete;
			virtual ~AbstractSensor();

		protected:

			virtual bool canPull() const override final
			{
				return true;
			}

		protected:
		private:
		private:
		};

	} // drivers
} // mpasv
