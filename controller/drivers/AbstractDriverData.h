/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractDriverData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:18 PM
 */

#pragma once

#include "tools/Tools.h"


namespace mpasv::drivers
{

	class AbstractDriverData
			: public tools::enable_shared_from_this<AbstractDriverData>
	{
	public:
		AbstractDriverData() = default;
		AbstractDriverData(const AbstractDriverData& orig) = default;
		virtual ~AbstractDriverData() = default;

	protected:
	protected:
	private:
	private:
	};

}
