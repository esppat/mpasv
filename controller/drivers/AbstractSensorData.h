/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractSensorData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:18 PM
 */

#pragma once

#include "drivers/AbstractDriverData.h"

#include "tools/Tools.h"


namespace mpasv::drivers
{

	class AbstractSensorData
			: public AbstractDriverData
			, public tools::enable_shared_from_this<AbstractSensorData>
	{
	public:
		AbstractSensorData() = default;
		AbstractSensorData(const AbstractSensorData& orig) = default;
		virtual ~AbstractSensorData() = default;

	protected:
	protected:
	private:
	private:
	};

}
