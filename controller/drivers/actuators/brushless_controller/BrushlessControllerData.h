/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessControllerData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractActuatorData.h"

#include "tools/physics/Units.h"
#include "tools/physics/RegulatedValue.h"


namespace mpasv::drivers::brushless_controller
{

	class BrushlessControllerData
			: public drivers::AbstractActuatorData
			, public tools::enable_shared_from_this<BrushlessControllerData>
	{
	public:
		BrushlessControllerData();
		BrushlessControllerData(const BrushlessControllerData& orig) = default;
		virtual ~BrushlessControllerData() = default;

	public:

		tools::physics::RegulatedValue<percent_t, percents_per_second_t> m_throttle;

	protected:
	protected:
	private:
	private:
	};

}
