/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessController.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#include <utility>
#include "drivers/actuators/brushless_controller/BrushlessControllerSim.h"
#include "drivers/actuators/brushless_controller/BrushlessControllerData.h"

#include "controller/simulation/SimulationController.h"


namespace mpasv::drivers::brushless_controller
		{

			BrushlessControllerSim::BrushlessControllerSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
				: BrushlessController(name)
				, m_simulationController(std::move(simulationController))
			{
			}

			BrushlessControllerSim::~BrushlessControllerSim()
			= default;

			void BrushlessControllerSim::pushImpl(const std::shared_ptr<const AbstractDriverData> data)
			{
				const std::shared_ptr<const BrushlessControllerData> brushlessControllerData = std::dynamic_pointer_cast<const BrushlessControllerData>(data);
				m_simulationController->update(brushlessControllerData, name());
			}

		} // mpasv
