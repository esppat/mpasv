/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessControllerSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#pragma once

#include "drivers/actuators/brushless_controller/BrushlessController.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace brushless_controller
		{

			class BrushlessControllerSim
			: public BrushlessController
			, public tools::enable_shared_from_this<BrushlessControllerSim>
			{
			public:
				BrushlessControllerSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				BrushlessControllerSim(const BrushlessController& orig) = delete;
				virtual ~BrushlessControllerSim();

			protected:
				virtual void pushImpl(const std::shared_ptr<const AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // brushless_controller
	} // drivers
} // mpasv
