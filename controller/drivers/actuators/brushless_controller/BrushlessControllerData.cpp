/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessControllerData.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#include "drivers/actuators/brushless_controller/BrushlessControllerData.h"

#include "tools/parameters/ParameterManager.h"

namespace mpasv::drivers::brushless_controller
{

	BrushlessControllerData::BrushlessControllerData()
		: m_throttle(0.0_pct, param<percents_per_second_t, double>("PhysicalSimulator.Throttle change (pct_per_sec)"), -100.0_pct, +100.0_pct)
	{
	}

} // mpasv
