/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessController.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#pragma once

#include "drivers/AbstractActuator.h"


namespace mpasv
{
	namespace drivers
	{
		namespace brushless_controller
		{
			class BrushlessControllerSim;

			class BrushlessController
			: public drivers::AbstractActuator
			, public tools::enable_shared_from_this<BrushlessController>
			{
			public:

				typedef BrushlessControllerSim SimulatedType;

				BrushlessController(const std::string & name);
				BrushlessController(const BrushlessController& orig) = delete;
				virtual ~BrushlessController();

			protected:

				virtual void pushImpl(const std::shared_ptr<const AbstractDriverData> data) override;

			protected:
			private:
			private:
			};

		} // brushless_controller
	} // drivers
} // mpasv
