/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessController.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#include "drivers/actuators/brushless_controller/BrushlessController.h"
#include "drivers/actuators/brushless_controller/BrushlessControllerData.h"


namespace mpasv::drivers::brushless_controller
		{

			BrushlessController::BrushlessController(const std::string & name)
				: AbstractActuator(name)
			{
			}

			BrushlessController::~BrushlessController()
			= default;

			void BrushlessController::pushImpl(const std::shared_ptr<const AbstractDriverData> /*data*/)
			{
				// TODO
			}

		} // mpasv
