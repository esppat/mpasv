/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServo.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#pragma once

#include "drivers/AbstractActuator.h"


namespace mpasv
{
	namespace drivers
	{
		namespace pwm_servo
		{
			class PWMServoSim;

			class PWMServo
			: public drivers::AbstractActuator
			, public tools::enable_shared_from_this<PWMServo>
			{
			public:
				typedef PWMServoSim SimulatedType;

				PWMServo(const std::string & name);
				PWMServo(const PWMServo& orig) = delete;
				virtual ~PWMServo();
				
				void setStroke(const degree_t stroke)
				{
					CHECK_GT(stroke, 0.0_deg);
					m_stroke = stroke;
				}
				
				degree_t stroke() const
				{
					CHECK_GT(m_stroke, 0.0_deg);
					return m_stroke;
				}

				void setAngularVelocity(const degrees_per_second_t angularVelocity)
				{
					CHECK_GT(angularVelocity, 0.0_dps);
					m_angularVelocity = angularVelocity;
				}

				degrees_per_second_t angularVelocity() const
				{
					CHECK_GT(m_angularVelocity, 0.0_dps);
					return m_angularVelocity;
				}

			protected:
				virtual void pushImpl(const std::shared_ptr<const AbstractDriverData> data) override;

			protected:
			private:
			private:
				
				degree_t m_stroke = 0.0_deg;
				degrees_per_second_t m_angularVelocity = 0.0_dps;
				
			};

		} // pwm_servo
	} // drivers
} // mpasv
