/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServoData.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#include "drivers/actuators/pwm_servo/PWMServoData.h"
#include "drivers/actuators/pwm_servo/PWMServo.h"
#include "tools/parameters/ParameterManager.h"

#include <utility>


namespace mpasv::drivers::pwm_servo
{

	PWMServoData::PWMServoData(std::shared_ptr<PWMServo> servo)
		: m_percent(0.0_pct, param<percents_per_second_t, double>("PhysicalSimulator.Servo rotation (pps)"), -100.0_pct, +100.0_pct)
		, m_servo(std::move(servo))
	{
		CHECK_NOT_NULLPTR(m_servo);
	}

	percent_t PWMServoData::roundPercent() const
	{
		return round(m_percent.get());
	}

	degree_t PWMServoData::roundAngle() const
	{
		return stroke() * roundPercent();
	}

	degree_t PWMServoData::stroke() const
	{
		return m_servo->stroke();
	}

	degrees_per_second_t PWMServoData::angularVelocity() const
	{
		return m_servo->angularVelocity();
	}

} // mpasv
