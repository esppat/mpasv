/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServo.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#include "drivers/actuators/pwm_servo/PWMServo.h"
#include "drivers/actuators/pwm_servo/PWMServoData.h"


namespace mpasv::drivers::pwm_servo
		{

			PWMServo::PWMServo(const std::string & name)
				: AbstractActuator(name)
			{
			}

			PWMServo::~PWMServo()
			= default;

			void PWMServo::pushImpl(const std::shared_ptr<const AbstractDriverData> /*data*/)
			{
				// TODO
			}

		} // mpasv
