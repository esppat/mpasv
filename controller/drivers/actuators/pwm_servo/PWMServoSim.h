/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServoSim.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#pragma once

#include "drivers/actuators/pwm_servo/PWMServo.h"


namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		namespace pwm_servo
		{

			class PWMServoSim
			: public PWMServo
			, public tools::enable_shared_from_this<PWMServoSim>
			{
			public:
				PWMServoSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController);
				PWMServoSim(const PWMServo& orig) = delete;
				virtual ~PWMServoSim();

			protected:
				virtual void pushImpl(const std::shared_ptr<const AbstractDriverData> data) override;

			protected:
			private:
			private:

				std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			};

		} // pwm_servo
	} // drivers
} // mpasv
