/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServoSim.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#include "drivers/actuators/pwm_servo/PWMServoSim.h"
#include "drivers/actuators/pwm_servo/PWMServoData.h"

#include "controller/simulation/SimulationController.h"

#include <utility>


namespace mpasv::drivers::pwm_servo
{

	PWMServoSim::PWMServoSim(const std::string & name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: PWMServo(name)
		, m_simulationController(std::move(simulationController))
	{
	}

	PWMServoSim::~PWMServoSim()
	= default;

	void PWMServoSim::pushImpl(const std::shared_ptr<const AbstractDriverData> data)
	{
		const std::shared_ptr<const PWMServoData> pwmServoData = std::dynamic_pointer_cast<const PWMServoData>(data);
		m_simulationController->update(pwmServoData, name());
	}

} // mpasv
