/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServoData.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:23 PM
 */

#pragma once

#include "drivers/AbstractActuatorData.h"

#include "tools/physics/Units.h"
#include "tools/physics/RegulatedValue.h"


namespace mpasv::drivers::pwm_servo
{
	class PWMServo;

	class PWMServoData
			: public drivers::AbstractActuatorData
			, public tools::enable_shared_from_this<PWMServoData>
	{
	public:

		PWMServoData(std::shared_ptr<PWMServo> servo);
		PWMServoData(const PWMServoData& orig) = default;
		virtual ~PWMServoData() = default;

		percent_t roundPercent() const;
		degree_t roundAngle() const;
		degree_t stroke() const;
		degrees_per_second_t angularVelocity() const;

	public:

		tools::physics::RegulatedValue<percent_t, percents_per_second_t> m_percent;
		std::shared_ptr<PWMServo> m_servo;

	protected:
	protected:
	private:
	private:
	};

}
