/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   LevelOfAutonomy.h
 * Author: esppat
 *
 * Created on 1 décembre 2018, 22:19
 */

#pragma once

namespace mpasv
{
	namespace virtual_captain
	{

		enum class LevelOfAutonomy
		{
			Sheridan_1, /* Computer offers no assistance; human does it all */
			Sheridan_2, /* Computer offers a complete set of action alternatives */
			Sheridan_3, /* Computer narrows the selection down to a few choices */
			Sheridan_4, /* Computer suggests a single action */
			Sheridan_5, /* Computer executes that action if human approves */
			Sheridan_6, /* Computer allows the human limited time to veto before automatic execution */
			Sheridan_7, /* Computer executes automatically then necessarily informs the human */
			Sheridan_8, /* Computer informs human after automatic execution only if human asks */
			Sheridan_9, /* Computer informs human after automatic execution only if it decides to */
			Sheridan_10 /* Computer decides everything and acts autonomously, ignoring the human */
		};

	} // virtual_captain
} // mpasv
