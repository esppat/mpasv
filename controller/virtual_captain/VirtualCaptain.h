/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VirtualCaptain.h
 * Author: esppat
 *
 * Created on 18 novembre 2018, 14:07
 */

#pragma once

#include "tools/Tools.h"
#include "tools/ThreadedTask.h"

#include <memory>


namespace mpasv
{
	namespace controller
	{
		namespace communication
		{
			class CommunicationController;
		}

		namespace route
		{
			class RouteController;
		}

		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{
		class DriverManager;
		class DriverFactory;
	}

	namespace virtual_captain
	{
		class Alert;

		class VirtualCaptain
		: public tools::enable_shared_from_this<VirtualCaptain>
		, public tools::ThreadedTask
		{
		public:

			VirtualCaptain();
			VirtualCaptain(const VirtualCaptain& orig) = delete;
			virtual ~VirtualCaptain();

			void alert(const std::shared_ptr<const virtual_captain::Alert>& alert);

		private:

			virtual bool setup() noexcept override;
			virtual void initialize() noexcept override;
			virtual void run(const second_t tickDuration) noexcept override;
			virtual void finalize() noexcept override;

			void initializeDrivers();

		private:

			std::shared_ptr<controller::route::RouteController> m_routeController;

			std::shared_ptr<drivers::DriverManager> m_driverManager;
			std::shared_ptr<drivers::DriverFactory> m_driverFactory;

			std::shared_ptr<controller::simulation::SimulationController> m_simulationController;

			std::shared_ptr<controller::communication::CommunicationController> m_communicationController;
		};

	} // virtual_captain
} // mpasv
