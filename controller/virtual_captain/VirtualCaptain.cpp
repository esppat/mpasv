/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VirtualCaptain.cpp
 * Author: esppat
 *
 * Created on 18 novembre 2018, 14:07
 */

#include "virtual_captain/VirtualCaptain.h"

#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"

#include "controller/route/RouteController.h"
#include "controller/route/RouteControllerGoal.h"
#include "controller/route/RouteControllerGoalFollowRoute.h"

#include "controller/simulation/SimulationController.h"
#include "controller/simulation/SimulationController_ReactPhysics3d.h"
//#include "controller/simulation/SimulationController_Basic.h"
//#include "controller/simulation/SimulationController_Bullet.h"

#include "controller/communication/CommunicationController.h"

#include "drivers/DriverManager.h"
#include "drivers/DriverFactory.h"
#include "drivers/actuators/brushless_controller/BrushlessController.h"
#include "drivers/actuators/brushless_controller/BrushlessControllerSim.h"
#include "drivers/actuators/pwm_servo/PWMServo.h"
#include "drivers/actuators/pwm_servo/PWMServoSim.h"
#include "drivers/sensors/GPS/GPSSensor.h"
#include "drivers/sensors/GPS/GPSSensorSim.h"
#include "drivers/sensors/AIS/AISSensor.h"
#include "drivers/sensors/AIS/AISSensorSim.h"
#include "drivers/sensors/IMU/IMUSensor.h"
#include "drivers/sensors/IMU/IMUSensorSim.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensor.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensorSim.h"
#include "drivers/sensors/wind/WindDirectionSensor.h"
#include "drivers/sensors/wind/WindDirectionSensorSim.h"
#include "drivers/sensors/wind/WindSpeedSensor.h"
#include "drivers/sensors/wind/WindSpeedSensorSim.h"
#include "drivers/sensors/ampere/AmperageSensor.h"
#include "drivers/sensors/ampere/AmperageSensorSim.h"
#include "drivers/sensors/voltage/VoltageSensor.h"
#include "drivers/sensors/voltage/VoltageSensorSim.h"
#include "controller/situation/GlobalSituationController.h"

#include <memory>


namespace mpasv::virtual_captain
{

	VirtualCaptain::VirtualCaptain()
		: tools::ThreadedTask("VirtualCaptain")
	{
		LOG_DEBUG("");

//		SimulationController_ReactPhysics3d::fullTest();
	}

	VirtualCaptain::~VirtualCaptain()
	{
		LOG_ERR("------------------- DELETING VIRTUAL CAPTAIN -------------------");
	}

	void VirtualCaptain::alert(const std::shared_ptr<const virtual_captain::Alert>& /*alert*/)
	{
		// TODO
	}

	bool VirtualCaptain::setup() noexcept
	{
		bool result = false;

		try
		{
			m_communicationController = std::make_shared<controller::communication::CommunicationController>();
			addPriorTask(m_communicationController);

			m_driverManager = std::make_shared<drivers::DriverManager>();

			if (param<bool>("VirtualCaptain.Simulation.Do simulation (bool)"))
			{
				m_simulationController = std::make_shared<controller::simulation::SimulationController_ReactPhysics3d>();
				//					m_simulationController = std::make_shared<controller::simulation::SimulationController_Basic>();
				//					m_simulationController = std::make_shared<controller::simulation::SimulationController_Bullet>();
				m_simulationController->setDriverManager(m_driverManager);
				addPriorTask(m_simulationController);
			}

			m_driverFactory = std::make_shared<drivers::DriverFactory>(m_simulationController);

			initializeDrivers();

			m_routeController = std::make_shared<controller::route::RouteController>();
			m_routeController->setDriverManager(m_driverManager);
			addPriorTask(m_routeController);

			m_communicationController->setRouteController(m_routeController);

			// TODO

			result = ThreadedTask::setup();
		}
		catch (...)
		{
			LOG_ERR("Exception caught");
		}

		return result;
	}

	void VirtualCaptain::initialize() noexcept
	{
		ThreadedTask::initialize();

		// TODO

		auto goal = std::make_shared<controller::route::RouteControllerGoalFollowRoute>();

		goal->loadFromFile(param<std::string>("VirtualCaptain.Route"));
		goal->setSpeed(3.0_kts);

		m_routeController->pushGoal(goal);

		if (param<bool>("VirtualCaptain.Simulation.Do simulation (bool)"))
		{
			m_simulationController->setStartPoint(goal->route().getWaypoint(0).pos().forwardTo(param<degree_t, double>("VirtualCaptain.Simulation.Start azymuth decal (deg)"),
																							   param<meter_t, double>("VirtualCaptain.Simulation.Start distance decal (m)")),
												  param<degree_t, double>("VirtualCaptain.Simulation.Start hull degree_t (deg)"));
		}
	}

	void VirtualCaptain::run(const second_t /*tickDuration*/) noexcept
	{
		// TODO
	}

	void VirtualCaptain::finalize() noexcept
	{
		// TODO
		m_routeController.reset();
		m_simulationController.reset();
		m_driverFactory.reset();
		m_driverManager.reset();
		m_communicationController.reset();

		ThreadedTask::finalize();
	}

	void VirtualCaptain::initializeDrivers()
	{
		// IMU
		auto IMUSensor = m_driverFactory->spawnDriver<drivers::IMU::IMUSensor>("IMU 1");
		// TODO init sensor
		m_driverManager->addDriver(IMUSensor);

		// GPS
		auto GPSSensor = m_driverFactory->spawnDriver<drivers::GPS::GPSSensor>("GPS 1");
		// TODO init sensor
		m_driverManager->addDriver(GPSSensor);

		// AIS
		auto AISSensor = m_driverFactory->spawnDriver<drivers::AIS::AISSensor>("AIS 1");
		// TODO init sensor
		m_driverManager->addDriver(AISSensor);

		// water
		auto propulsionWaterSpeedSensor = m_driverFactory->spawnDriver<drivers::water::PropulsionWaterSpeedSensor>("Lock 1");
		// TODO init sensor
		m_driverManager->addDriver(propulsionWaterSpeedSensor);

		// water
		auto windSpeedSensor = m_driverFactory->spawnDriver<drivers::wind::WindSpeedSensor>("Wind 1");
		// TODO init sensor
		m_driverManager->addDriver(windSpeedSensor);

		// water
		auto windDirectionSensor = m_driverFactory->spawnDriver<drivers::wind::WindDirectionSensor>("Wind 1");
		// TODO init sensor
		m_driverManager->addDriver(windDirectionSensor);

		// Helm servo
		auto helmServo = m_driverFactory->spawnDriver<drivers::pwm_servo::PWMServo>("Helm 1");
		helmServo->setStroke(param<degree_t, double>("HelmController.Maximum helm angle (deg)"));
		helmServo->setAngularVelocity(param<degrees_per_second_t, double>("HelmController.Angular velocity (dps)"));
		// TODO init actuator
		m_driverManager->addDriver(helmServo);

		// Engine brushless controller 1
		auto brushlessControllerEngine1 = m_driverFactory->spawnDriver<drivers::brushless_controller::BrushlessController>("Engine 1");
		// TODO init actuator
		m_driverManager->addDriver(brushlessControllerEngine1);

		if (param<bool>("PhysicalDescription.Has two hulls (bool)"))
		{
			// Engine brushless controller 2
			auto brushlessControllerEngine2 = m_driverFactory->spawnDriver<drivers::brushless_controller::BrushlessController>("Engine 2");
			// TODO init actuator
			m_driverManager->addDriver(brushlessControllerEngine2);
		}

		auto enginesAmperageSensor = m_driverFactory->spawnDriver<drivers::ampere::AmperageSensor>("Engine 1");
		// TODO init sensor
		m_driverManager->addDriver(enginesAmperageSensor);

		auto enginesVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Engine 1");
		// TODO init sensor
		m_driverManager->addDriver(enginesVoltageSensor);

		if (param<bool>("PhysicalDescription.Has two hulls (bool)"))
		{
			enginesAmperageSensor = m_driverFactory->spawnDriver<drivers::ampere::AmperageSensor>("Engine 2");
			// TODO init sensor
			m_driverManager->addDriver(enginesAmperageSensor);

			enginesVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Engine 2");
			// TODO init sensor
			m_driverManager->addDriver(enginesVoltageSensor);
		}
	}

} // mpasv
