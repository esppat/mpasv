/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PositionController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/position/PositionController.h"

#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/physics/Units.h"
#include "tools/gps/GeoPos.h"

#include "drivers/DriverManager.h"
#include "drivers/actuators/brushless_controller/BrushlessController.h"
#include "drivers/actuators/brushless_controller/BrushlessControllerData.h"
#include "drivers/actuators/pwm_servo/PWMServo.h"
#include "drivers/actuators/pwm_servo/PWMServoData.h"


namespace mpasv::controller::position
{

	PositionController::PositionController()
		: Controller("PositionController")
	{
	}

	PositionController::~PositionController()
	= default;

	void PositionController::setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	void PositionController::setCurrentGPSSpeed(const knot_t speed)
	{
		CHECK_GE(speed, 0.0_kts);
		m_currentGPSSpeed = speed;
	}

	void PositionController::setTargetGPSSpeed(const knot_t speed, bool forward)
	{
		m_targetGPSSpeed = speed;
		m_moveForward = forward;
	}

	void PositionController::setLinearAccelerationsAndRotationVelocity(const meters_per_second_squared_t linearAcceleration,
																	   const degrees_per_second_t angularVelocity)
	{
		m_linearAcceleration = linearAcceleration;
		m_angularVelocity = angularVelocity;
	}

	void PositionController::setCurrentHullBearing(const degree_t bearing)
	{
		CHECK_VALID_BEARING(bearing);
		m_currentHullBearing = bearing;
	}

	void PositionController::setCurrentGPSBearing(const degree_t bearing)
	{
		CHECK_VALID_BEARING(bearing);
		m_currentGPSBearing = bearing;
	}

	void PositionController::setTargetGPSBearing(const degree_t bearing)
	{
		CHECK_VALID_BEARING(bearing);
		m_targetGPSBearing = bearing;
	}

	percent_t PositionController::helmPercent() const
	{
		return m_pwmServoData_helm->roundPercent();
	}

	degree_t PositionController::helmAngle() const
	{
		return m_pwmServoData_helm->roundAngle();
	}

	percent_t PositionController::throttle1() const
	{
		return m_brushlessControllerData_engine1->m_throttle.get();
	}

	percent_t PositionController::throttle2() const
	{
		return m_brushlessControllerData_engine2->m_throttle.get();
	}

	bool PositionController::setup() noexcept
	{
		bool result = false;

		try
		{
			m_brushlessController_engine1 = m_driverManager->getActuator<drivers::brushless_controller::BrushlessController>("Engine 1");
			m_brushlessControllerData_engine1 = std::make_shared<drivers::brushless_controller::BrushlessControllerData>();

			m_brushlessController_engine2 = m_driverManager->getActuator<drivers::brushless_controller::BrushlessController>("Engine 2");
			m_brushlessControllerData_engine2 = std::make_shared<drivers::brushless_controller::BrushlessControllerData>();

			m_pwmServo_helm = m_driverManager->getActuator<drivers::pwm_servo::PWMServo>("Helm 1");
			m_pwmServoData_helm = std::make_shared<drivers::pwm_servo::PWMServoData>(m_pwmServo_helm);

			// TODO

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void PositionController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void PositionController::run(const second_t /*tickDuration*/) noexcept
	{
		calculateThrottle();
		calculateHelmAngle();

		m_brushlessControllerData_engine1->m_throttle.set(m_throttle1);
		m_brushlessControllerData_engine2->m_throttle.set(m_throttle2);

		m_brushlessController_engine1->push(m_brushlessControllerData_engine1);
		m_brushlessController_engine2->push(m_brushlessControllerData_engine2);

		m_pwmServoData_helm->m_percent.set(m_helmPercent);

		m_pwmServo_helm->push(m_pwmServoData_helm);
	}

	void PositionController::finalize() noexcept
	{
		m_brushlessController_engine1.reset();
		m_brushlessControllerData_engine1.reset();

		m_brushlessController_engine2.reset();
		m_brushlessControllerData_engine2.reset();

		m_pwmServo_helm.reset();
		m_pwmServoData_helm.reset();

		// TODO

		Controller::finalize();
	}

	void PositionController::calculateThrottle()
	{
		if (m_targetGPSSpeed == 0.0_kts)
		{
			// TODO should be an active process, no just cut engines
			m_throttle1 = 0.0_pct;
			m_throttle2 = 0.0_pct;
		}
		else
		{
			const knot_t speedDiff = m_targetGPSSpeed - m_currentGPSSpeed;

			m_throttle1 += (speedDiff / 50.0_kts);
			m_throttle2 += (speedDiff / 50.0_kts);

			// TODO limit to N percent (as a param)

			if (m_moveForward)
			{
				mpasv::tools::setInLimit(m_throttle1, 0.0_pct, +100.0_pct);
				mpasv::tools::setInLimit(m_throttle2, 0.0_pct, +100.0_pct);
			}
			else
			{
				mpasv::tools::setInLimit(m_throttle1, -100.0_pct, 0.0_pct);
				mpasv::tools::setInLimit(m_throttle2, -100.0_pct, 0.0_pct);
			}
		}
	}

	template <typename T>
	bool sign(const T & t)
	{
		return t >= T(0);
	}

	void PositionController::calculateHelmAngle()
	{
		degree_t courseToAdd = m_targetGPSBearing - m_currentGPSBearing;
		courseToAdd = normalize180(courseToAdd);

		degree_t servoStroke = m_pwmServo_helm->stroke();
		m_helmPercent = (-courseToAdd / servoStroke);

		if (sign(courseToAdd) == sign(m_angularVelocity))
		{
			// rotation in the correct sense

			// calculate time to obtain target bearing
			second_t timeToBearing = fabs(courseToAdd / m_angularVelocity);

			// calculate time to set servo to neutral
			degrees_per_second_t servoAngularVelocity = m_pwmServo_helm->angularVelocity();

			second_t timeToNeutral = fabs((m_helmPercent * servoStroke) / servoAngularVelocity);

			if (timeToNeutral >= timeToBearing + 1.0_s)
			{
				m_helmPercent = 0_pct;
			}

//			LOG_DEBUG("helmPercent=", m_helmPercent, " angularVelocity=", m_angularVelocity, " timeToBearing=", timeToBearing, " timeToNeutral=", timeToNeutral);
		}
		else
		{
//			LOG_DEBUG("helmPercent=", m_helmPercent, " angularVelocity=", m_angularVelocity);
		}

		// TODO limit to N percent (as a param)
		// TODO use gain (as a param)

		mpasv::tools::setInLimit(m_helmPercent, -100.0_pct, +100.0_pct);
	}

}
