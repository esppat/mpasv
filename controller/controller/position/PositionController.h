/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PositionController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/physics/ValueHistory.h"


namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace brushless_controller
		{
			class BrushlessController;
			class BrushlessControllerData;
		}

		namespace pwm_servo
		{
			class PWMServo;
			class PWMServoData;
		}
	}

	namespace controller::position
	{

		class PositionController
				: public controller::Controller
		{
		public:

			PositionController();
			virtual ~PositionController();

			void setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager);

			void setCurrentGPSSpeed(const knot_t speed);
			void setTargetGPSSpeed(const knot_t speed, bool forward);

			void setLinearAccelerationsAndRotationVelocity(const meters_per_second_squared_t linearAcceleration,
														   const degrees_per_second_t angularVelocity);

			void setCurrentHullBearing(const degree_t degree_t);
			void setCurrentGPSBearing(const degree_t degree_t);
			void setTargetGPSBearing(const degree_t degree_t);

			percent_t helmPercent() const;
			degree_t helmAngle() const;
			percent_t throttle1() const;
			percent_t throttle2() const;

		protected:
		protected:
		private:

			virtual bool setup() noexcept override;
			virtual void initialize() noexcept override;
			virtual void run(const second_t tickDuration) noexcept override;
			virtual void finalize() noexcept override;

			void calculateThrottle();
			void calculateHelmAngle();

		private:

			std::shared_ptr<drivers::DriverManager> m_driverManager;

			std::shared_ptr<drivers::brushless_controller::BrushlessController> m_brushlessController_engine1;
			std::shared_ptr<drivers::brushless_controller::BrushlessControllerData> m_brushlessControllerData_engine1;

			std::shared_ptr<drivers::brushless_controller::BrushlessController> m_brushlessController_engine2;
			std::shared_ptr<drivers::brushless_controller::BrushlessControllerData> m_brushlessControllerData_engine2;

			std::shared_ptr<drivers::pwm_servo::PWMServo> m_pwmServo_helm;
			std::shared_ptr<drivers::pwm_servo::PWMServoData> m_pwmServoData_helm;

			knot_t m_currentGPSSpeed = 0.0_kts;
			knot_t m_targetGPSSpeed = 0.0_kts;

			bool m_moveForward = true;

			degree_t m_currentHullBearing = 0.0_deg;
			degree_t m_currentGPSBearing = 0.0_deg;
			degree_t m_targetGPSBearing = 0.0_deg;

			meters_per_second_squared_t m_linearAcceleration;
			degrees_per_second_t m_angularVelocity;

			percent_t m_helmPercent = 0.0_pct;

			percent_t m_throttle1 = 0.0_pct;
			percent_t m_throttle2 = 0.0_pct;

		};

	}
} // mpasv
