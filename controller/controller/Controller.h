/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Controller.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:56 PM
 */

#pragma once

#include "tools/ThreadedTask.h"

#include <memory>
#include <stack>


namespace mpasv
{
	namespace controller
	{
		class ControllerGoal;

		class Controller
				: public tools::ThreadedTask
		{
		protected:

			Controller(const std::string & name);
			virtual ~Controller() = default;

			void popGoal();
			void pushGoal(const std::shared_ptr<ControllerGoal>& goal);
			std::shared_ptr<ControllerGoal> currentGoal();

			virtual bool setup() noexcept override;
			virtual void initialize() noexcept override;
			virtual void run(const second_t tickDuration) noexcept override;
			virtual void finalize() noexcept override;

		protected:
		private:
		private:

			std::stack<std::shared_ptr<ControllerGoal>> m_goals;

		};

	} // controller
} // mpasv
