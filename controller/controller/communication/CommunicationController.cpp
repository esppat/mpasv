/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CommunicationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/communication/CommunicationController.h"

#include "tools/Exception.h"
#include "tools/Logger.h"

#include "tools/intercomm/IntercommSender.h"
#include "controller/situation/PhysicalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/route/RouteController.h"
#include "controller/route/RouteControllerGoal.h"
#include "controller/route/RouteControllerGoalFollowRoute.h"
#include "controller/situation/GlobalSituationController.h"


namespace mpasv::controller::communication
		{

			CommunicationController::CommunicationController()
				: Controller("CommunicationController")
			{
			}

			CommunicationController::~CommunicationController()
			= default;

			bool CommunicationController::setup() noexcept
			{
				bool result = false;

				try
				{
					m_intercommSender = std::make_shared<tools::intercomm::IntercommSender>("tcp://*:5555");

					// TODO

					result = Controller::setup();
				}
				catch (std::exception & e)
				{
					LOG_DEBUG("Caught exception: ", e.what());
				}

				return result;
			}

			void CommunicationController::initialize() noexcept
			{
				Controller::initialize();

				// TODO

				// TODO frequency and activated should be read from, at least, parameters
				// but also should be tunable from a client
				m_dataBaseFreq.init(1_Hz, true);
				m_dataDebugFreq.init(2_Hz, true);
				m_dataExtendedFreq.init(2_Hz, true);
				m_dataIMUFreq.init(10_Hz, true);
			}

			void CommunicationController::run(const second_t /*tickDuration*/) noexcept
			{
				tools::intercomm::IntercommMessage message;

				if (m_dataBaseFreq.shouldPulse())
				{
					DataBase data;
					populate(data);
					message.setData(data);
					m_intercommSender->sendMessage(message);
				}

				if (m_dataDebugFreq.shouldPulse())
				{
					DataDebug data;
					populate(data);
					message.setData(data);
					m_intercommSender->sendMessage(message);
				}

				if (m_dataExtendedFreq.shouldPulse())
				{
					DataExtended data;
					populate(data);
					message.setData(data);
					m_intercommSender->sendMessage(message);
				}

				if (m_dataIMUFreq.shouldPulse())
				{
					DataIMU data;
					populate(data);
					message.setData(data);
					m_intercommSender->sendMessage(message);
				}
			}

			void CommunicationController::finalize() noexcept
			{
				m_intercommSender.reset();

				// TODO

				Controller::finalize();
			}

			void CommunicationController::populate(DataBase & data) const
			{
				if (m_routeController != nullptr)
				{
					std::shared_ptr<controller::situation::GlobalSituationController> gsc = m_routeController->globalSituationController();
					if (gsc != nullptr)
					{
						std::shared_ptr<controller::situation::PhysicalSituationController> psc = gsc->physicalSituationController();
						if (psc != nullptr)
						{
							if (psc->hasGPSCourse())
								data.GPSCourse_deg = tools::physics::floatFrom<degree_t>(psc->getGPSCourse());

							if (psc->hasGPSSpeed())
								data.GPSSpeed_kts = tools::physics::floatFrom<knot_t>(psc->getGPSSpeed());

							if (psc->hasGeoPos() && psc->getGeoPos().isValid())
							{
								auto pos = psc->getGeoPos();
								data.lat_deg = tools::physics::floatFrom<degree_t>(pos.lat());
								data.lon_deg = tools::physics::floatFrom<degree_t>(pos.lon());
							}
						}
					}
				}
			}

			void CommunicationController::populate(DataDebug & data) const
			{
				if (m_routeController != nullptr)
				{
					std::shared_ptr<controller::situation::GlobalSituationController> gsc = m_routeController->globalSituationController();
					if (gsc != nullptr)
					{
						std::shared_ptr<controller::situation::PhysicalSituationController> psc = gsc->physicalSituationController();
						if (psc != nullptr)
						{
							if (psc->hasRelativeWaterSpeed())
								data.relativeWaterSpeed_kts = tools::physics::floatFrom<knot_t>(psc->getRelativeWaterSpeed());

							if (psc->hasRelativeWindSpeed())
								data.relativeWindSpeed_kts = tools::physics::floatFrom<knot_t>(psc->getRelativeWindSpeed());

							if (psc->hasRelativeWindBearing())
								data.relativeWinddegree_t_deg = tools::physics::floatFrom<degree_t>(psc->getRelativeWindBearing());

							if (psc->hasRealWindSpeedCalculated())
								data.realWindSpeedCalculated_kts = tools::physics::floatFrom<knot_t>(psc->getRealWindSpeedCalculated());

							if (psc->hasRealWinddegree_tCalculated())
								data.realWinddegree_tCalculated_deg = tools::physics::floatFrom<degree_t>(psc->getRealWinddegree_tCalculated());

							if (psc->hasCompassBearing())
								data.compassdegree_t_deg = tools::physics::floatFrom<degree_t>(psc->getCompassBearing());
						}
					}

					std::shared_ptr<controller::route::RouteControllerGoal> currentGoal = m_routeController->currentGoal();
					std::shared_ptr<controller::route::RouteControllerGoalFollowRoute> currentGoalFollowRoute = std::dynamic_pointer_cast<controller::route::RouteControllerGoalFollowRoute>(currentGoal);
					if (currentGoalFollowRoute != nullptr)
					{
						data.targetCourse_deg = tools::physics::floatFrom<degree_t>(currentGoalFollowRoute->targetCourse());
						data.targetLat_deg = tools::physics::floatFrom<tools::geo::latitude_t>(currentGoalFollowRoute->targetPos().lat());
						data.targetLon_deg = tools::physics::floatFrom<tools::geo::latitude_t>(currentGoalFollowRoute->targetPos().lon());
						data.crossTrackError_m = tools::physics::floatFrom<meter_t>(currentGoalFollowRoute->crossTrackError());
					}
				}
			}

			void CommunicationController::populate(DataExtended & data) const
			{
				if (m_routeController != nullptr)
				{
					if (m_routeController->positionController() != nullptr)
					{
						data.helmPercent_pct = 100.0F * tools::physics::floatFrom<percent_t>(m_routeController->positionController()->helmPercent());
						data.throttles_pct[0] = 100.0F * tools::physics::floatFrom<percent_t>(m_routeController->positionController()->throttle1());
						data.throttles_pct[1] = 100.0F * tools::physics::floatFrom<percent_t>(m_routeController->positionController()->throttle2());
					}
				}
			}

			void CommunicationController::populate(DataIMU & data) const
			{
				// TODO
				data.accel_mps_sq[0] = INV_VAL;
				data.accel_mps_sq[1] = INV_VAL;
				data.accel_mps_sq[2] = INV_VAL;
				data.gyro_dps[0] = INV_VAL;
				data.gyro_dps[1] = INV_VAL;
				data.gyro_dps[2] = INV_VAL;
			}

		} // mpasv
