/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CommunicationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/intercomm/IntercommData.h"

#include "tools/physics/Pulse.h"

#include "tools/Exception.h"


namespace mpasv
{
	namespace controller
	{
		namespace situation
		{
			class PhysicalSituationController;
		}

		namespace position
		{
			class PositionController;
		}

		namespace route
		{
			class RouteController;
		}
	}

	namespace drivers
	{
		class DriverManager;
	}

	namespace tools
	{
		namespace intercomm
		{
			class IntercommSender;
		}
	}

	namespace controller
	{
		namespace communication
		{

			class CommunicationController
			: public controller::Controller
			{
			public:

				CommunicationController();
				virtual ~CommunicationController();

				void setRouteController(std::shared_ptr<controller::route::RouteController> routeController)
				{
					CHECK_NOT_NULLPTR(routeController);
					m_routeController = routeController;
				}

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

				void populate(DataBase & data) const;
				void populate(DataDebug & data) const;
				void populate(DataExtended & data) const;
				void populate(DataIMU & data) const;

			private:

				std::shared_ptr<tools::intercomm::IntercommSender> m_intercommSender;

				tools::physics::Pulse m_dataBaseFreq;
				tools::physics::Pulse m_dataDebugFreq;
				tools::physics::Pulse m_dataExtendedFreq;
				tools::physics::Pulse m_dataIMUFreq;

				std::shared_ptr<controller::route::RouteController> m_routeController;

			};

		} // communication
	} // controller
} // mpasv
