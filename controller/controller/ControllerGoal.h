/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ControllerGoal.h
 * Author: esppat
 *
 * Created on January 16, 2020, 9:02 PM
 */

#pragma once

#include <string>


namespace mpasv
{
	namespace controller
	{

		class ControllerGoal
		{
		public:

			ControllerGoal();
			virtual ~ControllerGoal();

			virtual std::string toString() const = 0;

			bool isComplete() const
			{
				return m_complete;
			}

			void setComplete()
			{
				m_complete = true;
			}

		protected:
		protected:
		private:
		private:

			bool m_complete = false;

		};

	} // controller
} // mpasv
