/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Controller.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:56 PM
 */

#include "controller/Controller.h"
#include "controller/ControllerGoal.h"

#include "tools/Exception.h"
#include "tools/Logger.h"


namespace mpasv::controller
{

	Controller::Controller(const std::string & name)
		: ThreadedTask(name)
	{
	}

	void Controller::popGoal()
	{
		CHECK_GT(m_goals.size(), 1);
		m_goals.pop();
		LOG_DEBUG("Previous goal restored: ", currentGoal()->toString());
	}

	void Controller::pushGoal(const std::shared_ptr<ControllerGoal>& goal)
	{
		m_goals.push(goal);
		LOG_DEBUG("New goal defined: ", goal->toString());
	}

	std::shared_ptr<ControllerGoal> Controller::currentGoal()
	{
		CHECK_FALSE(m_goals.empty());
		return m_goals.top();
	}

	bool Controller::setup() noexcept
	{
		bool result = false;

		try
		{
			// TODO

			result = ThreadedTask::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void Controller::initialize() noexcept
	{
		ThreadedTask::initialize();

		// TODO
	}

	void Controller::run(const second_t /*tickDuration*/) noexcept
	{
	}

	void Controller::finalize() noexcept
	{
		// TODO

		ThreadedTask::finalize();
	}

} // mpasv
