#include "SimuDaggerBoardPlane.h"

#include "tools/Tools.h"
#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/gps/GeoPos.h"


namespace mpasv::controller::simulation
{
	std::map<degree_t, std::pair<percent_t, percent_t>> SimuDaggerBoardPlane::m_attackAngleToPercent;

	SimuDaggerBoardPlane::SimuDaggerBoardPlane(const std::string & _name)
		: SimuElement(_name)
		, m_x(param<meter_t, double>(std::string("PhysicalSimulator.Dagger board planes.") + name() + ".X (m)"))
		, m_z(param<meter_t, double>(std::string("PhysicalSimulator.Dagger board planes.") + name() + ".Z (m)"))
		, m_distanceToCOG(doubleFrom<meter_t>(m_x), 0.0, doubleFrom<meter_t>(m_z))
		, m_dragPerKnot(param<newton_t, double>(std::string("PhysicalSimulator.Dagger board planes.") + name() + ".Drag per knot (N)"))
		, m_angleToElement(atan2(m_z, m_x))
		, m_distanceToElement(sqrt(m_x * m_x + m_z * m_z))
	{
	}

	void SimuDaggerBoardPlane::getThrust(const Vector3 & bodyLinearVelocity,
										 const Vector3 & bodyAngularVelocity,
										 Vector3 & applicationPoint,
										 Vector3 & force)
	{
		applicationPoint.x = decimal(m_x);
		applicationPoint.y = 0.0;
		applicationPoint.z = decimal(m_z);

		Vector3 realLinearVelocity;

		calculateAttackAngleAndVelocity(bodyLinearVelocity, bodyAngularVelocity, realLinearVelocity);

		decimal velocity = realLinearVelocity.length();
		degree_t attackAngle = radian_t(std::atan2(realLinearVelocity.z, realLinearVelocity.x));
		attackAngle -= m_orientation;
		attackAngle = normalize180(attackAngle);

		percent_t xThrustPercent, zThrustPercent;
		calculateThrustsPercent(attackAngle, xThrustPercent, zThrustPercent);

		force.x = decimal(m_dragPerKnot * velocity * xThrustPercent);
		force.z = decimal(m_dragPerKnot * velocity * zThrustPercent);

//		LOG_DEBUG(name(),
//				  " linearVelocity=", bodyLinearVelocity.to_string(),
//				  " angularVelocity=", bodyAngularVelocity.to_string(),
//				  " m_orientation=", degree_t(m_orientation),
//				  " attackAngle=", attackAngle,
//				  " force=", force.to_string());
	}

	void SimuDaggerBoardPlane::calculateAttackAngleAndVelocity(const Vector3 & bodyLinearVelocity,
															   const Vector3 & bodyAngularVelocity,
															   Vector3 & realLinearVelocity)
	{
		realLinearVelocity = bodyLinearVelocity + bodyAngularVelocity.cross(m_distanceToCOG);
	}

	void SimuDaggerBoardPlane::calculateThrustsPercent(const degree_t & attackAngle, percent_t & xThrustPercent, percent_t & zThrustPercent)
	{
		if (m_attackAngleToPercent.size() == 0)
			fillAttackAngleToPercent();

		CHECK_GE(attackAngle, -180.0_deg);
		CHECK_LE(attackAngle, 180.0_deg);

		degree_t previousDegree, nextDegree;
		findPreviousAndNextDegrees(attackAngle, previousDegree, nextDegree);

		if (previousDegree == nextDegree)
		{
			CHECK_EQUAL(previousDegree, attackAngle);

			auto & p = m_attackAngleToPercent[previousDegree];
			xThrustPercent = p.first;
			zThrustPercent = p.second;
		}
		else
		{
			CHECK_LT(previousDegree, attackAngle);
			CHECK_LT(attackAngle, nextDegree);

			percent_t percent = (attackAngle - previousDegree) / (nextDegree - previousDegree);

			auto & previous = m_attackAngleToPercent[previousDegree];
			auto & next = m_attackAngleToPercent[nextDegree];

			percent_t diff = next.first - previous.first;
			xThrustPercent = previous.first + diff * percent;

			diff = next.second - previous.second;
			zThrustPercent = previous.second + diff * percent;
		}

		// must change coordinates system

		double cosT = cos(m_orientation);
		double sinT = sin(m_orientation);

		double x = xThrustPercent;
		double y = zThrustPercent;

		xThrustPercent = x * cosT + y * sinT;
		zThrustPercent = x * sinT + y * cosT;
	}

	void SimuDaggerBoardPlane::fillAttackAngleToPercent()
	{
		m_attackAngleToPercent[-180.0_deg] = {20.0_pct, 0.0_pct};
		m_attackAngleToPercent[-170.0_deg] = {25.0_pct, -10.0_pct};
		m_attackAngleToPercent[-135.0_deg] = {25.0_pct, -50.0_pct};
		m_attackAngleToPercent[-90.0_deg] = {0.0_pct, -100.0_pct};
		m_attackAngleToPercent[-45.0_deg] = {-20.0_pct, -60.0_pct};
		m_attackAngleToPercent[-10.0_deg] = {-10.0_pct, -50.0_pct};

		m_attackAngleToPercent[0.0_deg] = {-10.0_pct, 0.0_pct};

		m_attackAngleToPercent[10.0_deg] = {-10.0_pct, 50.0_pct};
		m_attackAngleToPercent[45.0_deg] = {-20.0_pct, 60.0_pct};
		m_attackAngleToPercent[90.0_deg] = {0.0_pct, 100.0_pct};
		m_attackAngleToPercent[135.0_deg] = {25.0_pct, 50.0_pct};
		m_attackAngleToPercent[170.0_deg] = {25.0_pct, 10.0_pct};
		m_attackAngleToPercent[180.0_deg] = {20.0_pct, 0.0_pct};
	}

	void SimuDaggerBoardPlane::findPreviousAndNextDegrees(const degree_t & attackAngle, degree_t & previousDegree, degree_t & nextDegree)
	{
		auto first = m_attackAngleToPercent.begin();

		for (auto it = m_attackAngleToPercent.begin(); it != m_attackAngleToPercent.end(); it++)
		{
			if (it->first == attackAngle)
			{
				previousDegree = nextDegree = attackAngle;
				return;
			}

			if (it->first > attackAngle)
			{
				previousDegree = first->first;
				nextDegree = it->first;
				return;
			}

			first = it;
		}

		THROW_ALWAYS("Internal error");
	}

	void SimuDaggerBoardPlane::rotate(Vector2 & v, const decimal angleRadian)
	{
		rotate(v.x, v.y, angleRadian);
	}

	void SimuDaggerBoardPlane::rotate(decimal & x, decimal & y, const decimal angleRadian)
	{
		Matrix2x2 rotationMatrix(decimal(+std::cos(angleRadian)), decimal(-std::sin(angleRadian)),
								 decimal(+std::sin(angleRadian)), decimal(+std::cos(angleRadian)));

		Vector2 v(x, y);
		v = rotationMatrix * v;
		x = v.x;
		y = v.y;
	}

}
