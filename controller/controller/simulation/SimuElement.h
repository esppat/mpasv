#pragma once

#include "tools/Tools.h"
#include "tools/Nameable.h"
#include "tools/physics/Units.h"

#include "reactphysics3d.h"
using namespace reactphysics3d;


namespace mpasv::controller::simulation
{
	class SimuElement
			: public Nameable
			, public mpasv::tools::enable_shared_from_this<SimuElement>
	{
	public:

		SimuElement(const std::string name);

		virtual void getThrust(const Vector3 & bodyLinearVelocity,
							   const Vector3 & bodyAngularVelocity,
							   Vector3 & applicationPoint,
							   Vector3 & force) = 0;

	public:
	protected:
	protected:
	private:
	private:
	};
}
