/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/simulation/SimulationController.h"

#include "drivers/actuators/brushless_controller/BrushlessControllerData.h"
#include "drivers/actuators/pwm_servo/PWMServo.h"
#include "drivers/actuators/pwm_servo/PWMServoData.h"
#include "drivers/sensors/GPS/GPSSensorData.h"
#include "drivers/sensors/AIS/AISSensorData.h"
#include "drivers/sensors/IMU/IMUSensorData.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensorData.h"
#include "drivers/sensors/wind/WindDirectionSensorData.h"
#include "drivers/sensors/wind/WindSpeedSensorData.h"
#include "drivers/sensors/ampere/AmperageSensorData.h"
#include "drivers/sensors/voltage/VoltageSensorData.h"

#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "drivers/DriverManager.h"


namespace mpasv::controller::simulation
{

	SimulationController::SimulationController(const std::string & name)
		: Controller(name)
		, m_oneHullWeight(param<gram_t, double>("PhysicalDescription.Hull.Weight (kg)"))
		, m_oneHullDragPerKnot(param<newton_t, double>("PhysicalSimulator.Hull drag per knot (N)"))
		, m_windEffectRatio(param<percent_t, double>("PhysicalSimulator.Wind effect ratio (pct)"))
		, m_currentEffectRatio(param<percent_t, double>("PhysicalSimulator.Current effect ratio (pct)"))
	{
	}

	void SimulationController::setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	void SimulationController::update(const std::shared_ptr<const drivers::brushless_controller::BrushlessControllerData>& brushlessControllerData, const std::string & name)
	{
		*m_brushlessControllerData[name] = *brushlessControllerData;
	}

	void SimulationController::update(const std::shared_ptr<const drivers::pwm_servo::PWMServoData>& pwmServoData, const std::string & name)
	{
		*m_pwmServoData[name] = *pwmServoData;
	}

	void SimulationController::update(const std::shared_ptr<drivers::GPS::GPSSensorData>& gpsData, const std::string & name)
	{
		*gpsData = *m_gpsData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::AIS::AISSensorData>& aisData, const std::string & name)
	{
		*aisData = *m_aisData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::IMU::IMUSensorData>& imuData, const std::string & name)
	{
		*imuData = *m_imuData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::wind::WindSpeedSensorData>& windSpeedSensorData, const std::string & name)
	{
		*windSpeedSensorData = *m_windSpeedSensorData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::ampere::AmperageSensorData>& AmperageSensorData, const std::string & name)
	{
		*AmperageSensorData = *m_AmperageData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::voltage::VoltageSensorData>& voltageSensorData, const std::string & name)
	{
		*voltageSensorData = *m_voltageData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::wind::WindDirectionSensorData>& windDirectionSensorData, const std::string & name)
	{
		*windDirectionSensorData = *m_windDirectionSensorData[name];
	}

	void SimulationController::update(const std::shared_ptr<drivers::water::PropulsionWaterSpeedSensorData>& propulsionWaterSpeedSensorData, const std::string & name)
	{
		*propulsionWaterSpeedSensorData = *m_propulsionWaterSpeedSensorData[name];
	}

	void SimulationController::setStartPoint(const tools::geo::GeoPos& geoPos, const degree_t hullHeading)
	{
		m_gpsData["GPS 1"]->m_geoPos = geoPos;
		m_gpsData["GPS 1"]->m_hasFix = m_gpsData["GPS 1"]->m_geoPos.isValid();

		CHECK_VALID_BEARING(hullHeading);
		m_imuData["IMU 1"]->m_hullHeading = hullHeading;

		m_gpsData["GPS 1"]->m_gpsSpeed = 0.0_kts;
		m_gpsData["GPS 1"]->m_gpsCourse = hullHeading;
	}

	bool SimulationController::setup() noexcept
	{
		bool result = false;
		theClock.pause();

		try
		{
			m_brushlessControllerData["Engine 1"] = std::make_shared<drivers::brushless_controller::BrushlessControllerData>();
			m_brushlessControllerData["Engine 2"] = std::make_shared<drivers::brushless_controller::BrushlessControllerData>();
			m_pwmServoData["Helm 1"] = std::make_shared<drivers::pwm_servo::PWMServoData>(m_driverManager->getActuator<drivers::pwm_servo::PWMServo>("Helm 1"));
			m_gpsData["GPS 1"] = std::make_shared<drivers::GPS::GPSSensorData>();
			m_imuData["IMU 1"] = std::make_shared<drivers::IMU::IMUSensorData>();
			m_propulsionWaterSpeedSensorData["Lock 1"] = std::make_shared<drivers::water::PropulsionWaterSpeedSensorData>();
			m_windDirectionSensorData["Wind 1"] = std::make_shared<drivers::wind::WindDirectionSensorData>();
			m_windSpeedSensorData["Wind 1"] = std::make_shared<drivers::wind::WindSpeedSensorData>();
			m_AmperageData["Engine 1"] = std::make_shared<drivers::ampere::AmperageSensorData>();
			m_voltageData["Engine 1"] = std::make_shared<drivers::voltage::VoltageSensorData>();
			if (param<bool>("PhysicalDescription.Has two hulls (bool)"))
			{
				m_voltageData["Engine 2"] = std::make_shared<drivers::voltage::VoltageSensorData>();
				m_AmperageData["Engine 2"] = std::make_shared<drivers::ampere::AmperageSensorData>();
			}

			allocateDataCalc();

			m_leftPropeller.reset(new SimuPropeller("Left propeller"));
			m_simuElements.push_back(m_leftPropeller);

			m_rightPropeller.reset(new SimuPropeller("Right propeller"));
			m_simuElements.push_back(m_rightPropeller);

//			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane("Hull front left")));
//			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane("Hull rear left")));
//			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane("Hull front right")));
//			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane("Hull rear right")));

			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane("Keel left")));
			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane("Keel right")));

//			m_leftHelm.reset(new SimuDaggerBoardPlane("Helm left"));
//			m_simuElements.push_back(m_leftHelm);

//			m_rightHelm.reset(new SimuDaggerBoardPlane("Helm right"));
//			m_simuElements.push_back(m_rightHelm);

			create3dBody();

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		theClock.resume();
		return result;
	}

	void SimulationController::initialize() noexcept
	{
		Controller::initialize();

		// TODO

		m_gpsData["GPS 1"]->m_hasFix = false;
	}

	void SimulationController::run(const second_t tickDuration) noexcept
	{
		if (!m_gpsData["GPS 1"]->m_hasFix || !m_gpsData["GPS 1"]->m_geoPos.isValid())
		{
			return;
		}

		theClock.pause();

		copyDataToDataCalc();
		doCalculation(tickDuration);
		copyDataCalcToData();

		theClock.resume();
	}

	void SimulationController::finalize() noexcept
	{
		// TODO

		Controller::finalize();
	}

	template <class T>
	void allocateData(const std::map<std::string, std::shared_ptr<T>> & src,
					  std::map<std::string, std::shared_ptr<T>> & dst)
	{
		for (auto & it : src)
		{
			dst[it.first] = std::make_shared<T>();
		}
	}

	template <>
	void allocateData(const std::map<std::string, std::shared_ptr<drivers::pwm_servo::PWMServoData>> & src,
					  std::map<std::string, std::shared_ptr<drivers::pwm_servo::PWMServoData>> & dst)
	{
		for (auto & it : src)
		{
			dst[it.first] = std::make_shared<drivers::pwm_servo::PWMServoData>(it.second->m_servo);
		}
	}

	template <class T>
	void copyData(const std::map<std::string, std::shared_ptr<T>> & src,
				  std::map<std::string, std::shared_ptr<T>> & dst)
	{
		for (auto & it : src)
		{
			*(dst[it.first]) = *it.second;
		}
	}

	void SimulationController::allocateDataCalc()
	{
		allocateData(m_brushlessControllerData, m_brushlessControllerDataCalc);
		allocateData(m_pwmServoData, m_pwmServoDataCalc);
		allocateData(m_gpsData, m_gpsDataCalc);
		allocateData(m_aisData, m_aisDataCalc);
		allocateData(m_imuData, m_imuDataCalc);
		allocateData(m_propulsionWaterSpeedSensorData, m_propulsionWaterSpeedSensorDataCalc);
		allocateData(m_windDirectionSensorData, m_windDirectionSensorDataCalc);
		allocateData(m_windSpeedSensorData, m_windSpeedSensorDataCalc);
		allocateData(m_AmperageData, m_AmperageDataCalc);
		allocateData(m_voltageData, m_voltageDataCalc);
	}

	void SimulationController::copyDataToDataCalc()
	{
		copyData(m_brushlessControllerData, m_brushlessControllerDataCalc);
		copyData(m_pwmServoData, m_pwmServoDataCalc);
		copyData(m_gpsData, m_gpsDataCalc);
		copyData(m_aisData, m_aisDataCalc);
		copyData(m_imuData, m_imuDataCalc);
		copyData(m_propulsionWaterSpeedSensorData, m_propulsionWaterSpeedSensorDataCalc);
		copyData(m_windDirectionSensorData, m_windDirectionSensorDataCalc);
		copyData(m_windSpeedSensorData, m_windSpeedSensorDataCalc);
		copyData(m_AmperageData, m_AmperageDataCalc);
		copyData(m_voltageData, m_voltageDataCalc);
	}

	void SimulationController::copyDataCalcToData()
	{
		copyData(m_brushlessControllerDataCalc, m_brushlessControllerData);
		copyData(m_pwmServoDataCalc, m_pwmServoData);
		copyData(m_gpsDataCalc, m_gpsData);
		copyData(m_aisDataCalc, m_aisData);
		copyData(m_imuDataCalc, m_imuData);
		copyData(m_propulsionWaterSpeedSensorDataCalc, m_propulsionWaterSpeedSensorData);
		copyData(m_windDirectionSensorDataCalc, m_windDirectionSensorData);
		copyData(m_windSpeedSensorDataCalc, m_windSpeedSensorData);
		copyData(m_AmperageDataCalc, m_AmperageData);
		copyData(m_voltageDataCalc, m_voltageData);
	}

	void SimulationController::doCalculation(const second_t tickDuration)
	{
		update3dBody(tickDuration);

		updateSimuElements();

		meter_t distanceInWaterDuringStep = meters_per_second_t(m_bodyLinearVelocity.length()) * tickDuration;
		mpasv::tools::geo::GeoPos newPos = m_gpsDataCalc["GPS 1"]->m_geoPos.forwardTo(m_imuDataCalc["IMU 1"]->m_hullHeading, distanceInWaterDuringStep);
		degree_t hullHeading = normalize360(m_imuDataCalc["IMU 1"]->m_hullHeading + radians_per_second_t(m_bodyAngularVelocity.y) * tickDuration);

		m_propulsionWaterSpeedSensorDataCalc["Lock 1"]->m_propulsionWaterSpeed = meters_per_second_t(m_bodyLinearVelocity.x); // TODO add current effect

		calculateWindParameters();
		calculateCurrentParameters();

		// wind derive
		meter_t windDrag = m_windSpeed * m_windEffectRatio * tickDuration;
		newPos = newPos.forwardTo(m_windComesFrom, -windDrag);

		// current derive
		meter_t currentDrag = m_currentSpeed * m_currentEffectRatio * tickDuration;
		newPos = newPos.forwardTo(m_currentComesFrom, currentDrag);

		m_gpsDataCalc["GPS 1"]->m_geoPos = newPos;
		m_imuDataCalc["IMU 1"]->m_hullHeading = hullHeading;
		m_imuDataCalc["IMU 1"]->m_accelX = meters_per_second_squared_t(m_bodyLinearAcceleration.x);
		m_imuDataCalc["IMU 1"]->m_gyroX = radians_per_second_t(m_bodyAngularVelocity.y);

		m_gpsDataCalc["GPS 1"]->m_gpsCourse = hullHeading; // TODO add current and wind deviation
		m_gpsDataCalc["GPS 1"]->m_gpsSpeed = m_gpsData["GPS 1"]->m_geoPos.distanceTo(m_gpsDataCalc["GPS 1"]->m_geoPos) / tickDuration;
	}

	void SimulationController::calculateWindParameters()
	{
		// TODO
		m_windSpeed = param<knot_t, double>("PhysicalSimulator.Wind speed (kts)");
		m_windComesFrom = param<degree_t, double>("PhysicalSimulator.Wind degree_t (deg)");
	}

	void SimulationController::calculateCurrentParameters()
	{
		// TODO
		m_currentSpeed = param<knot_t, double>("PhysicalSimulator.Current speed (kts)");
		m_currentComesFrom = param<degree_t, double>("PhysicalSimulator.Current degree_t (deg)");
	}

	void SimulationController::updateSimuElements()
	{
		if (m_leftPropeller != nullptr)
			m_leftPropeller->setThrottle(m_brushlessControllerDataCalc.at("Engine 1")->m_throttle.get());

		if (m_rightPropeller != nullptr)
			m_rightPropeller->setThrottle(m_brushlessControllerDataCalc.at("Engine 2")->m_throttle.get());

		if (m_leftHelm != nullptr)
			m_leftHelm->setOrientation(m_pwmServoData.at("Helm 1")->roundAngle());

		if (m_rightHelm != nullptr)
			m_rightHelm->setOrientation(m_pwmServoData.at("Helm 1")->roundAngle());
	}

}
