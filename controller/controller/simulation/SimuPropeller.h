#pragma once

#include "SimuElement.h"


namespace mpasv::controller::simulation
{

	class SimuPropeller
			: public SimuElement
	{
	public:

		SimuPropeller(const std::string & name);

		void setThrottle(const percent_t percent)
		{
			m_throttlePercent = percent;
			mpasv::tools::setInLimit(m_throttlePercent, -100.0_pct, +100.0_pct);
		}

		virtual void getThrust(const Vector3 & bodyLinearVelocity,
							   const Vector3 & bodyAngularVelocity,
							   Vector3 & applicationPoint,
							   Vector3 & force);

	private:

		percent_t m_throttlePercent = 0.0_pct;

		meter_t m_x;
		meter_t m_z;
		newton_t m_thrustPerThrottlePercent;

	};

}
