/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController_ReactPhysics3d.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/simulation/SimulationController_ReactPhysics3d.h"
#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"


namespace mpasv::controller::simulation
{

	SimulationController_ReactPhysics3d::SimulationController_ReactPhysics3d()
		: SimulationController("SimulationController_ReactPhysics3d")
	{
	}

	void SimulationController_ReactPhysics3d::create3dBody()
	{
		m_gravity = new rp3d::Vector3(0.0, 0.0, 0.0);
		m_world = new rp3d::DynamicsWorld(*m_gravity);

		// Change the number of iterations of the velocity solver
		m_world->setNbIterationsVelocitySolver(15);

		// Change the number of iterations of the position solver
		m_world->setNbIterationsPositionSolver(8);

		// Initial position and orientation of the rigid body
		rp3d::Vector3 initPosition(0.0, 0.0, 0.0);
		rp3d::Quaternion initOrientation = rp3d::Quaternion::identity();
		rp3d::Transform transform(initPosition, initOrientation);

		// Create a rigid body in the world
		m_body = m_world->createRigidBody(transform);

		rp3d::Vector3 hullHalfExtents(param<rp3d::decimal>("PhysicalDescription.Hull.Lenght (m)") / 2.0,
									  param<rp3d::decimal>("PhysicalDescription.Hull.Height (m)") / 2.0,
									  param<rp3d::decimal>("PhysicalDescription.Hull.Width (m)") / 2.0);
		m_hullCollisionShape = new rp3d::BoxShape(hullHalfExtents);

		m_hullMass = param<rp3d::decimal>("PhysicalDescription.Hull.Weight (kg)");

		initPosition = rp3d::Vector3(0.0, 0.0, -param<rp3d::decimal>("PhysicalDescription.Distance hull COG (m)"));
		transform = rp3d::Transform(initPosition, initOrientation);
		m_leftHull = m_body->addCollisionShape(m_hullCollisionShape, transform, m_hullMass);

		initPosition = rp3d::Vector3(0.0, 0.0, +param<rp3d::decimal>("PhysicalDescription.Distance hull COG (m)"));
		transform = rp3d::Transform(initPosition, initOrientation);
		m_rightHull = m_body->addCollisionShape(m_hullCollisionShape, transform, m_hullMass);

		m_body->recomputeMassInformation();
	}

	void SimulationController_ReactPhysics3d::update3dBody(const second_t tickDuration)
	{
		m_world->update(rp3d::decimal(delayBetweenRuns()));

		m_previousBodyLinearVelocity = m_bodyLinearVelocity;
		m_previousBodyAngularVelocity = m_bodyAngularVelocity;
		m_previousBodyTransform = m_bodyTransform;

		m_bodyLinearVelocity = m_body->getLinearVelocity();
		m_bodyAngularVelocity = m_body->getAngularVelocity();
		m_bodyTransform = m_body->getTransform();

		m_bodyLinearAcceleration = (m_bodyLinearVelocity - m_previousBodyLinearVelocity) / double(tickDuration);
		m_bodyAngularAcceleration = (m_bodyAngularVelocity - m_previousBodyAngularVelocity) / double(tickDuration);

		Vector3 bodyLocalLinearVelocity = m_bodyTransform.getOrientation().getInverse() * m_bodyLinearVelocity;

//		LOG_DEBUG("-------------------------------------------------------------------------------------------------------------------------------");
//		LOG_THAT(m_bodyLinearVelocity.to_string());
//		LOG_THAT(bodyLocalLinearVelocity.to_string());
//		LOG_THAT(m_bodyLinearAcceleration.to_string());
//		LOG_THAT(m_bodyAngularVelocity.to_string());
//		LOG_THAT(m_bodyAngularAcceleration.to_string());
//		LOG_THAT(m_bodyTransform.to_string());

		for (auto element : m_simuElements)
		{
			Vector3 applicationPoint;
			Vector3 force;

			element->getThrust(bodyLocalLinearVelocity, m_bodyAngularVelocity, applicationPoint, force);

//			LOG_DEBUG(element->name(), ": Applying force=", force.to_string(), " at point=", applicationPoint.to_string());

			applicationPoint = m_bodyTransform * applicationPoint;
			force = m_bodyTransform.getOrientation() * force;
			m_body->applyForce(force, applicationPoint);

//			LOG_DEBUG("externalForce=", m_body->getExternalForce().to_string(), " externalTorque=", m_body->getExternalTorque().to_string());
		}
	}

}
