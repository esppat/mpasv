/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController_ReactPhysics3d.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/simulation/SimulationController.h"

#include "reactphysics3d.h"
using namespace reactphysics3d;


namespace mpasv::controller::simulation
{

	class SimulationController_ReactPhysics3d
			: public SimulationController
	{
	public:

		SimulationController_ReactPhysics3d();
		virtual ~SimulationController_ReactPhysics3d() = default;

	protected:
	protected:
	private:

		void create3dBody() override;
		void update3dBody(const second_t tickDuration) override;

	private:

		Vector3 * m_gravity = nullptr;
		DynamicsWorld * m_world = nullptr;
		RigidBody * m_body = nullptr;
		BoxShape * m_hullCollisionShape = nullptr;
		decimal m_hullMass;
		ProxyShape * m_leftHull = nullptr;
		ProxyShape * m_rightHull = nullptr;
	};

}
