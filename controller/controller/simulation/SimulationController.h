/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"
#include "controller/simulation/SimuElement.h"
#include "controller/simulation/SimuPropeller.h"
#include "controller/simulation/SimuDaggerBoardPlane.h"

#include "tools/gps/GeoPos.h"

#include <map>

#include "reactphysics3d.h"
using namespace reactphysics3d;


namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace brushless_controller
		{
			class BrushlessControllerData;
		}

		namespace pwm_servo
		{
			class PWMServoData;
		}

		namespace GPS
		{
			class GPSSensorData;
		}

		namespace AIS
		{
			class AISSensorData;
		}

		namespace IMU
		{
			class IMUSensorData;
		}

		namespace water
		{
			class PropulsionWaterSpeedSensorData;
		}

		namespace wind
		{
			class WindSpeedSensorData;
			class WindDirectionSensorData;
		}

		namespace ampere
		{
			class AmperageSensorData;
		}

		namespace voltage
		{
			class VoltageSensorData;
		}
	}

	namespace controller::simulation
	{

		class SimulationController
				: public controller::Controller
		{
		public:

			SimulationController(const std::string & name);
			virtual ~SimulationController() = default;

			void setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager);

			void update(const std::shared_ptr<const drivers::brushless_controller::BrushlessControllerData>& brushlessControllerData, const std::string & name);
			void update(const std::shared_ptr<const drivers::pwm_servo::PWMServoData>& pwmServoData, const std::string & name);
			void update(const std::shared_ptr<drivers::GPS::GPSSensorData>& gpsData, const std::string & name);
			void update(const std::shared_ptr<drivers::AIS::AISSensorData>&aisDataa, const std::string & name);
			void update(const std::shared_ptr<drivers::IMU::IMUSensorData>& imuData, const std::string & name);
			void update(const std::shared_ptr<drivers::water::PropulsionWaterSpeedSensorData>& propulsionWaterSpeedSensorData, const std::string & name);
			void update(const std::shared_ptr<drivers::wind::WindDirectionSensorData>& windDirectionSensorData, const std::string & name);
			void update(const std::shared_ptr<drivers::wind::WindSpeedSensorData>& windSpeedSensorData, const std::string & name);
			void update(const std::shared_ptr<drivers::ampere::AmperageSensorData>& AmperageSensorData, const std::string & name);
			void update(const std::shared_ptr<drivers::voltage::VoltageSensorData>& voltageSensorData, const std::string & name);

			void setStartPoint(const tools::geo::GeoPos& geoPos, const degree_t hullHeading);

		protected:

			virtual void create3dBody() = 0;
			virtual void update3dBody(const second_t tickDuration) = 0;

		protected:
		private:

			virtual bool setup() noexcept override;
			virtual void initialize() noexcept override;
			virtual void run(const second_t tickDuration) noexcept override;
			virtual void finalize() noexcept override;

			void allocateDataCalc();
			void copyDataToDataCalc();
			void copyDataCalcToData();

			void doCalculation(const second_t tickDuration);
			void calculateWindParameters();
			void calculateCurrentParameters();
			void updateSimuElements();

		protected:

			std::list<std::shared_ptr<SimuElement>> m_simuElements;

			const kilogram_t m_oneHullWeight;
			const newton_t m_oneHullDragPerKnot;
			const percent_t m_windEffectRatio;
			const percent_t m_currentEffectRatio;

			Vector3 m_previousBodyLinearVelocity;
			Vector3 m_previousBodyAngularVelocity;
			Transform m_previousBodyTransform;

			Vector3 m_bodyLinearVelocity;
			Vector3 m_bodyAngularVelocity;
			Transform m_bodyTransform;

			Vector3 m_bodyLinearAcceleration;
			Vector3 m_bodyAngularAcceleration;

			knot_t m_windSpeed = 0.0_kts;
			degree_t m_windComesFrom = 0.0_deg;
			knot_t m_currentSpeed = 0.0_kts;;
			degree_t m_currentComesFrom = 0.0_deg;

		private:

			std::shared_ptr<drivers::DriverManager> m_driverManager;

			std::map<std::string, std::shared_ptr<drivers::brushless_controller::BrushlessControllerData>> m_brushlessControllerData;
			std::map<std::string, std::shared_ptr<drivers::pwm_servo::PWMServoData>> m_pwmServoData;

			std::map<std::string, std::shared_ptr<drivers::GPS::GPSSensorData>> m_gpsData;
			std::map<std::string, std::shared_ptr<drivers::AIS::AISSensorData>> m_aisData;
			std::map<std::string, std::shared_ptr<drivers::IMU::IMUSensorData>> m_imuData;
			std::map<std::string, std::shared_ptr<drivers::water::PropulsionWaterSpeedSensorData>> m_propulsionWaterSpeedSensorData;
			std::map<std::string, std::shared_ptr<drivers::wind::WindDirectionSensorData>> m_windDirectionSensorData;
			std::map<std::string, std::shared_ptr<drivers::wind::WindSpeedSensorData>> m_windSpeedSensorData;
			std::map<std::string, std::shared_ptr<drivers::ampere::AmperageSensorData>> m_AmperageData;
			std::map<std::string, std::shared_ptr<drivers::voltage::VoltageSensorData>> m_voltageData;

			std::map<std::string, std::shared_ptr<drivers::brushless_controller::BrushlessControllerData>> m_brushlessControllerDataCalc;
			std::map<std::string, std::shared_ptr<drivers::pwm_servo::PWMServoData>> m_pwmServoDataCalc;

			std::map<std::string, std::shared_ptr<drivers::GPS::GPSSensorData>> m_gpsDataCalc;
			std::map<std::string, std::shared_ptr<drivers::AIS::AISSensorData>> m_aisDataCalc;
			std::map<std::string, std::shared_ptr<drivers::IMU::IMUSensorData>> m_imuDataCalc;
			std::map<std::string, std::shared_ptr<drivers::water::PropulsionWaterSpeedSensorData>> m_propulsionWaterSpeedSensorDataCalc;
			std::map<std::string, std::shared_ptr<drivers::wind::WindDirectionSensorData>> m_windDirectionSensorDataCalc;
			std::map<std::string, std::shared_ptr<drivers::wind::WindSpeedSensorData>> m_windSpeedSensorDataCalc;
			std::map<std::string, std::shared_ptr<drivers::ampere::AmperageSensorData>> m_AmperageDataCalc;
			std::map<std::string, std::shared_ptr<drivers::voltage::VoltageSensorData>> m_voltageDataCalc;

			std::shared_ptr<SimuPropeller> m_leftPropeller;
			std::shared_ptr<SimuPropeller> m_rightPropeller;
			std::shared_ptr<SimuDaggerBoardPlane> m_leftHelm;
			std::shared_ptr<SimuDaggerBoardPlane> m_rightHelm;
		};

	}
}

using namespace mpasv::controller::simulation;
