#include "SimuPropeller.h"

#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"


namespace mpasv::controller::simulation
{

	SimuPropeller::SimuPropeller(const std::string & name)
		: SimuElement(name)
	{
		m_x = param<meter_t, double>(std::string("PhysicalDescription.Hull.Propeller X (m)"));
		m_z = param<meter_t, double>(std::string("PhysicalDescription.Distance hull COG (m)"));
		if (name == "Left propeller")
			m_z = -m_z;
		m_thrustPerThrottlePercent = param<newton_t, double>("PhysicalSimulator.Propeller.Thrust per throttle percent (N)");
	}

	void SimuPropeller::getThrust(const Vector3 & /*m_bodyLinearVelocity*/,
								  const Vector3 & /*m_bodyAngularVelocity*/,
								  Vector3 & applicationPoint,
								  Vector3 & force)
	{
		applicationPoint.x = decimal(m_x);
		applicationPoint.y = 0.0;
		applicationPoint.z = decimal(m_z);

		force.x = decimal(m_thrustPerThrottlePercent * m_throttlePercent * 100.0);
		force.y = force.z = 0.0;
	}

}
