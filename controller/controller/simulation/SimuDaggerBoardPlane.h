#pragma once

#include "SimuElement.h"
#include <map>


namespace mpasv::controller::simulation
{

	class SimuDaggerBoardPlane
			: public SimuElement
	{
	public:

		SimuDaggerBoardPlane(const std::string & name);

		virtual void getThrust(const Vector3 & bodyLinearVelocity,
							   const Vector3 & bodyAngularVelocity,
							   Vector3 & applicationPoint,
							   Vector3 & force);

		void setOrientation(const degree_t orientation)
		{
			m_orientation = orientation;
		}

	private:

		void calculateAttackAngleAndVelocity(const Vector3 & bodyLinearVelocity,
											 const Vector3 & bodyAngularVelocity,
											 Vector3 & realLinearVelocity);

		void calculateThrustsPercent(const degree_t & attackAngle, percent_t & xThrustPercent, percent_t & zThrustPercent);

		static void fillAttackAngleToPercent();
		static void findPreviousAndNextDegrees(const degree_t & attackAngle, degree_t & previousDegree, degree_t & nextDegree);

		static void rotate(Vector2 & v, const decimal angleRadian);
		static void rotate(decimal & x, decimal & y, const decimal angleRadian);

	private:

		degree_t m_orientation = 0.0_deg;

		const meter_t m_x;
		const meter_t m_z;

		const Vector3 m_distanceToCOG;


		const newton_t m_dragPerKnot;

		const degree_t m_angleToElement;
		const meter_t m_distanceToElement;

		static std::map<degree_t, std::pair<percent_t, percent_t>> m_attackAngleToPercent;
	};

}
