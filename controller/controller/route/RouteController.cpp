/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/route/RouteController.h"
#include "controller/route/RouteControllerGoal.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"

#include "tools/Logger.h"
#include "tools/Exception.h"
#include "tools/routing/Route.h"
#include "RouteControllerGoalFollowRoute.h"
#include "RouteControllerGoalStop.h"


namespace mpasv::controller::route
		{

			RouteController::RouteController()
				: Controller("RouteController")
				, m_intercommServer(*this, "tcp://*:5556")
			{
				pushGoal(std::make_shared<RouteControllerGoalStop>());
			}

			RouteController::~RouteController()
			= default;

			void RouteController::setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager)
			{
				CHECK_NOT_NULLPTR(driverManager);
				m_driverManager = driverManager;
			}

			void RouteController::pushGoal(const std::shared_ptr<RouteControllerGoal>& goal)
			{
				Controller::pushGoal(goal);
			}

			std::shared_ptr<RouteControllerGoal> RouteController::currentGoal()
			{
				auto result = std::dynamic_pointer_cast<RouteControllerGoal>(Controller::currentGoal());
				CHECK_NOT_NULLPTR(result);
				return result;
			}

			bool RouteController::setup() noexcept
			{
				bool result = false;

				try
				{
					if (m_intercommServer.start())
					{
						m_globalSituationController = std::make_shared<controller::situation::GlobalSituationController>();
						m_globalSituationController->setDriverManager(m_driverManager);
						addPriorTask(m_globalSituationController);

						m_positionController = std::make_shared<controller::position::PositionController>();
						m_positionController->setDriverManager(m_driverManager);
						addPriorTask(m_positionController);

						// TODO

						result = Controller::setup();
					}
				}
				catch (std::exception & e)
				{
					LOG_DEBUG("Caught exception: ", e.what());
				}

				return result;
			}

			void RouteController::initialize() noexcept
			{
				Controller::initialize();

				// TODO
			}

			void RouteController::run(const second_t tickDuration) noexcept
			{
				currentGoal()->update(m_globalSituationController, m_positionController, tickDuration);
				if (currentGoal()->isComplete())
				{
					popGoal();
				}
			}

			void RouteController::finalize() noexcept
			{
				m_globalSituationController.reset();
				m_positionController.reset();

				// TODO

				Controller::finalize();
			}

			void RouteController::manageRequest(const tools::intercomm::IntercommMessage & request, tools::intercomm::IntercommMessage & reply)
			{
				switch (request.getType())
				{
					case DataType::dataRequestWaypointDescription:
					{
						manageRequest_waypointDescription(request, reply);
					}
					break;

					default:
					{
					}
					break;
				}
			}

			void RouteController::manageRequest_waypointDescription(const tools::intercomm::IntercommMessage & request,
																	tools::intercomm::IntercommMessage & reply)
			{
				DataReplyWaypointDescription replyData;

				std::shared_ptr<RouteControllerGoalFollowRoute> goal = std::dynamic_pointer_cast<RouteControllerGoalFollowRoute>(currentGoal());

				if (goal != nullptr)
				{
					const auto & requestData = request.data<DataRequestWaypointDescription>();

					LOG_DEBUG("Received request for WP descr No ", requestData.waypointIndex);

					if (requestData.waypointIndex < goal->route().getWaypointCount())
					{
						replyData.waypointCount = goal->route().getWaypointCount();
						replyData.waypointIndex = requestData.waypointIndex;

						replyData.lat_deg = tools::physics::floatFrom<degree_t>(goal->route().getWaypoint(replyData.waypointIndex).pos().lat());
						replyData.lon_deg = tools::physics::floatFrom<degree_t>(goal->route().getWaypoint(replyData.waypointIndex).pos().lon());
						replyData.maxDistanceToPass_m = tools::physics::floatFrom<meter_t>(goal->route().getWaypoint(replyData.waypointIndex).getMaxDistanceToPass());
						replyData.durationToStay_s = tools::physics::floatFrom<second_t>(goal->route().getWaypoint(replyData.waypointIndex).getDurationToStay());
						replyData.maxDistanceToStay_m = tools::physics::floatFrom<meter_t>(goal->route().getWaypoint(replyData.waypointIndex).getMaxDistanceToStay());
					}
					else
					{
						LOG_ERR("Invalid waypoint index requested: ", requestData.waypointIndex, " (max is ", goal->route().getWaypointCount() - 1, ")");
						replyData.waypointCount = 0;
					}
				}
				else
				{
					replyData.waypointCount = 0;
				}

				reply.setData(replyData);
			}

		} // mpasv
