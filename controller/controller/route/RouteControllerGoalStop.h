/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalStop.h
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#pragma once

#include "RouteControllerGoal.h"

#include "tools/physics/Units.h"

#include <memory>


namespace mpasv
{
	namespace controller
	{
		namespace situation
		{
			class GlobalSituationController;
		}

		namespace position
		{
			class PositionController;
		}

		namespace route
		{

			class RouteControllerGoalStop
			: public RouteControllerGoal
			{
			public:

				RouteControllerGoalStop();
				virtual ~RouteControllerGoalStop();

				virtual std::string toString() const override;

				virtual void update(std::shared_ptr<controller::situation::GlobalSituationController> globalSituationController,
									std::shared_ptr<controller::position::PositionController> positionController,
									const second_t tickDuration) override;

			protected:
			protected:
			private:
			private:

			};

		} // route
	} // controller
} // mpasv
