/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalFollowRoute.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#include "controller/route/RouteControllerGoalFollowRoute.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/situation/PhysicalSituationController.h"

#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/gps/GeoPos.h"
#include "tools/parameters/ParameterManager.h"


namespace mpasv::controller::route
{

	RouteControllerGoalFollowRoute::RouteControllerGoalFollowRoute()
	{
		// TODO
	}

	RouteControllerGoalFollowRoute::~RouteControllerGoalFollowRoute()
	= default;

	std::string RouteControllerGoalFollowRoute::toString() const
	{
		return std::string("Follow route goal: \"") + m_route.getName() + "\"";
	}

	void RouteControllerGoalFollowRoute::update(std::shared_ptr<controller::situation::GlobalSituationController> globalSituationController,
												std::shared_ptr<controller::position::PositionController> positionController,
												const second_t /*tickDuration*/)
	{
		if (!globalSituationController->physicalSituationController()->hasGeoPos())
		{
			return;
		}

		auto currentPos = globalSituationController->physicalSituationController()->getGeoPos();
		if (!m_startPos.isValid())
		{
			m_startPos = currentPos;
		}

		m_distanceToNextWaypoint = currentPos.distanceTo(m_route.getWaypoint(m_nextWaypointIndex).pos());

		while (m_distanceToNextWaypoint <= m_route.getWaypoint(m_nextWaypointIndex).getMaxDistanceToPass())
		{
			LOG_INFO("Passing waypoint ", m_nextWaypointIndex);

			m_nextWaypointIndex++;
			m_distanceToNextWaypoint = currentPos.distanceTo(m_route.getWaypoint(m_nextWaypointIndex).pos());

			if (m_nextWaypointIndex == m_route.getWaypointCount())
			{
				setComplete();
				break;
			}
		}

		if (!isComplete())
		{
			tools::geo::GeoPos currentRouteFirstPoint;
			tools::geo::GeoPos currentRouteSecondPoint = m_route.getWaypoint(m_nextWaypointIndex).pos();

			if (m_nextWaypointIndex == 0)
			{
				currentRouteFirstPoint = m_startPos;
			}
			else
			{
				currentRouteFirstPoint = m_route.getWaypoint(m_nextWaypointIndex - 1).pos();
			}

			m_crossTrackError = currentPos.crossTrackError(currentRouteFirstPoint, currentRouteSecondPoint);
			meter_t absCrossTrackError = fabs(m_crossTrackError);
			degree_t theoreticalCourse = currentRouteFirstPoint.bearingTo(currentRouteSecondPoint);

			degree_t directCourseToRoute;
			if (m_crossTrackError > 0.0_m)
			{
				directCourseToRoute = normalize360(theoreticalCourse - 90.0_deg);
			}
			else
			{
				directCourseToRoute = normalize360(theoreticalCourse + 90.0_deg);
			}

			// go directly to route
			m_targetPos = currentPos.forwardTo(directCourseToRoute, absCrossTrackError);

			// TODO improve route to store more information
			meter_t distanceToAnticipate = 10.0_m + globalSituationController->physicalSituationController()->getGPSSpeed() * 10.0_s;

			if (absCrossTrackError < distanceToAnticipate)
			{
				distanceToAnticipate -= absCrossTrackError;

				if (distanceToAnticipate <= m_targetPos.distanceTo(currentRouteSecondPoint))
				{
					m_targetPos = m_targetPos.forwardTo(theoreticalCourse, distanceToAnticipate);
				}
				else
				{
					m_targetPos = m_route.getWaypoint(m_nextWaypointIndex).pos();
				}
			}

			if (globalSituationController->physicalSituationController()->hasGlobalDriftBearing())
			{
				m_targetPos = m_targetPos.forwardTo(normalize360(globalSituationController->physicalSituationController()->getGlobalDriftBearing() + 180.0_deg),
													globalSituationController->physicalSituationController()->getGlobalDriftSpeed() * 10.0_s);
			}

			m_targetCourse = currentPos.bearingTo(m_targetPos);
			m_targetSpeed = 3.0_kts; // TODO
		}
		else
		{
			m_targetPos = globalSituationController->physicalSituationController()->getGeoPos();
			m_targetCourse = globalSituationController->physicalSituationController()->getGPSCourse();
			m_targetSpeed = 0.0_kts;
		}

		positionController->setCurrentGPSBearing(globalSituationController->physicalSituationController()->getGPSCourse());
		positionController->setCurrentHullBearing(globalSituationController->physicalSituationController()->getCompassBearing());
		positionController->setTargetGPSBearing(m_targetCourse);

		positionController->setCurrentGPSSpeed(globalSituationController->physicalSituationController()->getGPSSpeed());
		positionController->setTargetGPSSpeed(m_targetSpeed, true);

		positionController->setLinearAccelerationsAndRotationVelocity(globalSituationController->physicalSituationController()->getAccelX(),
																	  globalSituationController->physicalSituationController()->getGyroX());

		LOG_DEBUG(std::right, std::fixed,
				  "Speed ", std::setw(4), std::setprecision(1), globalSituationController->physicalSituationController()->getGPSSpeed(), " / ", m_targetSpeed, " | ",
				  "Course ", std::setw(4), std::setprecision(0), globalSituationController->physicalSituationController()->getGPSCourse(), " / ", std::setw(4), std::setprecision(0), m_targetCourse, " | ",
				  "Helm ", std::setw(3), std::setprecision(0), positionController->helmAngle(), " | ",
				  "LeftT ", std::setw(4), std::setprecision(0), positionController->throttle1(), " | ",
				  "RightT ", std::setw(4), std::setprecision(0), positionController->throttle2(), " | ",
				  "CTE ", std::setw(7), std::setprecision(1), m_crossTrackError, " | ",
				  "DTNW ", std::setw(7), std::setprecision(0), m_distanceToNextWaypoint);
	}

	void RouteControllerGoalFollowRoute::loadFromFile(const std::string & file_name)
	{
		m_route.loadMainWaypointsFromFile(file_name);
		m_nextWaypointIndex = 0;
	}

	void RouteControllerGoalFollowRoute::setSpeed(const knot_t speed)
	{
		CHECK_GT(speed, 0_kts);
		m_speed = speed;
	}

}
