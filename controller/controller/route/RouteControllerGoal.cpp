/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoal.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#include "controller/route/RouteControllerGoal.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/situation/PhysicalSituationController.h"

#include "tools/Logger.h"
#include "tools/Demangler.h"


namespace mpasv::controller::route
		{

			RouteControllerGoal::RouteControllerGoal()
			{
				// TODO
			}

			RouteControllerGoal::~RouteControllerGoal()
			= default;

			std::string RouteControllerGoal::toString() const
			{
				return std::string("Empty goal (does nothing)");
			}

			void RouteControllerGoal::update(std::shared_ptr<controller::situation::GlobalSituationController> /*globalSituationController*/,
											std::shared_ptr<controller::position::PositionController> /*positionController*/,
											const second_t /*tickDuration*/)
			{
				LOG_ERR(toString());
			}

		} // mpasv
