/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalFollowRoute.h
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#pragma once

#include "controller/route/RouteControllerGoal.h"

#include "tools/physics/Units.h"

#include "tools/routing/Route.h"

namespace mpasv
{
	namespace controller
	{

		namespace route
		{

			class RouteControllerGoalFollowRoute
			: public controller::route::RouteControllerGoal
			{
			public:

				RouteControllerGoalFollowRoute();
				virtual ~RouteControllerGoalFollowRoute();

				virtual std::string toString() const override;

				virtual void update(std::shared_ptr<controller::situation::GlobalSituationController> globalSituationController,
									std::shared_ptr<controller::position::PositionController> positionController,
									const second_t tickDuration) override;

				void loadFromFile(const std::string & file_name);
				void setSpeed(const knot_t speed);

				const routing::Route & route() const
				{
					return m_route;
				}

				routing::Route & route()
				{
					return m_route;
				}

				degree_t targetCourse() const
				{
					return m_targetCourse;
				}

				tools::geo::GeoPos targetPos() const
				{
					return m_targetPos;
				}

				meter_t crossTrackError() const
				{
					return m_crossTrackError;
				}

			protected:
			protected:
			private:
			private:

				routing::Route m_route;

				unsigned m_nextWaypointIndex{};
				meter_t m_distanceToNextWaypoint{};
				meter_t m_crossTrackError{};

				tools::geo::GeoPos m_startPos;


				tools::geo::GeoPos m_targetPos;
				degree_t m_targetCourse{};
				knot_t m_targetSpeed{};

				// TODO improve route to store speed, etc.
				knot_t m_speed{};

			};

		} // route
	} // controller
} // mpasv
