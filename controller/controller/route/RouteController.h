/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/Exception.h"

#include "tools/intercomm/IntercommServer.h"


namespace mpasv
{
	namespace drivers
	{
		class DriverManager;
	}

	namespace controller
	{
		namespace situation
		{
			class GlobalSituationController;
		}

		namespace position
		{
			class PositionController;
		}

		namespace route
		{

			class RouteControllerGoal;

			class RouteController
			: public controller::Controller
			{
			public:

				RouteController();
				virtual ~RouteController();

				void setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager);

				void pushGoal(const std::shared_ptr<RouteControllerGoal>& goal);
				std::shared_ptr<RouteControllerGoal> currentGoal();

				std::shared_ptr<controller::situation::GlobalSituationController> globalSituationController() const
				{
					CHECK_NOT_NULLPTR(m_globalSituationController);
					return m_globalSituationController;
				}

				std::shared_ptr<controller::position::PositionController> positionController() const
				{
					CHECK_NOT_NULLPTR(m_positionController);
					return m_positionController;
				}

				void manageRequest(const tools::intercomm::IntercommMessage & request, tools::intercomm::IntercommMessage & reply);

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

				void manageRequest_waypointDescription(const tools::intercomm::IntercommMessage & request, tools::intercomm::IntercommMessage & reply);

			private:

				std::shared_ptr<controller::situation::GlobalSituationController> m_globalSituationController;
				std::shared_ptr<controller::position::PositionController> m_positionController;

				std::shared_ptr<drivers::DriverManager> m_driverManager;

				tools::intercomm::IntercommServer<RouteController> m_intercommServer;

			};

		} // route
	} // controller
} // mpasv
