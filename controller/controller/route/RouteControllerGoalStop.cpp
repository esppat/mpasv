/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalStop.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#include "controller/route/RouteControllerGoalStop.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/situation/PhysicalSituationController.h"

#include "tools/Logger.h"
#include "tools/Demangler.h"


namespace mpasv::controller::route
		{

			RouteControllerGoalStop::RouteControllerGoalStop()
			{
				// TODO
			}

			RouteControllerGoalStop::~RouteControllerGoalStop()
			= default;

			std::string RouteControllerGoalStop::toString() const
			{
				return std::string("Empty goal (does nothing)");
			}

			void RouteControllerGoalStop::update(std::shared_ptr<controller::situation::GlobalSituationController> globalSituationController,
											std::shared_ptr<controller::position::PositionController> positionController,
											const second_t /*tickDuration*/)
			{
				if (globalSituationController->physicalSituationController()->hasGeoPos())
				{
					LOG_DEBUG("Speed = ", globalSituationController->physicalSituationController()->getGPSSpeed(),
							", Course = ", globalSituationController->physicalSituationController()->getGPSCourse());

					positionController->setCurrentGPSSpeed(globalSituationController->physicalSituationController()->getGPSSpeed());
				}

				if (globalSituationController->physicalSituationController()->hasCompassBearing())
				{
					positionController->setCurrentGPSBearing(globalSituationController->physicalSituationController()->getCompassBearing());
				}

				positionController->setTargetGPSBearing(0.0_deg);
				positionController->setTargetGPSSpeed(0.0_kts, true);
			}

		} // mpasv
