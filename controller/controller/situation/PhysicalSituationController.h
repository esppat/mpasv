/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PhysicalSituationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/SetUnsetValue.h"
#include "tools/physics/AmortizedValue.h"
#include "tools/physics/ValueHistory.h"
#include "tools/Tools.h"
#include "tools/Exception.h"
#include "tools/gps/GeoPos.h"


namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace GPS
		{
			class GPSSensor;
			class GPSSensorData;
		}

		namespace IMU
		{
			class IMUSensor;
			class IMUSensorData;
		}

		namespace water
		{
			class PropulsionWaterSpeedSensor;
			class PropulsionWaterSpeedSensorData;
		}

		namespace wind
		{
			class WindSpeedSensor;
			class WindSpeedSensorData;
			class WindDirectionSensor;
			class WindDirectionSensorData;
		}
	}

	namespace controller
	{
		namespace situation
		{

			class PhysicalSituationController
			: public controller::Controller
			{
			public:

				PhysicalSituationController();
				virtual ~PhysicalSituationController();

				void setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager);

				_DEF_HAS_AND_GET(gpsSpeed, GPSSpeed, knot_t)
				_DEF_HAS_AND_GET(gpsCourse, GPSCourse, degree_t)
				_DEF_HAS_AND_GET(geoPos, GeoPos, tools::geo::GeoPos)
				_DEF_HAS_AND_GET(relativeWaterSpeed, RelativeWaterSpeed, knot_t)
				_DEF_HAS_AND_GET(relativeWindSpeed, RelativeWindSpeed, knot_t)
				_DEF_HAS_AND_GET(relativeWindBearing, RelativeWindBearing, degree_t)
				_DEF_HAS_AND_GET(realWindSpeedCalculated, RealWindSpeedCalculated, knot_t)
				_DEF_HAS_AND_GET(realWinddegree_tCalculated, RealWinddegree_tCalculated, degree_t)
				_DEF_HAS_AND_GET(compassBearing, CompassBearing, degree_t)
				_DEF_HAS_AND_GET(globalDriftSpeed, GlobalDriftSpeed, knot_t)
				_DEF_HAS_AND_GET(globalDriftBearing, GlobalDriftBearing, degree_t)
				_DEF_HAS_AND_GET(accelX, AccelX, meters_per_second_squared_t)
				_DEF_HAS_AND_GET(accelY, AccelY, meters_per_second_squared_t)
				_DEF_HAS_AND_GET(accelZ, AccelZ, meters_per_second_squared_t)
				_DEF_HAS_AND_GET(gyroX, GyroX, degrees_per_second_t)
				_DEF_HAS_AND_GET(gyroY, GyroY, degrees_per_second_t)
				_DEF_HAS_AND_GET(gyroZ, GyroZ, degrees_per_second_t)

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

				_DEF_SET_WITH_LIMIT(gpsSpeed, GPSSpeed, knot_t, -30.0_kts, +30.0_kts)
				_DEF_SET_WITH_LIMIT(gpsCourse, GPSCourse, degree_t, 0.0_deg, 360.0_deg)
				_DEF_SET(geoPos, GeoPos, tools::geo::GeoPos)
				_DEF_SET_WITH_LIMIT(relativeWaterSpeed, RelativeWaterSpeed, knot_t, -30.0_kts, +30.0_kts)
				_DEF_SET_WITH_LIMIT(relativeWindSpeed, RelativeWindSpeed, knot_t, 0.0_kts, +200.0_kts)
				_DEF_SET_WITH_LIMIT(relativeWindBearing, RelativeWindBearing, degree_t, 0.0_deg, 360.0_deg)
				_DEF_SET_WITH_LIMIT(realWindSpeedCalculated, RealWindSpeedCalculated, knot_t, 0.0_kts, 200.0_kts)
				_DEF_SET_WITH_LIMIT(realWinddegree_tCalculated, RealWinddegree_tCalculated, degree_t, 0.0_deg, 360.0_deg)
				_DEF_SET_WITH_LIMIT(compassBearing, CompassBearing, degree_t, 0.0_deg, 360.0_deg)
				_DEF_SET(globalDriftSpeed, GlobalDriftSpeed, knot_t)
				_DEF_SET(globalDriftBearing, GlobalDriftBearing, degree_t)
				_DEF_SET(accelX, AccelX, meters_per_second_squared_t)
				_DEF_SET(accelY, AccelY, meters_per_second_squared_t)
				_DEF_SET(accelZ, AccelZ, meters_per_second_squared_t)
				_DEF_SET(gyroX, GyroX, degrees_per_second_t)
				_DEF_SET(gyroY, GyroY, degrees_per_second_t)
				_DEF_SET(gyroZ, GyroZ, degrees_per_second_t)

				_DEF_MEMBER(gpsSpeed, knot_t)
				_DEF_MEMBER(gpsCourse, degree_t)
				_DEF_MEMBER(geoPos, tools::geo::GeoPos)
				_DEF_MEMBER(relativeWaterSpeed, knot_t)
				_DEF_MEMBER(relativeWindSpeed, knot_t)
				_DEF_MEMBER(relativeWindBearing, degree_t)
				_DEF_MEMBER(realWindSpeedCalculated, knot_t)
				_DEF_MEMBER(realWinddegree_tCalculated, degree_t)
				_DEF_MEMBER(compassBearing, degree_t)
				_DEF_MEMBER(globalDriftSpeed, knot_t)
				_DEF_MEMBER(globalDriftBearing, degree_t)
				_DEF_MEMBER(accelX, meters_per_second_squared_t)
				_DEF_MEMBER(accelY, meters_per_second_squared_t)
				_DEF_MEMBER(accelZ, meters_per_second_squared_t)
				_DEF_MEMBER(gyroX, degrees_per_second_t)
				_DEF_MEMBER(gyroY, degrees_per_second_t)
				_DEF_MEMBER(gyroZ, degrees_per_second_t)

				std::shared_ptr<drivers::DriverManager> m_driverManager;

				std::shared_ptr<drivers::IMU::IMUSensor> m_IMUSensor;
				std::shared_ptr<drivers::IMU::IMUSensorData> m_IMUSensorData;

				std::shared_ptr<drivers::GPS::GPSSensor> m_GPSSensor;
				std::shared_ptr<drivers::GPS::GPSSensorData> m_GPSSensorData;

				std::shared_ptr<drivers::water::PropulsionWaterSpeedSensor> m_propsulsionWaterSpeedSensor;
				std::shared_ptr<drivers::water::PropulsionWaterSpeedSensorData> m_propsulsionWaterSpeedSensorData;

				std::shared_ptr<drivers::wind::WindSpeedSensor> m_windSpeedSensor;
				std::shared_ptr<drivers::wind::WindSpeedSensorData> m_windSpeedSensorData;

				std::shared_ptr<drivers::wind::WindDirectionSensor> m_windDirectionSensor;
				std::shared_ptr<drivers::wind::WindDirectionSensorData> m_windDirectionSensorData;

			};

		} // situation
	} // controller
} // mpasv
