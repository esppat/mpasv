/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RouteController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"


namespace mpasv
{
	namespace drivers
	{
		class DriverManager;
	}

	namespace controller
	{
		namespace situation
		{
			class PhysicalSituationController;
			class InternalSituationController;

			class GlobalSituationController
				: public controller::Controller
			{
			public:

				GlobalSituationController();
				virtual ~GlobalSituationController();

				void setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager);

				std::shared_ptr<PhysicalSituationController> physicalSituationController()
				{
					return m_physicalSituationController;
				}

				std::shared_ptr<InternalSituationController> internalSituationController()
				{
					return m_internalSituationController;
				}

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

			private:

				std::shared_ptr<drivers::DriverManager> m_driverManager;

				std::shared_ptr<PhysicalSituationController> m_physicalSituationController;
				std::shared_ptr<InternalSituationController> m_internalSituationController;

			};

		} // situation
	} // controller
} // mpasv
