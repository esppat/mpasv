/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   InternalSituationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/SetUnsetValue.h"
#include "tools/physics/AmortizedValue.h"
#include "tools/physics/ValueHistory.h"
#include "tools/Tools.h"
#include "tools/Exception.h"
#include "tools/gps/GeoPos.h"

#include "drivers/AbstractDriver.h"

#include <map>


namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace ampere
		{
			class AmperageSensor;
			class AmperageSensorData;
		}

		namespace voltage
		{
			class VoltageSensor;
			class VoltageSensorData;
		}
	}

	namespace controller
	{
		namespace situation
		{

			class InternalSituationController
				: public controller::Controller
			{
			public:

				InternalSituationController();
				virtual ~InternalSituationController();

				void setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager);

				_DEF_HAS_AND_GET(engineVoltage, EngineVoltage, volt_t)
				_DEF_HAS_AND_GET(engineAmperage, EngineAmperage, ampere_t)

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;
				virtual void initialize() noexcept override;
				virtual void run(const second_t tickDuration) noexcept override;
				virtual void finalize() noexcept override;

				_DEF_SET_WITH_LIMIT(engineVoltage, EngineVoltage, volt_t, 0.0_V, +100.0_V)
				_DEF_SET_WITH_LIMIT(engineAmperage, EngineAmperage, ampere_t, 0.0_A, +200.0_A)

				_DEF_MEMBER(engineVoltage, volt_t)
				_DEF_MEMBER(engineAmperage, ampere_t)

				std::shared_ptr<drivers::DriverManager> m_driverManager;

				std::map<std::string, std::shared_ptr<drivers::AbstractDriver>> * m_engine1VoltageSensor{};
				std::map<std::string, std::shared_ptr<drivers::voltage::VoltageSensorData>> m_engine1VoltageSensorData;
				std::map<std::string, std::shared_ptr<drivers::AbstractDriver>> * m_engine1AmperageSensor{};
				std::map<std::string, std::shared_ptr<drivers::ampere::AmperageSensorData>> m_engine1AmperageSensorData;

				watt_hour_t m_consummedEnergy = 0.0_Wh;

			};

		} // situation
	} // controller
} // mpasv
