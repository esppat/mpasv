/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PhysicalSituationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/situation/PhysicalSituationController.h"

#include "tools/Exception.h"
#include "tools/Logger.h"

#include "drivers/DriverManager.h"
#include "drivers/sensors/GPS/GPSSensor.h"
#include "drivers/sensors/GPS/GPSSensorData.h"
#include "drivers/sensors/IMU/IMUSensor.h"
#include "drivers/sensors/IMU/IMUSensorData.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensor.h"
#include "drivers/sensors/water/PropulsionWaterSpeedSensorData.h"
#include "drivers/sensors/wind/WindDirectionSensor.h"
#include "drivers/sensors/wind/WindDirectionSensorData.h"
#include "drivers/sensors/wind/WindSpeedSensor.h"
#include "drivers/sensors/wind/WindSpeedSensorData.h"


namespace mpasv::controller::situation
{

	PhysicalSituationController::PhysicalSituationController()
		: Controller("PhysicalSituationController")
	{
	}

	PhysicalSituationController::~PhysicalSituationController()
	= default;

	void PhysicalSituationController::setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	bool PhysicalSituationController::setup() noexcept
	{
		bool result = false;

		try
		{
			m_IMUSensor = m_driverManager->getSensor<drivers::IMU::IMUSensor>("IMU 1");
			m_IMUSensorData = std::make_shared<drivers::IMU::IMUSensorData>();

			m_GPSSensor = m_driverManager->getSensor<drivers::GPS::GPSSensor>("GPS 1");
			m_GPSSensorData = std::make_shared<drivers::GPS::GPSSensorData>();

			m_propsulsionWaterSpeedSensor = m_driverManager->getSensor<drivers::water::PropulsionWaterSpeedSensor>("Lock 1");
			m_propsulsionWaterSpeedSensorData = std::make_shared<drivers::water::PropulsionWaterSpeedSensorData>();

			m_windSpeedSensor = m_driverManager->getSensor<drivers::wind::WindSpeedSensor>("Wind 1");
			m_windSpeedSensorData = std::make_shared<drivers::wind::WindSpeedSensorData>();

			m_windDirectionSensor = m_driverManager->getSensor<drivers::wind::WindDirectionSensor>("Wind 1");
			m_windDirectionSensorData = std::make_shared<drivers::wind::WindDirectionSensorData>();

			// TODO

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void PhysicalSituationController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void PhysicalSituationController::run(const second_t tickDuration) noexcept
	{
		bool haveFixBefore = m_GPSSensorData->m_hasFix && m_GPSSensorData->m_geoPos.isValid();

		m_IMUSensor->pull(m_IMUSensorData);
		m_GPSSensor->pull(m_GPSSensorData);
		m_propsulsionWaterSpeedSensor->pull(m_propsulsionWaterSpeedSensorData);
		m_windDirectionSensor->pull(m_windDirectionSensorData);
		m_windSpeedSensor->pull(m_windSpeedSensorData);

		bool haveFixAfter = m_GPSSensorData->m_hasFix && m_GPSSensorData->m_geoPos.isValid();

		if (haveFixBefore != haveFixAfter)
		{
			if (haveFixAfter)
			{
				LOG_INFO("Gained GPS fix");
			}
			else
			{
				LOG_WARN("Lost GPS fix");
			}
		}

		if (haveFixAfter)
		{
			setGPSSpeed(m_GPSSensorData->m_gpsSpeed);
			setGPSCourse(m_GPSSensorData->m_gpsCourse);
			setGeoPos(m_GPSSensorData->m_geoPos);

			auto posStart = m_GPSSensorData->m_geoPos;
			auto posEnd = posStart.forwardTo(m_windDirectionSensorData->m_windDirection, m_windSpeedSensorData->m_windSpeed * tickDuration);
			posEnd = posEnd.forwardTo(m_GPSSensorData->m_gpsCourse, -m_GPSSensorData->m_gpsSpeed * tickDuration);

			knot_t realWindSpeedCalculated = posStart.distanceTo(posEnd) / tickDuration;
			degree_t realWinddegree_tCalculated = posEnd.bearingTo(posStart);

			setRealWindSpeedCalculated(realWindSpeedCalculated);
			setRealWinddegree_tCalculated(normalize360(realWinddegree_tCalculated + 180.0_deg));

			setRelativeWindSpeed(m_windSpeedSensorData->m_windSpeed);
			setRelativeWindBearing(m_windDirectionSensorData->m_windDirection);
		}
		else
		{
			unsetGPSSpeed();
			unsetGPSCourse();
			unsetGeoPos();

			unsetRealWindSpeedCalculated();
			unsetRealWinddegree_tCalculated();
		}

		if (m_propsulsionWaterSpeedSensorData->m_propulsionWaterSpeed != -9999.0_kts)
		{
			setRelativeWaterSpeed(m_propsulsionWaterSpeedSensorData->m_propulsionWaterSpeed);
		}

		if (m_IMUSensorData->m_hullHeading != -9999.0_deg)
		{
			setCompassBearing(m_IMUSensorData->m_hullHeading);

			if (haveFixAfter)
			{
				// TODO use an historic here
				auto posStart = m_GPSSensorData->m_geoPos;
				auto posEndTheorical = posStart.forwardTo(getCompassBearing(), m_propsulsionWaterSpeedSensorData->m_propulsionWaterSpeed * tickDuration);
				auto posEndReal = posStart.forwardTo(getGPSCourse(), getGPSSpeed() * tickDuration);

				setGlobalDriftBearing(posEndTheorical.bearingTo(posEndReal));
				setGlobalDriftSpeed((posEndTheorical.distanceTo(posEndReal)) / tickDuration);
			}
			else
			{
				unsetGlobalDriftBearing();
				unsetGlobalDriftSpeed();
			}
		}
		else
		{
			unsetCompassBearing();
			unsetGlobalDriftBearing();
			unsetGlobalDriftSpeed();
		}

		if (m_IMUSensorData->m_accelX != -9999.0_mps_sq)
			setAccelX(m_IMUSensorData->m_accelX);
		else
			unsetAccelX();

		if (m_IMUSensorData->m_accelY != -9999.0_mps_sq)
			setAccelY(m_IMUSensorData->m_accelY);
		else
			unsetAccelY();

		if (m_IMUSensorData->m_accelZ != -9999.0_mps_sq)
			setAccelZ(m_IMUSensorData->m_accelZ);
		else
			unsetAccelZ();

		if (m_IMUSensorData->m_gyroX != -9999.0_dps)
			setGyroX(m_IMUSensorData->m_gyroX);
		else
			unsetGyroX();

		if (m_IMUSensorData->m_gyroY != -9999.0_dps)
			setGyroY(m_IMUSensorData->m_gyroY);
		else
			unsetGyroY();

		if (m_IMUSensorData->m_gyroZ != -9999.0_dps)
			setGyroZ(m_IMUSensorData->m_gyroZ);
		else
			unsetGyroZ();
	}

	void PhysicalSituationController::finalize() noexcept
	{
		m_IMUSensor.reset();
		m_IMUSensorData.reset();

		m_GPSSensor.reset();
		m_GPSSensorData.reset();

		m_propsulsionWaterSpeedSensor.reset();
		m_propsulsionWaterSpeedSensorData.reset();

		m_windSpeedSensor.reset();
		m_windSpeedSensorData.reset();

		m_windDirectionSensor.reset();
		m_windDirectionSensorData.reset();

		// TODO

		Controller::finalize();
	}

} // mpasv
