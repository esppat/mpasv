/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/situation/GlobalSituationController.h"
#include "controller/situation/PhysicalSituationController.h"
#include "controller/situation/InternalSituationController.h"

#include "tools/Exception.h"
#include "tools/Logger.h"


namespace mpasv::controller::situation
		{

			GlobalSituationController::GlobalSituationController()
				: Controller("GlobalSituationController")
			{
			}

			GlobalSituationController::~GlobalSituationController()
			= default;

			void GlobalSituationController::setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager)
			{
				CHECK_NOT_NULLPTR(driverManager);
				m_driverManager = driverManager;
			}

			bool GlobalSituationController::setup() noexcept
			{
				bool result = false;

				try
				{
					m_physicalSituationController = std::make_shared<PhysicalSituationController>();
					m_physicalSituationController->setDriverManager(m_driverManager);
					addPriorTask(m_physicalSituationController);

					m_internalSituationController = std::make_shared<InternalSituationController>();
					m_internalSituationController->setDriverManager(m_driverManager);
					addPriorTask(m_internalSituationController);

					// TODO

					result = Controller::setup();
				}
				catch (std::exception & e)
				{
					LOG_DEBUG("Caught exception: ", e.what());
				}

				return result;
			}

			void GlobalSituationController::initialize() noexcept
			{
				Controller::initialize();

				// TODO
			}

			void GlobalSituationController::run(const second_t /*tickDuration*/) noexcept
			{
			}

			void GlobalSituationController::finalize() noexcept
			{
				m_physicalSituationController.reset();
				m_internalSituationController.reset();

				// TODO

				Controller::finalize();
			}

		} // mpasv
