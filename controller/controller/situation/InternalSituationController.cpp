/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   InternalSituationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/situation/InternalSituationController.h"

#include "tools/Exception.h"
#include "tools/Logger.h"
#include "tools/parameters/ParameterManager.h"

#include "drivers/DriverManager.h"
#include "drivers/sensors/ampere/AmperageSensor.h"
#include "drivers/sensors/ampere/AmperageSensorData.h"
#include "drivers/sensors/ampere/AmperageSensorSim.h"
#include "drivers/sensors/voltage/VoltageSensor.h"
#include "drivers/sensors/voltage/VoltageSensorData.h"
#include "drivers/sensors/voltage/VoltageSensorSim.h"


namespace mpasv::controller::situation
		{

			InternalSituationController::InternalSituationController()
				: Controller("InternalSituationController")
			{
			}

			InternalSituationController::~InternalSituationController()
			= default;

			void InternalSituationController::setDriverManager(const std::shared_ptr<drivers::DriverManager>& driverManager)
			{
				CHECK_NOT_NULLPTR(driverManager);
				m_driverManager = driverManager;
			}

			bool InternalSituationController::setup() noexcept
			{
				bool result = false;

				try
				{
					m_engine1VoltageSensor = &m_driverManager->getDriversByType<drivers::voltage::VoltageSensor>();
					for (auto & it : *m_engine1VoltageSensor)
					{
						m_engine1VoltageSensorData[it.first] = std::make_shared<drivers::voltage::VoltageSensorData>();
					}

					m_engine1AmperageSensor = &m_driverManager->getDriversByType<drivers::ampere::AmperageSensor>();
					for (auto & it : *m_engine1AmperageSensor)
					{
						m_engine1AmperageSensorData[it.first] = std::make_shared<drivers::ampere::AmperageSensorData>();
					}

					// TODO

					result = Controller::setup();
				}
				catch (std::exception & e)
				{
					LOG_DEBUG("Caught exception: ", e.what());
				}

				return result;
			}

			void InternalSituationController::initialize() noexcept
			{
				Controller::initialize();

				// TODO
			}

			void InternalSituationController::run(const second_t tickDuration) noexcept
			{
				for (auto & it : *m_engine1VoltageSensor)
				{
					it.second->pull(m_engine1VoltageSensorData[it.first]);
				}
				for (auto & it : *m_engine1AmperageSensor)
				{
					it.second->pull(m_engine1AmperageSensorData[it.first]);
				}

				watt_t immediatepower = 0_W;

				for (auto & it : m_engine1VoltageSensorData)
				{
					immediatepower += it.second->m_voltage * m_engine1AmperageSensorData[it.first]->m_amperage;
				}

				m_consummedEnergy += tickDuration * immediatepower;
			}

			void InternalSituationController::finalize() noexcept
			{
				// TODO

				Controller::finalize();
			}

		} // mpasv
