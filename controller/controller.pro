TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
		controller/Controller.cpp \
		controller/ControllerGoal.cpp \
		controller/communication/CommunicationController.cpp \
		controller/position/PositionController.cpp \
		controller/route/RouteController.cpp \
		controller/route/RouteControllerGoal.cpp \
		controller/route/RouteControllerGoalFollowRoute.cpp \
		controller/route/RouteControllerGoalStop.cpp \
		controller/simulation/SimuDaggerBoardPlane.cpp \
		controller/simulation/SimuElement.cpp \
		controller/simulation/SimuPropeller.cpp \
		controller/simulation/SimulationController.cpp \
		controller/simulation/SimulationController_ReactPhysics3d.cpp \
		controller/situation/GlobalSituationController.cpp \
		controller/situation/InternalSituationController.cpp \
		controller/situation/PhysicalSituationController.cpp \
		drivers/AbstractActuator.cpp \
		drivers/AbstractActuatorData.cpp \
		drivers/AbstractDriver.cpp \
		drivers/AbstractDriverData.cpp \
		drivers/AbstractSensor.cpp \
		drivers/AbstractSensorData.cpp \
		drivers/DriverFactoy.cpp \
		drivers/DriverManager.cpp \
		drivers/actuators/brushless_controller/BrushlessControllerData.cpp \
		drivers/actuators/brushless_controller/BushlessController.cpp \
		drivers/actuators/brushless_controller/BushlessControllerSim.cpp \
		drivers/actuators/pwm_servo/PWMServo.cpp \
		drivers/actuators/pwm_servo/PWMServoData.cpp \
		drivers/actuators/pwm_servo/PWMServoSim.cpp \
		drivers/sensors/AIS/AISSensor.cpp \
		drivers/sensors/AIS/AISSensorData.cpp \
		drivers/sensors/AIS/AISSensorSim.cpp \
		drivers/sensors/GPS/GPSSensor.cpp \
		drivers/sensors/GPS/GPSSensorData.cpp \
		drivers/sensors/GPS/GPSSensorSim.cpp \
		drivers/sensors/IMU/IMUSensor.cpp \
		drivers/sensors/IMU/IMUSensorData.cpp \
		drivers/sensors/IMU/IMUSensorSim.cpp \
		drivers/sensors/ampere/AmperageSensor.cpp \
		drivers/sensors/ampere/AmperageSensorData.cpp \
		drivers/sensors/ampere/AmperageSensorSim.cpp \
		drivers/sensors/voltage/VoltageSensor.cpp \
		drivers/sensors/voltage/VoltageSensorData.cpp \
		drivers/sensors/voltage/VoltageSensorSim.cpp \
		drivers/sensors/water/PropulsionWaterSpeedSensor.cpp \
		drivers/sensors/water/PropulsionWaterSpeedSensorData.cpp \
		drivers/sensors/water/PropulsionWaterSpeedSensorSim.cpp \
		drivers/sensors/wind/WindDirectionSensor.cpp \
		drivers/sensors/wind/WindDirectionSensorData.cpp \
		drivers/sensors/wind/WindDirectionSensorSim.cpp \
		drivers/sensors/wind/WindSpeedSensor.cpp \
		drivers/sensors/wind/WindSpeedSensorData.cpp \
		drivers/sensors/wind/WindSpeedSensorSim.cpp \
		main.cpp \
		virtual_captain/VirtualCaptain.cpp

HEADERS += \
	controller/Controller.h \
	controller/ControllerGoal.h \
	controller/communication/CommunicationController.h \
	controller/position/PositionController.h \
	controller/route/RouteController.h \
	controller/route/RouteControllerGoal.h \
	controller/route/RouteControllerGoalFollowRoute.h \
	controller/route/RouteControllerGoalStop.h \
	controller/simulation/SimuDaggerBoardPlane.h \
	controller/simulation/SimuElement.h \
	controller/simulation/SimuPropeller.h \
	controller/simulation/SimulationController.h \
	controller/simulation/SimulationController_ReactPhysics3d.h \
	controller/situation/GlobalSituationController.h \
	controller/situation/InternalSituationController.h \
	controller/situation/PhysicalSituationController.h \
	drivers/AbstractActuator.h \
	drivers/AbstractActuatorData.h \
	drivers/AbstractDriver.h \
	drivers/AbstractDriverData.h \
	drivers/AbstractSensor.h \
	drivers/AbstractSensorData.h \
	drivers/DriverFactory.h \
	drivers/DriverManager.h \
	drivers/actuators/brushless_controller/BrushlessController.h \
	drivers/actuators/brushless_controller/BrushlessControllerData.h \
	drivers/actuators/brushless_controller/BrushlessControllerSim.h \
	drivers/actuators/pwm_servo/PWMServo.h \
	drivers/actuators/pwm_servo/PWMServoData.h \
	drivers/actuators/pwm_servo/PWMServoSim.h \
	drivers/sensors/AIS/AISSensor.h \
	drivers/sensors/AIS/AISSensorData.h \
	drivers/sensors/AIS/AISSensorSim.h \
	drivers/sensors/GPS/GPSSensor.h \
	drivers/sensors/GPS/GPSSensorData.h \
	drivers/sensors/GPS/GPSSensorSim.h \
	drivers/sensors/IMU/IMUSensor.h \
	drivers/sensors/IMU/IMUSensorData.h \
	drivers/sensors/IMU/IMUSensorSim.h \
	drivers/sensors/ampere/AmperageSensor.h \
	drivers/sensors/ampere/AmperageSensorData.h \
	drivers/sensors/ampere/AmperageSensorSim.h \
	drivers/sensors/voltage/VoltageSensor.h \
	drivers/sensors/voltage/VoltageSensorData.h \
	drivers/sensors/voltage/VoltageSensorSim.h \
	drivers/sensors/water/PropulsionWaterSpeedSensor.h \
	drivers/sensors/water/PropulsionWaterSpeedSensorData.h \
	drivers/sensors/water/PropulsionWaterSpeedSensorSim.h \
	drivers/sensors/wind/WindDirectionSensor.h \
	drivers/sensors/wind/WindDirectionSensorData.h \
	drivers/sensors/wind/WindDirectionSensorSim.h \
	drivers/sensors/wind/WindSpeedSensor.h \
	drivers/sensors/wind/WindSpeedSensorData.h \
	drivers/sensors/wind/WindSpeedSensorSim.h \
	virtual_captain/LevelOfAutonomy.h \
	virtual_captain/VirtualCaptain.h

CONFIG += link_pkgconfig

PKGCONFIG += geographiclib
PKGCONFIG += libgps
PKGCONFIG += libzmq

DISTFILES += \
	../rundir/data/parameters/parameters.json \
	../rundir/data/routes/Le Cres, tour - part.xml \
	../rundir/data/routes/Le Cres, tour 2.xml \
	../rundir/data/routes/Le Cres, tour.xml \
	../rundir/data/routes/Palavas, spirale - part.xml \
	../rundir/data/routes/Palavas, spirale.xml \
	../rundir/data/routes/__do_not_remove \
	../rundir/data/routes/bassin_jacques_coeur__montpellier.json \
	../rundir/data/routes/bassin_jacques_coeur__montpellier_court.json \
	../rundir/data/routes/etang_du_ponant__la_grande_motte.json \
	../rundir/data/routes/etang_du_ponant__la_grande_motte_court.json \
	../rundir/data/routes/montpellier_baleares_montpellier.json \
	rundir/data/parameters/parameters.json

# mpasv::base

LIBS += -L$$OUT_PWD/../base/ -lbase

INCLUDEPATH += $$PWD/../base
DEPENDPATH += $$PWD/../base

# ReactPhysics3d

LIBS += -L$$PWD/reactphysics3d/lib/ -lreactphysics3d

INCLUDEPATH += $$PWD/reactphysics3d/src
DEPENDPATH += $$PWD/reactphysics3d/src

PRE_TARGETDEPS += $$PWD/reactphysics3d/lib/libreactphysics3d.a
