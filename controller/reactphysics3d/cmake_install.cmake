# Install script for directory: /home/esppat/dev/mpasv/controller/reactphysics3d

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/esppat/dev/mpasv/controller/reactphysics3d/lib/libreactphysics3d.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/reactphysics3d" TYPE FILE FILES
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/configuration.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/decimal.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/reactphysics3d.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/body/Body.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/body/CollisionBody.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/body/RigidBody.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/ContactPointInfo.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/ContactManifoldInfo.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/broadphase/BroadPhaseAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/broadphase/DynamicAABBTree.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/CollisionDispatch.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/DefaultCollisionDispatch.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/GJK/VoronoiSimplex.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/GJK/GJKAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/SAT/SATAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/NarrowPhaseAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/SphereVsSphereAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/CapsuleVsCapsuleAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/SphereVsCapsuleAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/SphereVsConvexPolyhedronAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/CapsuleVsConvexPolyhedronAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/narrowphase/ConvexPolyhedronVsConvexPolyhedronAlgorithm.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/AABB.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/ConvexShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/ConvexPolyhedronShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/ConcaveShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/BoxShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/CapsuleShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/CollisionShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/ConvexMeshShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/SphereShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/TriangleShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/ConcaveMeshShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/shapes/HeightFieldShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/RaycastInfo.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/ProxyShape.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/TriangleVertexArray.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/PolygonVertexArray.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/TriangleMesh.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/PolyhedronMesh.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/HalfEdgeStructure.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/CollisionDetection.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/NarrowPhaseInfo.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/ContactManifold.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/ContactManifoldSet.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/MiddlePhaseTriangleCallback.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/constraint/BallAndSocketJoint.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/constraint/ContactPoint.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/constraint/FixedJoint.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/constraint/HingeJoint.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/constraint/Joint.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/constraint/SliderJoint.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/CollisionWorld.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/ConstraintSolver.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/ContactSolver.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/DynamicsWorld.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/EventListener.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/Island.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/Material.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/OverlappingPair.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/Timer.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/engine/Timer.cpp"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/CollisionCallback.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/collision/OverlapCallback.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/mathematics.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/mathematics_functions.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Matrix2x2.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Matrix3x3.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Quaternion.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Transform.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Vector2.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Vector3.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/mathematics/Ray.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/memory/MemoryAllocator.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/memory/DefaultPoolAllocator.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/memory/DefaultSingleFrameAllocator.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/memory/DefaultAllocator.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/memory/MemoryManager.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/containers/Stack.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/containers/LinkedList.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/containers/List.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/containers/Map.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/containers/Set.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/containers/Pair.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/utils/Profiler.h"
    "/home/esppat/dev/mpasv/controller/reactphysics3d/src/utils/Logger.h"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/esppat/dev/mpasv/controller/reactphysics3d/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
