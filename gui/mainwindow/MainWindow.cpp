#include "MainWindow.h"


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	setDockOptions(QMainWindow::AnimatedDocks |
				   QMainWindow::AllowNestedDocks |
				   QMainWindow::GroupedDragging);

	createController();
	createMenus();
	createWindows();

	createConnections();
}

MainWindow::~MainWindow()
= default;

void MainWindow::createController()
{
	m_controller = new Controller();
}

void MainWindow::createWindows()
{
	initDockWindow<MapWidget>(m_mapWindow, "Map", "&Map", m_menuMap, Qt::RightDockWidgetArea);

	initDockWindow<BasicGaugeWidget>(m_gpsSpeedWindow, "GPS speed", "GPS &speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_relativeWaterSpeedWindow, "Water speed", "&Water speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_gpsCourseWindow, "GPS course", "GPS &course", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_compassdegree_tWindow, "Compass degree_t", "Compass &degree_t", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_targetCourseWindow, "Target course", "Target &course", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_courseErrorWindow, "Course error", "Course &error", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_crossTrackErrorWindow, "Cross track error", "Cross track error", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_helmWindow, "Helm", "&Helm", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_relativeWindSpeedWindow, "Relative wind speed", "&Relative wind speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_relativeWinddegree_tWindow, "Relative wind degree_t", "Re&lative wind degree_t", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_realWindSpeedWindow, "Real wind speed", "Real wind speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_realWinddegree_tWindow, "Real wind degree_t", "Real wind degree_t", m_menuGauges, Qt::TopDockWidgetArea);

#ifdef __DEBUG__
	createDebugCharts();
#endif
}

void MainWindow::createMenus()
{
	m_menuApplication = menuBar()->addMenu("&Application");
	m_menuVehicle = menuBar()->addMenu("&Vehicle");
	m_menuMap = menuBar()->addMenu("&Map");
	m_menuGauges = menuBar()->addMenu("&Gauges");
	m_menuCharts = menuBar()->addMenu("&Charts");

	auto * actionGroup = new QActionGroup(this);

	m_actionConnectVehicle = new QAction("Connect", this);
	m_actionConnectVehicle->setCheckable(true);
	actionGroup->addAction(m_actionConnectVehicle);
	m_menuVehicle->addAction(m_actionConnectVehicle);

	m_actionDisconnectVehicle = new QAction("Disconnect", this);
	m_actionDisconnectVehicle->setCheckable(true);
	m_actionDisconnectVehicle->setChecked(true);
	actionGroup->addAction(m_actionDisconnectVehicle);
	m_menuVehicle->addAction(m_actionDisconnectVehicle);
}

void MainWindow::createConnections()
{
	// controller updates
	CHECK_TRUE((bool)connect(m_controller, &Controller::GSPSpeedChanged, m_gpsSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::GSPCourseChanged, m_gpsCourseWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::GPSPositionChanged, m_mapWindow->m_widget, &MapWidget::createCircleAtPos));
	CHECK_TRUE((bool)connect(m_controller, &Controller::GPSPositionChanged, m_mapWindow->m_widget, &MapWidget::gotoGPSPosition));
	CHECK_TRUE((bool)connect(m_controller, &Controller::createSegment, m_mapWindow->m_widget, &MapWidget::createSegment));
	CHECK_TRUE((bool)connect(m_controller, &Controller::relativeWaterSpeedChanged, m_relativeWaterSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::relativeWindSpeedChanged, m_relativeWindSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::relativeWinddegree_tChanged, m_relativeWinddegree_tWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::realWindSpeedChanged, m_realWindSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::realWinddegree_tChanged, m_realWinddegree_tWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::compassdegree_tChanged, m_compassdegree_tWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::helmPercentChanged, m_helmWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::targetCourseChanged, m_targetCourseWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::courseErrorChanged, m_courseErrorWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &Controller::crossTrackErrorChanged, m_crossTrackErrorWindow->m_widget, &BasicGaugeWidget::setCurrentValue));

	// Vehicle connection
	CHECK_TRUE((bool)connect(m_actionConnectVehicle, &QAction::triggered, m_controller, &Controller::connectToVehicle));
	CHECK_TRUE((bool)connect(m_controller, &Controller::connectedToVehicle, m_actionConnectVehicle, &QAction::setChecked));
	CHECK_TRUE((bool)connect(m_actionDisconnectVehicle, &QAction::triggered, m_controller, &Controller::disconnectFromVehicle));
	CHECK_TRUE((bool)connect(m_controller, &Controller::disconnectedFromVehicle, m_actionDisconnectVehicle, &QAction::setChecked));
}
