#pragma once

#include <QMainWindow>
#include <QMenuBar>
#include <QAction>
#include <QPointer>
#include <QDockWidget>

#include <QtCharts/QChartView>
QT_CHARTS_USE_NAMESPACE

#include "widgets/map/MapWidget.h"
#include "widgets/gauge/BasicGaugeWidget.h"

#include "controller/Controller.h"

#include "tools/Exception.h"


class MainWindow : public QMainWindow
{
	Q_OBJECT

public:

	MainWindow(QWidget *parent = nullptr);
	virtual ~MainWindow();

protected:

protected:

private slots:

private:

	void createController();
	void createWindows();
	void createMenus();

	void createConnections();

	struct DockWindowAction
	{
		DockWindowAction(MainWindow * parent,
						const QString & menuText,
						QMenu * menu,
						 QPointer<QDockWidget> window)
		{
			CHECK_NOT_NULLPTR(parent);
			CHECK_NOT_EMPTY(menuText);
			//m_action = new QAction(menuText, parent);
			m_action = window->toggleViewAction();
			menu->addAction(m_action);
		}

		QPointer<QAction> m_action = nullptr;
	};

	template <class WIDGET>
	struct DockWindowEntry
	{
		DockWindowEntry(MainWindow * parent,
						const QString & title,
						Qt::DockWidgetArea area)
		{
			CHECK_NOT_NULLPTR(parent);
			m_window = new QDockWidget(title, parent);
			m_widget = new WIDGET(title, m_window);
			m_window->setWidget(m_widget);
			parent->addDockWidget(area, m_window);
		}

		QDockWidget * m_window;
		WIDGET * m_widget;
		DockWindowAction * m_action;
	};

	template <class WIDGET>
	void initDockWindow(DockWindowEntry<WIDGET> * & entry,
						const QString & title,
						const QString & menuText,
						QMenu * menu,
						Qt::DockWidgetArea area)
	{
		entry = new DockWindowEntry<WIDGET>(this, title, area);
		entry->m_action = new DockWindowAction(this, menuText, menu, entry->m_window);
	}

private:

	Controller * m_controller = nullptr;

	QMenu * m_menuApplication = nullptr;
	QMenu * m_menuVehicle = nullptr;
	QMenu * m_menuMap = nullptr;
	QMenu * m_menuGauges = nullptr;
	QMenu * m_menuCharts = nullptr;

	QAction * m_actionConnectVehicle = nullptr;
	QAction * m_actionDisconnectVehicle = nullptr;

	DockWindowEntry<MapWidget> * m_mapWindow = nullptr;

	DockWindowEntry<BasicGaugeWidget> * m_gpsSpeedWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_relativeWaterSpeedWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_gpsCourseWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_compassdegree_tWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_targetCourseWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_courseErrorWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_helmWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_relativeWindSpeedWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_relativeWinddegree_tWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_realWindSpeedWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_realWinddegree_tWindow = nullptr;
	DockWindowEntry<BasicGaugeWidget> * m_crossTrackErrorWindow = nullptr;

	DockWindowEntry<QChartView> * m_chart = nullptr;
};
