#include "mainwindow/MainWindow.h"

#include <QApplication>

#include "Parameters.h"


int main(int argc, char *argv[])
{
	mpasv::tools::parameters::ParameterManager::setParameterFileName("data/parameters/gui-parameters.json");

	mpasv::tools::parameters::ParameterManager::get();

	QApplication a(argc, argv);
	MainWindow w;
	w.show();
	return QApplication::exec();
}
