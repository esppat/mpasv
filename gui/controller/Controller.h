/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Controller.h
 * Author: esppat
 *
 * Created on January 2, 2020, 6:10 PM
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QtCore/QtCore>

#include "tools/intercomm/IntercommReceiver.h"
#include "tools/intercomm/IntercommClient.h"

#include "tools/gps/GeoPos.h"


class Controller
	: public QObject
{
	Q_OBJECT

public:

	Controller();
	Controller(const Controller& orig) = delete;
	virtual ~Controller();

public slots:

	void connectToVehicle();
	void disconnectFromVehicle();

signals:

	void connectedToVehicle(const bool isConnected);
	void disconnectedFromVehicle(const bool isConnected);

	void GSPSpeedChanged(const double & value);
	void GSPCourseChanged(const double & value);
	void GPSPositionChanged(const double & lat, const double & lon);
	void relativeWaterSpeedChanged(const double & value);
	void relativeWindSpeedChanged(const double & value);
	void relativeWinddegree_tChanged(const double & value);
	void realWindSpeedChanged(const double & value);
	void realWinddegree_tChanged(const double & value);
	void compassdegree_tChanged(const double & value);
	void helmPercentChanged(const double & value);
	void targetCourseChanged(const double & value);
	void courseErrorChanged(const double & value);
	void crossTrackErrorChanged(const double & value);
	void leftThrottleChanged(const double & value);
	void rightThrottleChanged(const double & value);

	void createSegment(const mpasv::tools::geo::GeoPos p1, const mpasv::tools::geo::GeoPos p2, const QString & color);


protected:

	void messageReceived(const mpasv::tools::intercomm::IntercommMessage & message);

protected:
private:

	void requestAndCreateRoute();

private:

	bool m_connectedToVehicle = false;

	friend mpasv::tools::intercomm::IntercommReceiver<Controller>;
	mpasv::tools::intercomm::IntercommReceiver<Controller> m_intercommReceiver;
	mpasv::tools::intercomm::IntercommClient m_intercommClient;

	float m_currentGPSCourse_deg = 0.0;
};

#endif /* CONTROLLER_H */
