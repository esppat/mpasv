/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Controller.cpp
 * Author: esppat
 *
 * Created on January 2, 2020, 6:10 PM
 */

#include "Controller.h"

#include "tools/Tools.h"
#include "tools/Logger.h"

#include "tools/gps/GeoPos.h"

#include "tools/intercomm/zmq.hpp"

#include "tools/intercomm/IntercommData.h"

Controller::Controller()
	: m_intercommReceiver(*this, "tcp://127.0.0.1:5555")
	, m_intercommClient("tcp://127.0.0.1:5556")
{
	std::setlocale(LC_ALL, "C");
}

Controller::~Controller()
= default;

void Controller::connectToVehicle()
{
	if (m_intercommReceiver.start())
	{
		emit connectedToVehicle(true);
		emit disconnectedFromVehicle(false);

		requestAndCreateRoute();
	}
	else
	{
		LOG_DEBUG("Cannot connect to vehicle");
		emit connectedToVehicle(false);
		emit disconnectedFromVehicle(true);
	}
}

void Controller::disconnectFromVehicle()
{
	m_intercommReceiver.stop();

	// TODO
	emit connectedToVehicle(false);
	emit disconnectedFromVehicle(true);
}

void Controller::messageReceived(const mpasv::tools::intercomm::IntercommMessage & message)
{
	switch (message.getType())
	{
		case DataType::dataBase:
		{
			const auto & data = message.data<DataBase>();

			emit GSPSpeedChanged(data.GPSSpeed_kts);
			m_currentGPSCourse_deg = data.GPSCourse_deg;
			emit GSPCourseChanged(m_currentGPSCourse_deg);
			emit GPSPositionChanged(data.lat_deg, data.lon_deg);
		}
		break;

		case DataType::dataDebug:
		{
			const auto & data = message.data<DataDebug>();

			emit relativeWaterSpeedChanged(data.relativeWaterSpeed_kts);
			emit relativeWindSpeedChanged(data.relativeWindSpeed_kts);
			emit relativeWinddegree_tChanged(data.relativeWinddegree_t_deg);
			emit realWindSpeedChanged(data.realWindSpeedCalculated_kts);
			emit realWinddegree_tChanged(data.realWinddegree_tCalculated_deg);
			emit compassdegree_tChanged(data.compassdegree_t_deg);
			emit targetCourseChanged(data.targetCourse_deg);
			emit courseErrorChanged(m_currentGPSCourse_deg - data.targetCourse_deg);
			emit crossTrackErrorChanged(data.crossTrackError_m);
		}
		break;

		case DataType::dataExtended:
		{
			const auto & data = message.data<DataExtended>();

			emit helmPercentChanged(data.helmPercent_pct);
			emit leftThrottleChanged(data.throttles_pct[0]);
			emit rightThrottleChanged(data.throttles_pct[1]);
		}
		break;

		case DataType::dataIMU:
		{
			// TODO
		}
		break;

		default:
		{
		}
		break;
	}
}

void Controller::requestAndCreateRoute()
{
	mpasv::tools::intercomm::IntercommMessage request; mpasv::tools::intercomm::IntercommMessage reply;
	DataRequestWaypointDescription requestData;

	size_t index = 0;
	mpasv::tools::geo::latitude_t previousLat;
	mpasv::tools::geo::longitude_t previousLon;

	for (;;)
	{
		requestData.waypointIndex = index;
		request.setData(requestData);

		if (m_intercommClient.sendRequest(request, reply))
		{
			const auto & replyData = reply.data<DataReplyWaypointDescription>();

			auto lat = (mpasv::tools::geo::latitude_t)(replyData.lat_deg);
			auto lon = (mpasv::tools::geo::latitude_t)(replyData.lon_deg);

			if (index > 0)
			{
				emit createSegment(mpasv::tools::geo::GeoPos(previousLat, previousLon), mpasv::tools::geo::GeoPos(lat, lon), "green");
			}

			previousLat = lat;
			previousLon = lon;

			if (replyData.waypointIndex == replyData.waypointCount - 1)
			{
				break;
			}
		}
		else
		{
			LOG_ERR("Cannot ask for waypoint description");
			break;
		}

		index++;
	}
}
