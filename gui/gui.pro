QT       += core gui webchannel 3dcore 3dextras webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix: CONFIG += link_pkgconfig

unix: PKGCONFIG += geographiclib
unix: PKGCONFIG += libgps
unix: PKGCONFIG += libzmq

HEADERS += \
	Parameters.h \
	controller/Controller.h \
	mainwindow/MainWindow.h \
	widgets/3dview/OrbitTransformController.h \
	widgets/3dview/Widget3D.h \
	widgets/gauge/BasicGaugeWidget.h \
	widgets/gauge/GaugeWidget.h \
	widgets/gauge/qcgaugewidget.h \
	widgets/map/MapWidget.h

SOURCES += \
	controller/Controller.cpp \
	main.cpp \
	mainwindow/MainWindow.cpp \
	widgets/3dview/OrbitTransformController.cpp \
	widgets/3dview/Widget3D.cpp \
	widgets/gauge/BasicGaugeWidget.cpp \
	widgets/gauge/GaugeWidget.cpp \
	widgets/gauge/qcgaugewidget.cpp \
	widgets/map/MapWidget.cpp

RESOURCES += \
	resources/basemap.qrc

DISTFILES += \
	../rundir/data/parameters/gui-parameters.json \
	resources/boat.stl \
	resources/map.html \
	rundir/data/parameters/gui-parameters.json

unix:!macx: LIBS += -L$$OUT_PWD/../base/ -lbase

INCLUDEPATH += $$PWD/../base
DEPENDPATH += $$PWD/../base
