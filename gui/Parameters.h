#pragma once

#include "tools/parameters/ParameterManager.h"

#include <QString>


template<class Type, class UnderlyingType = Type>
Type param(const QString & path)
{
	return param<Type, UnderlyingType>(std::string(path.toStdString().c_str()));
}

template<class Type, class UnderlyingType = Type>
void setParam(const QString & path, const Type & value, bool mustSave = false)
{
	setParam<Type, UnderlyingType>(std::string(path.toStdString().c_str()), value, mustSave);
}

inline bool hasParam(const QString & path)
{
	return hasParam(std::string(path.toStdString().c_str()));
}
