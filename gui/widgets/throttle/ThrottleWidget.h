/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   LinearWidget.h
 * Author: esppat
 *
 * Created on January 14, 2020, 10:16 PM
 */

#pragma once

#include <QAbstractSlider>
#include <QLabel>

class ThrottleWidget
: public QAbstractSlider
{
public:
	ThrottleWidget();
	ThrottleWidget(const ThrottleWidget& orig) = delete;
	virtual ~ThrottleWidget();

	void setLabel(QLabel * label, const QString & labelFormat);

	public
slots:

	void setCurrentValue(const double & value);

protected:
protected:
private:

	virtual void paintEvent(QPaintEvent *) override;

private:

	double m_value;

	QLabel * m_label;
	QString m_labelFormat;

};
