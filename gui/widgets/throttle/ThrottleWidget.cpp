/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   LinearWidget.cpp
 * Author: esppat
 *
 * Created on January 14, 2020, 10:16 PM
 */

#include "ThrottleWidget.h"

#include "tools/Logger.h"
#include "tools/Exception.h"

#include <QPainter>

ThrottleWidget::ThrottleWidget()
{
}

ThrottleWidget::~ThrottleWidget()
{
}

void ThrottleWidget::setLabel(QLabel * label, const QString & labelFormat)
{
	m_label = label;
	m_labelFormat = labelFormat;
}

void ThrottleWidget::setCurrentValue(const double & value)
{
	CHECK_GE(value, -100.0);
	CHECK_LE(value, 100.0);

	m_value = value;

	m_label->setText(QString::asprintf(m_labelFormat.toStdString().c_str(), value));

	update();
}

void ThrottleWidget::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);

	QRect r = rect();

	painter.scale(r.width() / 100.0, r.height() / 220.0);
	painter.translate(50.0, 110.0);

	painter.drawRect(-5.0, -100.0, 10.0, 200.0);
	painter.drawRect(-20.0, -m_value - 5.0, 40.0, 10.0);
}
