/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GaugeWidget.h
 * Author: esppat
 *
 * Created on January 13, 2020, 8:07 PM
 */

#pragma once

#include "qcgaugewidget.h"

#include <QList>
#include <QLabel>

class GaugeWidget
: public QcGaugeWidget
{
public:
	GaugeWidget(QWidget * parent = 0);
	GaugeWidget(const GaugeWidget& orig) = delete;
	virtual ~GaugeWidget();

	GaugeWidget * setSize(const unsigned size);

	GaugeWidget * setAsCompass();

	GaugeWidget * setDegreeRange(const float min, const float max);
	GaugeWidget * setValueRange(const float min, const float max, const float step, const float subStep);
	GaugeWidget * setColors(const QList<QPair<QColor, float> > & colors);

	void setLabel(QLabel * label);
	void setLabelFormat(const QString & labelFormat);

	public
slots:

	void setCurrentValue(const double value);

private:

	void adjustNeedleColor();

private:

	unsigned m_size = 150;

	QcArcItem * m_arc1;
	QcDegreesItem * m_degrees1;
	QcValuesItem * m_values1;
	QcColorBand * m_colorBands1;
	QcNeedleItem * m_needle1;

	QList<QPair<QColor, float> > m_colors1;

	QLabel * m_label = nullptr;
	QString m_labelFormat;

};
