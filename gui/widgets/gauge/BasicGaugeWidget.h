#pragma once

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>

#include "GaugeWidget.h"


class BasicGaugeWidget : public QWidget
{
public:

	BasicGaugeWidget(const QString & model, QWidget * parent = 0);
	BasicGaugeWidget(QWidget * parent = 0);

	void setLabelFormat(const QString & labelFormat);

public slots:

	void setCurrentValue(const double value);

protected:

	const QString m_model;

	QVBoxLayout * m_layout{};
	GaugeWidget * m_gaugeWidget;
	QLabel * m_label{};

private:

	void createNormal(const QString & model);
	void createCompass(const QString & model);

	void setupCommonPart(const QString & model);
	void setupLayout();
};
