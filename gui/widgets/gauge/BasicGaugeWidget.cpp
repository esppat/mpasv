#include "BasicGaugeWidget.h"

#include "tools/Exception.h"
#include "Parameters.h"


BasicGaugeWidget::BasicGaugeWidget(QWidget * parent)
	: BasicGaugeWidget("Default", parent)
{
}

BasicGaugeWidget::BasicGaugeWidget(const QString & model, QWidget * parent)
	: QWidget(parent)
	, m_model(model)
{
	m_gaugeWidget = new GaugeWidget();

	const QString typeName = (hasParam("Widgets." + model + ".Type") ? "Widgets." + model + ".Type" : "Widgets.Default.Type");
	const QString type = QString(param<std::string>(typeName).c_str());

	if (type == "normal")
	{
		createNormal(model);
	}
	else if (type == "compass")
	{
		createCompass(model);
	}

	setupCommonPart(model);

	setupLayout();
}

void BasicGaugeWidget::setLabelFormat(const QString & labelFormat)
{
	m_gaugeWidget->setLabelFormat(labelFormat);
}

void BasicGaugeWidget::setCurrentValue(const double value)
{
	m_gaugeWidget->setCurrentValue(value);
}

void BasicGaugeWidget::createNormal(const QString & model)
{
	m_gaugeWidget->setDegreeRange(param<double>("Widgets." + model + ".Degree range A"),
								  param<double>("Widgets." + model + ".Degree range B"));

	m_gaugeWidget->setValueRange(param<double>("Widgets." + model + ".Value range min"),
								 param<double>("Widgets." + model + ".Value range max"),
								 param<double>("Widgets." + model + ".Value range step"),
								 param<double>("Widgets." + model + ".Value range substep"));

	QList<QPair<QColor, float>> colors;
	for (uint8_t index = 0; index < param<uint8_t>("Widgets." + model + ".Colors.Color count"); index++)
	{
		colors.append(QPair<QColor, float>(QColor(param<int>("Widgets." + model + ".Colors.C" + QString::number(index) + ".R"),
												  param<int>("Widgets." + model + ".Colors.C" + QString::number(index) + ".G"),
												  param<int>("Widgets." + model + ".Colors.C" + QString::number(index) + ".B")),
										   param<float>("Widgets." + model + ".Colors.C" + QString::number(index) + ".Limit")));
	}
	m_gaugeWidget->setColors(colors);
}

void BasicGaugeWidget::createCompass(const QString & /*model*/)
{
	m_gaugeWidget->setAsCompass();
}

void BasicGaugeWidget::setupCommonPart(const QString & model)
{
	const QString sizeName = (hasParam("Widgets." + model + ".Size") ? "Widgets." + model + ".Size" : "Widgets.Default.Size");
	m_gaugeWidget->setSize(param<uint16_t>(sizeName));

	m_label = new QLabel();
	m_label->setAlignment(Qt::AlignHCenter);

	const QString labelSizeName = (hasParam("Widgets." + model + ".Label size") ? "Widgets." + model + ".Label size" : "Widgets.Default.Label size");
	m_label->setFixedWidth(param<uint16_t>(labelSizeName));

	const QString styleSheetName = (hasParam("Widgets." + model + ".Style sheet") ? "Widgets." + model + ".Style sheet" : "Widgets.Default.Style sheet");
	m_label->setStyleSheet(QString(param<std::string>(styleSheetName).c_str()));

	m_gaugeWidget->setLabelFormat(QString(param<std::string>("Widgets." + model + ".Label format").c_str()));

	m_label->setText("<no value>");

	m_gaugeWidget->setLabel(m_label);

	const QString defaultCurrentValueName = (hasParam("Widgets." + model + ".Default current value") ? "Widgets." + model + ".Default current value" : "Widgets.Default.Default current value");
	m_gaugeWidget->setCurrentValue(param<double>(defaultCurrentValueName));
}

void BasicGaugeWidget::setupLayout()
{
	m_layout = new QVBoxLayout();

	m_layout->addWidget(m_gaugeWidget);
	m_layout->addWidget(m_label);

	this->setLayout(m_layout);
}
