/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GaugeWidget.cpp
 * Author: esppat
 *
 * Created on January 13, 2020, 8:07 PM
 */

#include "GaugeWidget.h"

#include "tools/Exception.h"

GaugeWidget::GaugeWidget(QWidget * parent)
	: QcGaugeWidget(parent)
{
	addBackground(99);
	QcBackgroundItem *bkg1 = addBackground(92);
	bkg1->clearrColors();
	bkg1->addColor(0.1, Qt::black);
	bkg1->addColor(1.0, Qt::white);

	QcBackgroundItem *bkg2 = addBackground(88);
	bkg2->clearrColors();
	bkg2->addColor(0.1, Qt::gray);
	bkg2->addColor(1.0, Qt::darkGray);

	m_arc1 = addArc(55);
	m_degrees1 = addDegrees(65);
	m_values1 = addValues(75);
	m_colorBands1 = addColorBand(50);
	m_needle1 = addNeedle(60);
	m_needle1->setColor(QColor(255, 0, 0));
}

GaugeWidget::~GaugeWidget()
= default;

GaugeWidget * GaugeWidget::setSize(const unsigned size)
{
	CHECK_GT(size, 30);
	setFixedSize(size, size);
	return this;
}

GaugeWidget * GaugeWidget::setAsCompass()
{
	m_arc1->setDegreeRange(-90.0, 270.0);
	m_degrees1->setDegreeRange(-90.0, 270.0);
	m_values1->setDegreeRange(-90.0, 270.0);
	m_needle1->setDegreeRange(-90.0, 270.0);

	if (m_colorBands1 != nullptr)
	{
		removeItem(m_colorBands1);
		delete m_colorBands1;
		m_colorBands1 = nullptr;
	}

	if (m_values1 != nullptr)
	{
		removeItem(m_values1);
		delete m_values1;
		m_values1 = nullptr;
	}

	m_arc1->setValueRange(-180, 180);
	m_degrees1->setValueRange(-180, 180);
	m_degrees1->setStep(45.0);

	m_needle1->setValueRange(-180, 180);

	QcLabelItem *w = addLabel(75);
	w->setText("W");
	w->setAngle(0);

	QcLabelItem *n = addLabel(75);
	n->setText("N");
	n->setAngle(90);

	QcLabelItem *e = addLabel(75);
	e->setText("E");
	e->setAngle(180);

	QcLabelItem *s = addLabel(75);
	s->setText("S");
	s->setAngle(270);

	return this;
}

GaugeWidget * GaugeWidget::setDegreeRange(const float min, const float max)
{
	m_arc1->setDegreeRange(min, max);
	m_degrees1->setDegreeRange(min, max);
	m_values1->setDegreeRange(min, max);
	m_colorBands1->setDegreeRange(min, max);
	m_needle1->setDegreeRange(min, max);

	return this;
}

GaugeWidget * GaugeWidget::setValueRange(const float min, const float max, const float step, const float subStep)
{
	m_degrees1->setValueRange(min, max);
	m_degrees1->setStep(subStep);

	m_values1->setValueRange(min, max);
	m_values1->setStep(step);

	m_colorBands1->setValueRange(min, max);

	m_needle1->setValueRange(min, max);

	return this;
}

GaugeWidget * GaugeWidget::setColors(const QList<QPair<QColor, float> > & colors)
{
	m_colors1 = colors;

	m_colorBands1->setColors(m_colors1);

	return this;
}

void GaugeWidget::setLabel(QLabel * label)
{
	m_label = label;
}

void GaugeWidget::setLabelFormat(const QString & labelFormat)
{
	m_labelFormat = labelFormat;
}

void GaugeWidget::setCurrentValue(const double value)
{
	m_needle1->setCurrentValue((float) value);

	if (m_label != nullptr)
	{
		if (!m_labelFormat.isEmpty())
		{
			m_label->setText(QString::asprintf(m_labelFormat.toStdString().c_str(), value));
		}
		else
		{
			m_label->setText(QString::asprintf("%lf", value));
		}
	}
}

void GaugeWidget::adjustNeedleColor()
{
	// TODO
}
