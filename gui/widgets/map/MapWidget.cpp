/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MainWindow.cpp
 * Author: esppat
 *
 * Created on December 30, 2019, 1:36 AM
 */
#include "MapWidget.h"

#include "tools/Logger.h"

#include <QMessageBox>
#include <QtGui>
#include <QtCore>
#include <QtWebEngineWidgets/QWebEngineSettings>
#include <QWebChannel>
#include <QGridLayout>
#include <QPushButton>

#include <cstdio>
#include <clocale>

MapWidget::MapWidget(const QString & /*title*/, QWidget *parent)
	: QWebEngineView(parent)
{
	auto * channel = new QWebChannel(this);
	channel->registerObject("mapWidget", this);
	page()->setWebChannel(channel);
	page()->setUrl(QUrl("qrc:///map.html"));

	setMinimumSize(800, 600);
}

MapWidget::~MapWidget()
= default;

void MapWidget::goTo(tools::geo::latitude_t latitude, tools::geo::longitude_t longitude, uint8_t zoom)
{
	setZoom(zoom);
	goTo(latitude, longitude);
}

void MapWidget::goTo(tools::geo::latitude_t latitude, tools::geo::longitude_t longitude)
{
	if (m_centerLatitude != (double) latitude || m_centerLongitude != (double) longitude)
	{
		startSingleCommand();
		m_centerLatitude = (double) latitude;
		m_centerLongitude = (double) longitude;
		emit centerChanged();
	}
}

void MapWidget::setZoom(uint8_t zoom)
{
	if (m_zoom != zoom && zoom > 1 && zoom < 20)
	{
		startSingleCommand();
		m_zoom = zoom;
		emit zoomChanged();
	}
}

void MapWidget::createCircle(const tools::geo::GeoPos & pos,
							 const meter_t radius,
							 const QString& color,
							 const QString& fillColor,
							 const double & fillOpacity)
{
	startSingleCommand();
	emit circleCreated(tools::physics::doubleFrom<tools::geo::latitude_t>(pos.lat()),
					tools::physics::doubleFrom<tools::geo::longitude_t>(pos.lon()),
					tools::physics::doubleFrom<meter_t>(radius),
					color,
					fillColor,
					fillOpacity);
}

void MapWidget::createSegment(const tools::geo::GeoPos & pos1,
							  const tools::geo::GeoPos & pos2,
							  const QString& color)
{
	startSingleCommand();
	emit segmentCreated(tools::physics::doubleFrom<tools::geo::latitude_t>(pos1.lat()),
						tools::physics::doubleFrom<tools::geo::longitude_t>(pos1.lon()),
						tools::physics::doubleFrom<tools::geo::latitude_t>(pos2.lat()),
						tools::physics::doubleFrom<tools::geo::longitude_t>(pos2.lon()),
						color);
}

void MapWidget::zoomIn()
{
	setZoom(m_zoom + 1);
}

void MapWidget::zoomOut()
{
	setZoom(m_zoom - 1);
}

void MapWidget::moveFinished(double lat, double lon)
{
	m_mutex.lock();
	if (m_commandPending)
	{
		if (std::fabs(m_centerLatitude - lat) <= 0.0001 && std::fabs(m_centerLongitude - lon) < 0.0001)
		{
			m_commandPending = false;
		}
		else
		{
			emit centerChanged();
		}
	}
	else
	{
		m_centerLatitude = lat;
		m_centerLongitude = lon;
	}
	m_mutex.unlock();
}

void MapWidget::zoomFinished(uint zoom)
{
	m_mutex.lock();
	if (m_commandPending)
	{
		if (zoom == m_zoom)
		{
			m_commandPending = false;
		}
		else
		{
			emit zoomChanged();
		}
	}
	else
	{
		m_zoom = zoom;
	}
	m_mutex.unlock();
}

void MapWidget::commandFinished()
{
	m_mutex.lock();
	if (m_commandPending)
	{
		m_commandPending = false;
	}
	m_mutex.unlock();
}

void MapWidget::onClicked(double lat, double lon)
{
	emit clicked(lat, lon);
}

void MapWidget::gotoGPSPosition(const double & lat, const double & lon)
{
	GeoPos pos {latitude_t(lat), longitude_t(lon)};
	goTo(pos.lat(), pos.lon());
}

void MapWidget::createCircleAtPos(const double & lat, const double & lon)
{
	GeoPos pos {latitude_t(lat), longitude_t(lon)};
	createCircle(pos, 0.2_m);
}

void MapWidget::startSingleCommand()
{
	for (;;)
	{
		m_mutex.lock();
		if (m_commandPending)
		{
			m_mutex.unlock();
			QEventLoop loop;
			loop.processEvents();
		}
		else
		{
			m_commandPending = true;
			m_mutex.unlock();
			break; // for (;;)
		}
	}
}
