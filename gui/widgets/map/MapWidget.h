/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MainWindow.h
 * Author: esppat
 *
 * Created on December 30, 2019, 1:36 AM
 */
#pragma once

#include "tools/physics/Units.h"
#include "tools/gps/GeoPos.h"

#include <mutex>

#include <QWidget>
#include <QtWebEngineWidgets/QWebEngineView>

using namespace mpasv;
using namespace mpasv::tools;
using namespace mpasv::tools::geo;

class MapWidget : public QWebEngineView
{
	Q_OBJECT
	Q_PROPERTY(unsigned zoom MEMBER m_zoom NOTIFY zoomChanged)
	Q_PROPERTY(double latitude MEMBER m_centerLatitude NOTIFY centerChanged)
	Q_PROPERTY(double longitude MEMBER m_centerLongitude NOTIFY centerChanged)

public:

	explicit MapWidget(const QString & /*title*/, QWidget *parent = nullptr);
	virtual ~MapWidget();

	void goTo(latitude_t latitude, longitude_t longitude, uint8_t zoom);
	void goTo(latitude_t latitude, longitude_t longitude);
	void setZoom(uint8_t zoom);

	latitude_t centerLatitude()
	{
		return (latitude_t) m_centerLatitude;
	}

	longitude_t centerLongitude()
	{
		return (longitude_t) m_centerLongitude;
	}

	unsigned zoom()
	{
		return m_zoom;
	}

public slots:

	void createCircle(const GeoPos & pos,
					  const meter_t radius,
					  const QString& color = "red",
					  const QString& fillColor = "red",
					  const double & fillOpacity = 0.2);

	void createSegment(const GeoPos & pos1,
					   const GeoPos & pos2,
					   const QString& color = "red");

	void zoomIn();
	void zoomOut();
	void moveFinished(double lat, double lon);
	void zoomFinished(uint zoom);
	void commandFinished();
	void onClicked(double lat, double lon);
	void gotoGPSPosition(const double & lat, const double & lon);
	void createCircleAtPos(const double & lat, const double & lon);

signals:

	void centerChanged();
	void zoomChanged();
	void clicked(double lat, double lon);

	void circleCreated(double lat,
					   double lon,
					   double radius,
					   const QString color,
					   const QString fillColor,
					   double fillOpacity);
	void segmentCreated(double lat1,
						double lon1,
						double lat2,
						double lon2,
						const QString color);

private:

	void startSingleCommand();

private:

	unsigned m_zoom = 1;
	double m_centerLatitude{};
	double m_centerLongitude{};
	std::mutex m_mutex;
	bool m_commandPending = false;

};
