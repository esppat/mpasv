/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Widget3D.h
 * Author: esppat
 *
 * Created on January 14, 2020, 10:16 PM
 */

#pragma once

#include <QWidget>
#include <Qt3DExtras/qt3dwindow.h>

#include <memory>

class Widget3D
: public QWidget
{
public:

	Widget3D(QWidget * parent = 0);
	Widget3D(const Widget3D& orig) = delete;
	virtual ~Widget3D();

protected:
protected:
private:
	
	Qt3DCore::QEntity * createScene();
	
private:

	std::shared_ptr<Qt3DExtras::Qt3DWindow> m_view;

};
