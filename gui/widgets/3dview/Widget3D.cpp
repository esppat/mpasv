/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Widget3D.cpp
 * Author: esppat
 *
 * Created on January 14, 2020, 10:16 PM
 */

#include "Widget3D.h"

#include "tools/Logger.h"
#include "tools/Exception.h"

#include <QPropertyAnimation>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <Qt3DCore/QEntity>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DCore/QTransform>
#include <Qt3DCore/QAspectEngine>

#include <Qt3DInput/QInputAspect>

#include <Qt3DRender/QRenderAspect>
#include <Qt3DRender/QMesh>

#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QOrbitCameraController>

#include "OrbitTransformController.h"

Widget3D::Widget3D(QWidget * parent)
	: QWidget(parent)
{
	m_view.reset(new Qt3DExtras::Qt3DWindow());
	QWidget *container = QWidget::createWindowContainer(m_view.get());
	container->setMinimumSize(QSize(100, 100));

	Qt3DCore::QEntity *scene = createScene();

	// Camera
	Qt3DRender::QCamera *camera = m_view->camera();
	camera->lens()->setPerspectiveProjection(45.0F, 16.0F / 9.0F, 0.1F, 1000.0F);
	camera->setPosition(QVector3D(0, 0, 40.0F));
	camera->setViewCenter(QVector3D(0, 0, 0));

	// For camera controls
	auto *camController = new Qt3DExtras::QOrbitCameraController(scene);
	camController->setLinearSpeed(50.0F);
	camController->setLookSpeed(180.0F);
	camController->setCamera(camera);

	m_view->setRootEntity(scene);

	auto *hLayout = new QHBoxLayout(this);
	hLayout->addWidget(container, 1);
	setLayout(hLayout);
}

Widget3D::~Widget3D()
= default;

Qt3DCore::QEntity * Widget3D::createScene()
{
	auto *rootEntity = new Qt3DCore::QEntity;

	auto *boatEntity = new Qt3DCore::QEntity(rootEntity);

	Qt3DRender::QMaterial *material = new Qt3DExtras::QPhongMaterial;

	auto *boatMesh = new Qt3DRender::QMesh;
	boatMesh->setSource(QUrl("qrc:/boat.stl"));

	auto *boatTransform = new Qt3DCore::QTransform;
	boatTransform->setRotation(QQuaternion::fromEulerAngles(-90.0F, 90.0F, 0.0F));
	boatTransform->setTranslation(QVector3D(0.0F, -25.0F, -25.0F));

	boatEntity->addComponent(boatMesh);
	boatEntity->addComponent(boatTransform);
	boatEntity->addComponent(material);

	auto *sphereEntity = new Qt3DCore::QEntity(rootEntity);
	auto *sphereMesh = new Qt3DExtras::QSphereMesh;
	sphereMesh->setRadius(3);

	auto *sphereTransform = new Qt3DCore::QTransform;
	auto *controller = new OrbitTransformController(sphereTransform);
	controller->setTarget(sphereTransform);
	controller->setRadius(20.0F);

	auto *sphereRotateTransformAnimation = new QPropertyAnimation(sphereTransform);
	sphereRotateTransformAnimation->setTargetObject(controller);
	sphereRotateTransformAnimation->setPropertyName("angle");
	sphereRotateTransformAnimation->setStartValue(QVariant::fromValue(0));
	sphereRotateTransformAnimation->setEndValue(QVariant::fromValue(360));
	sphereRotateTransformAnimation->setDuration(10000);
	sphereRotateTransformAnimation->setLoopCount(-1);
	sphereRotateTransformAnimation->setEasingCurve(*(new QEasingCurve(QEasingCurve::InOutQuad)));
	sphereRotateTransformAnimation->start();

	sphereEntity->addComponent(sphereMesh);
	sphereEntity->addComponent(sphereTransform);
	sphereEntity->addComponent(material);

	return rootEntity;
}
