/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   OrbitTransformController.cpp
 * Author: esppat
 *
 * Created on January 24, 2020, 5:58 AM
 */

#include "OrbitTransformController.h"

#include <Qt3DCore/qtransform.h>

QT_BEGIN_NAMESPACE

OrbitTransformController::OrbitTransformController(QObject *parent)
	: QObject(parent)
	, m_target(nullptr)
	, m_radius(1.0F)
	, m_angle(0.0F)
{
}

void OrbitTransformController::setTarget(Qt3DCore::QTransform *target)
{
	if (m_target != target)
	{
		m_target = target;
		emit targetChanged();
	}
}

Qt3DCore::QTransform *OrbitTransformController::target() const
{
	return m_target;
}

void OrbitTransformController::setRadius(float radius)
{
	if (!qFuzzyCompare(radius, m_radius))
	{
		m_radius = radius;
		updateMatrix();
		emit radiusChanged();
	}
}

float OrbitTransformController::radius() const
{
	return m_radius;
}

void OrbitTransformController::setAngle(float angle)
{
	if (!qFuzzyCompare(angle, m_angle))
	{
		m_angle = angle;
		updateMatrix();
		emit angleChanged();
	}
}

float OrbitTransformController::angle() const
{
	return m_angle;
}

void OrbitTransformController::updateMatrix()
{
	m_matrix.setToIdentity();
	m_matrix.rotate(m_angle, QVector3D(0.0F, 1.0F, 0.0F));
	m_matrix.translate(m_radius, 0.0F, 0.0F);
	m_target->setMatrix(m_matrix);
}

QT_END_NAMESPACE
